# Digi

Digi är Arbetsförmedlingens och Skolverkets gemensamma designsystem. Här finns bland annat komponentbibliotek, design
tokens, funktionalitet, api:er och annat som hjälper dig att bygga digitala tjänster som följer våra gemensamma
riktlinjer. Repot är ett monorepo genererat med [Nx](https://nx.dev).
Besök [Arbetsförmedlingens dokumentationssida](https://designsystem.arbetsformedlingen.se/) för att se vilka komponenter
som ingår.

## Kom igång

### Förutsättningar

- [node (och npm)](https://nodejs.org/en/) i din utvecklingsmiljö. Aktuell nodeversion finns i `.nvmrc`

### Installera

- `nvm use` om du använder nvm. Annars får byta till rätt nodeversion på annat sätt.
- `npm i` i roten av projektet.

## Arbeta med bibliotek och applikationer

Nx gör skillnad på bibliotek (libs) och applikationer (apps). Dessa hittar du under respektive mapp i projektet. Digi
innehåller flera olika bibliotek och applikationer med tydligt definierade syften.

## Versionshantering

Arbetsförmedlingen och Skolverket versionshanterar sina designsystem oberoende av varandra.
Versionshantering hanteras per designsystem och versionen sätt för hela designsystemet. Vi versionshanterar inte per
komponent, utan per system.

### Semantic versioning

Designsystemen följer [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

- MAJOR version ändras när en eller flera komponenter inte längre är bakåtkompatibla i sin version. Det kan bero på
  ändringar av komponentens API
  eller [visuella breaking changes](https://medium.com/eightshapes-llc/visual-breaking-change-in-design-systems-1e9109fac9c4).
- MINOR version ändras när vi lägger till nya features eller komponenter som är bakåtkompatibla.
- PATCH version ändras när vi endast gjort buggfixar, eller förändringar av dokumentationen.

### Struktur

Varje lib är grupperad efter ägandeskap:

- `core` är sådant som delas av alla. Publiceras som `@digi/core`
- `arbetsformedlingen` är specifikt för Arbetsförmedlingen. Publiceras som `@digi/arbetsformedlingen`
- `skolverket` är specifikt för Skolverket. Publiceras som `@digi/skolverket`

Dessa libs innehåller några av följande bibliotek:

- `package` - Detta är ett Stencil-projekt med komponenter. Alla komponenter från `core` kopieras in i de andras
  komponentbibliotek när dom byggs, vilket gör att dom finns tillgängliga direkt inne i respektive myndighets
  publicerade
  paket
- `design-tokens` - Design tokenskonfigurationer (använder sig av Style Dictionary) för respektive myndighet
- `fonts` - Typsnittsfiler och css-definitioner
- `angular` - Angularkomponenter från Stencils Angular output target
- `react` - Reactkomponenter från Stencils React output target

Utöver allt ovan finns även ett lib som heter `shared`. Där finns funktioner och annat som används av alla andra
tjänster. I `Shared\Styles` ligger
all css som genereras automatiskt vid byggen.

#### Shared/Taxonomies

Taxonomies är ett hjälp-bibliotek för att hantera alla olika eventuella taxonomier vi kan behöva. Här finns
kategorier för komponenter, och även css-kategorier kommer tillkomma. Vid behov av andra typer av taxonomier (till
exempel taggar om vi behöver flera-till-en-taxonomier), så lägger vi till dem här. Detta bibliotek används till exempel
av Docs för att kategorisera komponenter i navigationen.

## Börja jobba

Beroende på vilken ändring du ska göra i designsystemet så kan du behöva starta upp olika delar.

### Starta enskilt bibliotek (lib)

Du kan starta olika delar av repot genom `npm run start <lib>`.
Aktuella ´lib´ är;

- Core
- Arbetsförmeldingen
- Skolverket

Kommandot öppnar en `index.html`-fil som finns i just det bibliotekets src-mapp. Där kan du lägga in komponenten du
jobbar med. Det körs en watch i bakgrunden på det biblioteket.

En gotcha är att om exempelvis en AF-komponent är beroende av en core-komponent och du gör ändringar i core-komponenten
men har watchen för AF igång, så kommer core-ändringarna inte inkluderas i watchen.

### Starta dokumentations-webbarna (apps)

Du kan även starta de olika dokumentations-webbarna eller test-applikationerna genom att först bygga
dom `npm run build <apps>` och sedan `npm run start <apps>` `<apps>` motsvarar namnet på appen som finns specificerat i
dens `project.json`.

## Tooling (scripts)

Alla generella scripts ligger i rotens `package.json`. Dom dokumenteras här, eller i REDAME.md för respektive myndighet.

### Versions

- Alla `version-` script så som `version-arbetsformedlingen` används för att öka versionsnumret på specificerat
  designsystem. Detta kommando körs manuellt vid release.
    - Kom ihåg att specificera vilken semantic version som ska justeras, `version-arbetsformedlingen patch|minor|major`
    - Obs! Kommandot ändrar inte peer-dependencies för dom ramverks-specifika paketen, så dessa måste hanteras manuellt.
      Exempelvis `@digi/arbetsformedlingen-react` har `@digi/arbetsformedlingen` som peerDependency. Så efter att du
      kört versions-kommandot behöver du gå in i React-lib:ets `package.json` och justera peerDependency-versionen
      av `@digi/arbetsformedlingen` till den senaste.
- Behöver vi släppa ett paket som `beta` kan vi använda oss av `version-arbetsformedlingen:beta`. Vi behöver även där
  manuellt uppdatera peer-dependencies som beskrivet ovan.
    - För att en tjänst ska kunna hämta beta-paketet kör `release-arbetsformedlingen:beta`
- Kommandot `version-arbetsformedlingen:prerelease` används sällan, men fungerar på liknande sätt som `beta`.
- Kommandona `version:prerelease` eller `version:beta` påverkar alla som finns definierade som "workspaces" i rotens
  package.json

### Release

- Release-scripten bygger och publicerar definierat projekt. Projektens build och publish script finns i deras
  `project.json` filer.

### NX

Vi har NX och kan köra alla script som kommer med NX, även om de inte finns dokumenterade här.
De körs genom `npx nx ` - namn på script.

Bra kommandon att komma ihåg:

- `npx nx migrate latest` - Håller koll på alla paket i repot så alla beroenden ska fungera med varandra. Så om vi ska
  gå från Angular X till Y som i sin tur är beroende av Typescript Z så har NX koll på detta.
- `npx nx dep-graph` - Visar en utförlig dependency-graf.

### Övrigt

- `sw-precache:docs:arbetsformedlingen` - Behöver undersökas om och hur denna fortfarande behövs.

## Arbetsförmedlingen

Mer dokumentation för hur du jobbar med specifikt arbetsförmedlingens designsystem hittar
du [i README för apps/arbetsförmedlingen](./apps/arbetsformedlingen/README.md). Där finns bland annat instruktioner för
hur du skapar upp en ny komponent.

## Skolverket

Här hittar du information om hur du startar dokumentation och hur du bygger/testar komponenter för Skolverket.
[Skolverket](./apps/Skolverket/README.md)

## Publicera paket

Publicering av paket görs separat för dom olika myndigheterna.

- [Publicera Skolverkets paket och dokumentationswebb](/libs/skolverket)
- Arbetsförmedlingens process finns dokumenterad internt på myndigheten.
