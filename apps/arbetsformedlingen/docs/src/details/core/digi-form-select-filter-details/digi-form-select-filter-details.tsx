import { Component, h, Host, Prop, State } from '@stencil/core';
import {
	CodeExampleLanguage,
	FormSelectFilterValidation,
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-form-select-filter-details',
	styleUrl: 'digi-form-select-filter-details.scss'
})
export class DigiFormSelectFilter {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() formSelectFilterValidation: FormSelectFilterValidation = FormSelectFilterValidation.NEUTRAL;
	@State() afMultipleItems: boolean = false;

	_listItems=[
				{"label":"Danska"},
				{"label":"Tyska"},
				{"label":"العربية (Arabiska)", "dir":"rtl", "lang":"ar"},
				{"label":"Ryska"},
				{"label":"Franska"},
				{"label":"Finska"},
				{"label":"Holländska"},
				{"label":"Italienska"},
				{"label":"Spanska"},
				{"label":"Grekiska"},
				{"label":"Romänska"}
	]

	get listItems() {
		return JSON.stringify(this._listItems);
	}


	get formSelectFilterCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-form-select-filter
	af-filter-button-text-label="Välj språk"
	af-description="Beskrivande text"
	af-filter-button-text="Inget språk valt"
	af-submit-button-text="Filtrera"
	af-multiple-items="${this.afMultipleItems}"
	af-validation="${this.formSelectFilterValidation}"
	af-list-items=""
	${this.formSelectFilterValidation !== FormSelectFilterValidation.NEUTRAL ? 'af-validation-text="Det här är ett valideringsmeddelande" \t' : ''}
\>
</digi-form-select-filter>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-form-select-filter
	[attr.af-filter-button-text-label]="'Välj språk'"
	[attr.af-description]="'Beskrivande text'"
	[attr.af-filter-button-text]="'Inget språk valt'"
	[attr.af-submit-button-text]="'Filtrera'"
	[attr.af-multiple-items]="${this.afMultipleItems}"
	[attr.af-validation]="FormSelectFilterValidation.${this.formSelectFilterValidation.toUpperCase()}"
	[attr.af-list-items]="listItems"
	${this.formSelectFilterValidation !== FormSelectFilterValidation.NEUTRAL ? '[attr.af-validation-text]="Det här är ett valideringsmeddelande"\n\t' : ''}
\>
</digi-form-select-filter>`,
			[CodeExampleLanguage.REACT]: `\
<DigiFormSelectFilter
	afFilterButtonTextLabel="Välj språk"
	afDescription="Beskrivande text"
	afFilterButtonText="Inget språk valt"
	afSubmitButtonText="Filtrera"
	afMultipleItems={${this.afMultipleItems}
	afValidation={FormSelectFilterValidation.${this.formSelectFilterValidation.toUpperCase()}}
	afListItems={listItems}
	${this.formSelectFilterValidation !== FormSelectFilterValidation.NEUTRAL ? 'afValidationText="Det här är ett valideringsmeddelande"\n\t' : ''}\t
\>
</DigiFormSelectFilter>`
		};
	}

	render() {
		return (
			<Host>
				<div class="digi-form-select-filter-details">
					<div class="block-spacer">
					{!this.afShowOnlyExample && (
						<digi-typography-preamble>
							En väljare med möjlighet att söka i listan.
						</digi-typography-preamble>)}
					</div>
					<digi-layout-container af-no-gutter af-margin-bottom af-margin-top>
						<article>
					{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
					<digi-code-example
						af-code={JSON.stringify(this.formSelectFilterCode)}
						af-hide-controls={this.afHideControls ? 'true' : 'false'}
						af-hide-code={this.afHideCode ? 'true' : 'false'}
						class="digi-form-select-filter-details__code-example"
					>
						<div class="slot__controls" slot="controls">
							<digi-form-fieldset afLegend="Flera val ">
								<digi-form-checkbox
									afLabel="Ja"
									afChecked={this.afMultipleItems}
									onAfOnChange={() =>
										this.afMultipleItems
											? (this.afMultipleItems = false)
											: (this.afMultipleItems = true)
									}
								></digi-form-checkbox>
							</digi-form-fieldset>
							<digi-form-select
									afLabel="Validering"
									onAfOnChange={(e) =>
										(this.formSelectFilterValidation = (e.target as any).value)
									}
									af-variation="small"
								>
									<option value="neutral">Ingen</option>
									<option value="error">Felaktig</option>
									<option value="success">Godkänd</option>
									<option value="warning">Varning</option>
								</digi-form-select>
						</div>
					
						<div style={{ height: '500px' }}>
							{this.afMultipleItems && (<digi-form-select-filter
								afMultipleItems={this.afMultipleItems}
								afFilterButtonTextMultipleValues="Några val ({count})"
								afFilterButtonTextLabel="Välj språk"
								afFilterButtonText="Inget språk valt"
								afSubmitButtonText="Filtrera"
								afDescription="Beskrivande text"
								afName="Sök språk"
								afListItems={this.listItems}
								afRequired={true}
								afRequiredText="obligatoriskt"
								afAnnounceIfOptional={true}
								afAnnounceIfOptionalText="frivilligt"
								afDisableValidation={false}
								afValidation={this.formSelectFilterValidation}
								af-validation-text="Det här är ett valideringsmeddelande"
							>
							</digi-form-select-filter>
							)}
							{!this.afMultipleItems && (
								<digi-form-select-filter
								afMultipleItems={this.afMultipleItems}
								afFilterButtonTextMultipleValues="Några val ({count})"
								afFilterButtonTextLabel="Välj språk"
								afFilterButtonText="Inget språk valt"
								afSubmitButtonText="Filtrera"
								afDescription="Beskrivande text"
								afName="Sök språk"
								afListItems={this.listItems}
								afRequired={true}
								afRequiredText="obligatoriskt"
								afAnnounceIfOptional={true}
								afAnnounceIfOptionalText="frivilligt"
								afDisableValidation={false}
								afValidation={this.formSelectFilterValidation}
								af-validation-text="Det här är ett valideringsmeddelande"
							>
							</digi-form-select-filter>
							)}
						</div>
					</digi-code-example>
					
					</article>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
					<digi-layout-container af-no-gutter af-margin-bottom>
						<article>
							<h2>Beskrivning</h2>
							<h3>Att skicka in data</h3>
							<p>För att skicka in data så ser objektstrukturen ut så här:</p>
							<digi-code-block
											af-language="typescript"
											af-code='_listItems: IListItems[] = [
{"label":"Danska"},
{"label":"Tyska"},
{"label":"العربية (Arabiska)", "dir":"rtl", "lang":"ar"},
{"label":"Ryska"},
{"label":"Franska"},
{"label":"Finska"},
{"label":"Holländska"},
{"label":"Italienska"},
{"label":"Spanska"},
{"label":"Grekiska"},
{"label":"Romänska"}
]'
											af-variation="dark"
							></digi-code-block>
							<br/>
							<p>Om du använder HTML så kan du använda följande javascript för att skicka in alternativen i väljaren. Se till att ha byta utt id.</p>
							<digi-code-block 
								af-language="javascript"
								af-variation="dark"
								af-code='const listItems = JSON.stringify(_listItems);
document.getElementById("id").setAttribute("af-list-items", listItems);'
							></digi-code-block>
						</article>
					</digi-layout-container>
					)}
				</div>
			</Host>
		);
	}
}
