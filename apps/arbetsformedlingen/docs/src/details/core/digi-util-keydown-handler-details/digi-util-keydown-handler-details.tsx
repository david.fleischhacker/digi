import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'digi-util-keydown-handler-details',
  styleUrl: 'digi-util-keydown-handler-details.scss'
})
export class DigiUtilKeydownHandlerDetails {
  @Prop() component: string;

  render() {
    return (
      <div class="digi-util-keydown-handler-details">
        <digi-typography>
          <digi-layout-block af-variation="primary">
            <digi-typography-preamble>
              <digi-code af-code={`<digi-util-keydown-handler>`} />
              övervakar händelser och skapar events när en tangent trycks ner.
              Se kod-fliken i menyn för att se vilka tangenter som kan användas.
              <br />
              <br />
              <digi-link af-variation="small" afHref="https://digi-core.netlify.app/?path=/docs/util-digi-util-keydown-handler--standard" af-target="_blank">
                <digi-icon-arrow-right slot="icon"></digi-icon-arrow-right>
                Länk till dokumentation i Storybook
              </digi-link>
            </digi-typography-preamble>
          </digi-layout-block>
        </digi-typography>
      </div>
    );
  }
}
