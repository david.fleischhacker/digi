import { Component, h, State } from '@stencil/core';

@Component({
  tag: 'digi-docs-graphics-function-icons',
  styleUrl: 'digi-docs-graphics-function-icons.scss',
  scoped: true
})
export class DigiDocsGraphicsFunctionIcons {
  @State() componentName: string;

  render() {
    return (
      <div class="digi-docs-graphics-function-icons">
        <digi-docs-page-layout>
          <digi-layout-block>
            <p>funktionsikoner</p>
          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}