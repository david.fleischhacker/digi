import { Component, h } from '@stencil/core';

@Component({
  tag: 'digi-docs-news',
  styleUrl: 'digi-docs-news.scss',
  scoped: true
})
export class DigiDocsNews {

  render() {
    return <div>...
    </div>;
  }
}
