import { Component, h, Prop } from '@stencil/core';

@Component({
  tag: 'digi-docs-upgrade-from-digi-ng',
  styleUrl: 'digi-docs-upgrade-from-digi-ng.scss',
  scoped: true
})
export class DigiDocsUpgradeFromDigiNg {
  /**
   * The first name
   */
  @Prop() first: string;

  /**
   * The middle name
   */
  @Prop() middle: string;

  /**
   * The last name
   */
  @Prop() last: string;

  private getText(): string {
    return `${this.first} ${this.middle} ${this.last}`;
  }

  render() {
    return <div>Digi-ng{this.getText()}</div>;
  }
}
