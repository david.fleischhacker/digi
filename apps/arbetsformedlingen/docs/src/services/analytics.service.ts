const accID: string = "54ec8ad8-bfb9-470d-9bd7-fc80034fb519";
const prodID: string = "79af4693-613c-4b70-820c-a351f27c3c00";
const dataLayerName: string = "_mtm";
const appName: string = "Designsystem";
const baseURL: string = 'designsystem.arbetsformedlingen.se';
const config: object = { subtree: true, childList: true };
const isAFDomain: any = () => window.location.href.match(baseURL) ? true : false;
let prevURL = "";
	
(function(window, document, dataLayerName) {
	const id = isAFDomain() ? prodID : accID;
	window[dataLayerName]=window[dataLayerName]||[],window[dataLayerName].push({start:(new Date).getTime(),event:"stg.start"});var scripts=document.getElementsByTagName('script')[0],tags=document.createElement('script');
	function stgCreateCookie(a,b,c){var d="";if(c){var e=new Date;e.setTime(e.getTime()+24*c*60*60*1e3),d=`; expires=${e.toUTCString()}`}document.cookie=`${a}=${b}${d}; path=/`}
	var isStgDebug=(window.location.href.match("stg_debug")||document.cookie.match("stg_debug"))&&!window.location.href.match("stg_disable_debug");stgCreateCookie("stg_debug",isStgDebug?1:"",isStgDebug?14:-1);
	var qP=[];dataLayerName!=="dataLayer"&&qP.push(`data_layer_name=${dataLayerName}`),isStgDebug&&qP.push("stg_debug");var qPString=qP.length>0?(`?${qP.join("&")}`):"";
	tags.async=!0,tags.src=`https://af.containers.piwik.pro/${id}.js${qPString}`,scripts.parentNode.insertBefore(tags,scripts);
	!function(a,n,i):any{a[n]=a[n]||{};
	for(var c=0;c<i.length;c++)!function(i):any{a[n][i]=a[n][i]||{},a[n][i].api=a[n][i].api||
	function(){var a=[].slice.call(arguments,0);"string"==typeof a[0]&&
	window[dataLayerName].push({event:`${n}.${i}:${a[0]}`,parameters:
	[].slice.call(arguments,1)})}}(i[c])}(window,"ppms",["tm","cm"]);
})(window, document, dataLayerName);

export function setAnalytics() {
	const observer = new MutationObserver((): void => {
		if (window.location.href !== prevURL) {
			prevURL = window.location.href;
			let pathname = window.location.pathname;
			
			const trimmedPageTitle = (title: string) => title.split('/').filter(str => str !== '').join(' - ');

			if (trimmedPageTitle(pathname) !== "") {
				window[dataLayerName] = window[dataLayerName] || [],
				window[dataLayerName].push({event: 'virtualPageView',
					pageInfo: {
						pageUri: pathname,
						pageTitle: trimmedPageTitle(pathname),
						applicationName: appName,
					}
				});	
			}
		}
	});
	observer.observe(document, config);
}

export function analyticsClickHandler(val: string) {
	let pathname = window.location.pathname;
	const trimmedPageTitle = (title: string) => title.split('/').filter(str => str !== '').join(' - ');
	window[dataLayerName].push({
		event: 'customEvent',
		eventCategory: 'Feedback',
		eventAction: val,
		eventName: `Feedback - ${val} - ${trimmedPageTitle(pathname)}`,
		eventValue: 0,
		applicationName: appName,
  });
}
