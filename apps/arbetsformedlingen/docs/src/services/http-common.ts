import axios from 'axios';

// const url = 'https://x2six46k.directus.app';;
const url = '';

export default axios.create({
    baseURL: url,
    headers: {
        'Content-type': 'application/json'
    }
})