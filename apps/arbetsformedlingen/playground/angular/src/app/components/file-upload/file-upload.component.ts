import { Component } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import MyFiles from './demo-file';

@Component({
	selector: 'at-file-upload',
	templateUrl: './file-upload.component.html',
	styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent {

	myForm: UntypedFormGroup;

  myFiles = [...MyFiles];

	public resp = [
		{
			file_id: '44d88612fea8a8f36de82e1278abb02f',
			status: 'OK',
			details: {
				confidence: 5,
				malware_family: 54768315,
				malware_type: 114,
				severity: 4,
				signature_name: 'Trojan.Win32.Mitaka.TC.a'
			}
		},

		{
			file_id: '3e74f8be1ca4e3acaf843c8b4f5ef6cc',
			status: 'OK',
			details: {
				confidence: 0,
				severity: 0,
				signature_name: ''
			}
		}
	];

	ngOnInit() {

    // POPULERA MED FILER
    const files = [...this.myFiles];
    this.populateFiles(files);
	}

  async populateFiles(files) {
    await customElements.whenDefined('digi-form-file-upload');
		const fileUploadElement = document.querySelector('digi-form-file-upload');
		await fileUploadElement?.afMImportFiles(files);
  }

	async validateFile(scanResp) {
		await customElements.whenDefined('digi-form-file-upload');
		const fileUploadElement = document.querySelector('digi-form-file-upload');
		await fileUploadElement?.afMValidateFile(scanResp);
	}

	onUpload(e) {
    let file = e.detail;
    file = {
      ...file,
      name: file?.name,
      size: file?.size,
      type: file?.type
    }

		e.preventDefault();
		
		setTimeout(() => {
			const respIndex = Math.floor(Math.random() * 100);

			if(respIndex < 80) {
        file['status'] = 'OK';
			} else{
				file['status'] = 'error';
			}

      file['status'] = 'OK';

			this.validateFile({ ...file });
		}, 500);
	}

	onRemove(e) {
		console.log('onRemove', e);
	}

	onCancel(e) {
		console.log('onCancel', e);
	}

	onRetry(e) {
    console.log('onRetry', e);
		setTimeout(() => {
			const respIndex = Math.floor(Math.random() * 2);
			this.validateFile({ ...this.resp[respIndex], ...e.detail });
		}, 5000);
	}
}
