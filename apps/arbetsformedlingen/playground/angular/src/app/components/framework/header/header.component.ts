import { Component, Input, OnInit } from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';

import { LayoutContainerVariation } from 'arbetsformedlingen-dist';

@Component({
  selector: 'at-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  currentRoute: string;

  constructor(private router: Router) {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.currentRoute = event.url;
      }
    });
  }

  ngOnInit() {}

  @Input() navTemplate: string;

  navItems = [
    {
      label: "Formulär",
      path: "/form"
    },
    {
      label: "Filuppladdaren",
      path: "/fileupload"
    },
    {
      label: "Datavisualisering",
      path: "/charts"
    }
  ]

  isActiveRoute(path: string) {
    return this.currentRoute === path
  }

  LayoutContainerVariation = LayoutContainerVariation;

}
