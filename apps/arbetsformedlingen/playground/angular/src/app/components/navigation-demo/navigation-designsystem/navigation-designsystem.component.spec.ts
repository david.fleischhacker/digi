import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationDesignsystemComponent } from './navigation-designsystem.component';

describe('NavigationDesignsystemComponent', () => {
	let component: NavigationDesignsystemComponent;
	let fixture: ComponentFixture<NavigationDesignsystemComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [NavigationDesignsystemComponent]
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(NavigationDesignsystemComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
