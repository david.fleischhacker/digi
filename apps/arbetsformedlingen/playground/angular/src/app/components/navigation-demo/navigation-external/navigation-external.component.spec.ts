import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationExternalComponent } from './navigation-external.component';

describe('NavigationExternalComponent', () => {
  let component: NavigationExternalComponent;
  let fixture: ComponentFixture<NavigationExternalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavigationExternalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationExternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
