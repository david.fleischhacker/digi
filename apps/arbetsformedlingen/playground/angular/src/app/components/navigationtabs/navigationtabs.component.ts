import { Component, HostListener, OnInit, AfterViewInit } from '@angular/core';
import { DigiNavigationTab, DigiNavigationTabs } from 'dist/libs/arbetsformedlingen-angular';

@Component({
	selector: 'at-navigationtabs',
	templateUrl: './navigationtabs.component.html',
	styleUrls: ['./navigationtabs.component.scss']
})
export class NavigationtabsComponent implements OnInit, AfterViewInit {
	constructor() {
		//something
	}

	ngOnInit(): void {
			return;
	}
	ngAfterViewInit(): void {
		(async () => {
			await customElements.whenDefined('digi-navigation-tabs');

			const testTabs = document.querySelector('digi-navigation-tabs');
			
			if (testTabs) {
				testTabs.afInitActiveTab = 1;
				testTabs.addEventListener("afOnTabsReady", async (e:CustomEvent<number>)=>{
						console.log("afOnTabsReady called");
						console.log(e);
						console.log(e.detail);

						testTabs.afPreventScrollOnFocus = true;
						await testTabs.afMSetActiveTab(1);
						//testTabs.afPreventScrollOnFocus = false;
				});
			}
		})();
	}

	
}
