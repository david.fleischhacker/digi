import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestFormSelectComponent } from './test-form-select.component';

describe('TestFormSelectComponent', () => {
	let component: TestFormSelectComponent;
	let fixture: ComponentFixture<TestFormSelectComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [TestFormSelectComponent]
		}).compileComponents();

		fixture = TestBed.createComponent(TestFormSelectComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
