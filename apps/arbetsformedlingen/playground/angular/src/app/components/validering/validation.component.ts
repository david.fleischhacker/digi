import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'at-validation',
	templateUrl: './validation.component.html',
	styleUrls: ['./validation.component.scss']
})
export class ValidationComponent implements OnInit {
	constructor() {
		// something
	}
	showDialog = false;
	openDialog() {
		this.showDialog = true;
	}
	clickButton() {
		console.log("clickButton");
	}

	ngOnInit(): void {
		return;
	}
}
