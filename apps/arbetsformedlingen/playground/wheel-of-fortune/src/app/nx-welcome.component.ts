import { Component, OnInit, ViewEncapsulation } from '@angular/core';

/* eslint-disable */

@Component({
	selector: 'digi-nx-welcome',
	template: `
		Welcome
	`,
	styles: [],
	encapsulation: ViewEncapsulation.None
})
export class NxWelcomeComponent implements OnInit {
	constructor() {}

	ngOnInit(): void {}
}
