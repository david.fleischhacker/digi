import { FunctionalComponent, h } from '@stencil/core';

export const LayoutComponentGuidelines: FunctionalComponent = () => (
	<digi-notification-detail>
		<h3 slot="heading">Om responsivitet</h3>
		<p>
			Om vi följer några grundläggande mönster för responsiv design, kan vi hålla
			en hög tillgänglighet på alla skärmstorlekar. Individuella komponenter bör
			alltid designas responsivt individuellt, och det finns andra möjliga
			alternativ på lösningar. Men det bör alltid finnas en god motivation för
			avsteg från de grundläggande responsiva beteendena.
		</p>
	</digi-notification-detail>
);
