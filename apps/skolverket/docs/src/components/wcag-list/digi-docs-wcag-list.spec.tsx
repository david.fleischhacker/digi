import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsWcagList } from './digi-docs-wcag-list';

describe('digi-docs-wcag-list', () => {
  it('renders', async () => {
    const {root} = await newSpecPage({
      components: [DigiDocsWcagList],
      html: '<digi-docs-wcag-list></digi-docs-wcag-list>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-wcag-list>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-wcag-list>
    `);
  });

  it('renders with values', async () => {
    const {root} = await newSpecPage({
      components: [DigiDocsWcagList],
      html: `<digi-docs-wcag-list first="Stencil" last="'Don't call me a framework' JS"></digi-docs-wcag-list>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-wcag-list first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-wcag-list>
    `);
  });
});
