export const changelogData = `<digi-expandable-accordion af-heading="[unreleased]"     af-expanded="true">
<h3 id="retry">retry!</h3>
<h3 id="nytt">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/skolverket"></digi-code><ul>
<li><digi-code af-code="digi-form-process-steps"></digi-code>, <digi-code af-code="digi-form-process-step"></digi-code><ul>
<li>Nya komponenter!</li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-toc"></digi-code><ul>
<li>Ny komponent!</li>
</ul>
</li>
<li><digi-code af-code="digi-notification-cookie"></digi-code><ul>
<li>Ny komponent!</li>
</ul>
</li>
<li><digi-code af-code="digi-notification-detail"></digi-code><ul>
<li>Ny komponent!</li>
</ul>
</li>
<li><digi-code af-code="digi-notification-alert"></digi-code><ul>
<li>Nygammal komponent i ny skrud</li>
</ul>
</li>
</ul>
</li>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="digi-icon"></digi-code><ul>
<li>Ny komponent som ersätter tidigare specifika ikonkomponenter. Istället för detta <digi-code af-code="<digi-icon-search>"></digi-code> så gör man nu såhär <digi-code af-code="<digi-icon af-name="search">"></digi-code></li>
</ul>
</li>
</ul>
</li>
</ul>
<h3 id="ändrat">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/styles"></digi-code><ul>
<li>Lagt till <digi-code af-code="li"></digi-code> i <digi-code af-code="_reset.scss"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="digi-form-checkbox"></digi-code><ul>
<li>Ny property <digi-code af-code="afDescription"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="digi-button"></digi-code><ul>
<li>Fix så att <digi-code af-code="afForm"></digi-code> fungerar (mappas nu mot <digi-code af-code="form"></digi-code>-attributet i <digi-code af-code="ref"></digi-code> pga bugg i Stencil)</li>
</ul>
</li>
<li><digi-code af-code="digi-notification-alert"></digi-code>, <digi-code af-code="digi-form-error-list"></digi-code><ul>
<li>Flyttade från Core till Arbetsförmedlingen</li>
</ul>
</li>
<li><digi-code af-code="digi-expandable-accordion"></digi-code><ul>
<li>Ändrat några tokens för att täcka upp Skolverkets behov.</li>
<li>Ny property <digi-code af-code="afVariation"></digi-code>. <digi-code af-code="'primary'"></digi-code> är Arbetsförmedlingens utseende och <digi-code af-code="'secondary'"></digi-code> är Skolverkets utseende</li>
</ul>
</li>
<li><digi-code af-code="digi-expandable-faq"></digi-code>, <digi-code af-code="digi-expandable-faq-item"></digi-code><ul>
<li>Flyttade från Core till Arbetsförmedlingen</li>
</ul>
</li>
<li><digi-code af-code="digi-form-checkbox"></digi-code>, <digi-code af-code="digi-form-radiobutton"></digi-code><ul>
<li>Ny property <digi-code af-code="afLayout"></digi-code>. <digi-code af-code="'block'"></digi-code> är Skolverkets standardversion</li>
</ul>
</li>
<li><digi-code af-code="digi-form-input-search"></digi-code>, <digi-code af-code="digi-form-textarea"></digi-code>, <digi-code af-code="digi-form-label"></digi-code>, <digi-code af-code="digi-form-select"></digi-code>, <digi-code af-code="digi-form-input"></digi-code><ul>
<li>Refaktorerade för att täcka upp Skolverkets behov</li>
</ul>
</li>
<li><digi-code af-code="digi-button"></digi-code><ul>
<li>Småfixar för att täcka upp Skolverkets behov</li>
</ul>
</li>
<li><digi-code af-code="digi-icon-*"></digi-code><ul>
<li>Alla ikoner som inte täcks upp av <digi-code af-code="digi-icon"></digi-code> är nu flyttade från Core till Arbetsförmedlingen</li>
</ul>
</li>
</ul>
</li>
<li><digi-code af-code="@digi/skolverket"></digi-code><ul>
<li><digi-code af-code="digi-notification-alert"></digi-code>, <digi-code af-code="digi-notification-detail"></digi-code><ul>
<li>Korrekt bakgrundsfärg</li>
</ul>
</li>
<li><digi-code af-code="digi-notification-alert-skv"></digi-code><ul>
<li>Bortplockad helt</li>
</ul>
</li>
<li><digi-code af-code="digi-expandable-accordion-skv"></digi-code><ul>
<li>Bortplockad helt</li>
</ul>
</li>
<li><digi-code af-code="digi-form-input-search-skv"></digi-code>, <digi-code af-code="digi-form-textarea-skv"></digi-code>, <digi-code af-code="digi-form-label-skv"></digi-code>, <digi-code af-code="digi-form-select-skv"></digi-code>, <digi-code af-code="digi-form-input-skv"></digi-code><ul>
<li>Bortplockade helt</li>
</ul>
</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.1.2] - 2022-05-18"     af-expanded="true">
<h3 id="ändrat-1">Ändrat</h3>
<ul>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Färgsidan under grafisk profil är uppdaterad enligt varumärket</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.1.1] - 2022-05-17"     af-expanded="true">
<h3 id="ändrat-2">Ändrat</h3>
<ul>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Uppdaterade kodexempel<ul>
<li>Typografikomponenter</li>
<li>Mediakomponenter</li>
<li>Tabellkomponent</li>
</ul>
</li>
<li>Tillgänglighetsförbättringar på logotyplänk</li>
<li>Tillgänglighetslistan har fått funktionalitet för att importera csv-fil</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.1.0] - 2022-05-03"    >
<h3 id="ändrat-3">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li>Live-exempel täcker nu fullbredd</li>
</ul>
</li>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Uppdaterade kodexempel<ul>
<li>Formulärkomponenter</li>
<li>Förloppsmätarkomponenter</li>
<li>Kalenderkomponenten</li>
<li>Kodkomponenter</li>
<li>Kortkomponenter</li>
<li>Logotypkomponenten</li>
<li>Länkkomponenter</li>
<li>Taggkomponenten</li>
<li>Utfällbartkomponenter</li>
</ul>
</li>
<li>Title-texter i webbläsaren sätts nu korrekt</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.0.1] - 2022-04-29"    >
<h3 id="ändrat-4">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="digi-expandable-accordion"></digi-code>, <digi-code af-code="digi-expandable-faq-item"></digi-code>; Lagt in en knapp i rubriken för att interaktion med skärmläsare ska fungera korrekt.</li>
<li><digi-code af-code="digi-button"></digi-code>; Lagt till attributen <digi-code af-code="afAriaControls"></digi-code>, <digi-code af-code="afAriaPressed"></digi-code> och <digi-code af-code="afAriaExpanded"></digi-code>.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[16.0.0] - 2022-04-26"    >
<h3 id="nytt-1">Nytt</h3>
<ul>
<li><p><digi-code af-code="@digi/design-tokens"></digi-code>;<br><em>Designtokens används för att style:a applikationer enligt gemensam nomenklatur och struktur.</em></p>
<ul>
<li>Library som innehåller designtokens i jsonformat och förmåga att exportera dessa i olika format (e.g. CSS, SCSS, JSON, JS, m.fl.).</li>
<li>Designtokens finns i 3 nivåer; (Component-tokens finns bara i <digi-code af-code="@digi/core"></digi-code> och <digi-code af-code="@digi/styles"></digi-code>, <digi-code af-code="@digi/design-tokens"></digi-code> innehåller bara global och brand)<ul>
<li><strong>global</strong> - skalor av färger, avstånd m.m.,</li>
<li><strong>brand</strong> - global tokens applicerade i olika kontexter, t.ex. color text primary, padding medium,</li>
<li><strong>component</strong> - komponenters specifika applicering av brand-tokens, t.ex. button color background primary.</li>
</ul>
</li>
<li>Alla designtokens hanteras i grunden här och inte längre i <digi-code af-code="@digi/styles"></digi-code> (som nu endast konsumerar designtokens i css/scss-format och exporterar vidare).</li>
<li>Olika applikationer kan nyttja designtokens i olika format, dessa format går att konfigurera direkt i <digi-code af-code="@digi/design-tokens"></digi-code> för att kunna exporteras vid behov - synka med designsystemet för att få till er applikations format!</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li>Ny struktur på komponenter som bättre följer en gemensam struktur och arkitektur. i Styles-mappen i komponentens mapp finns två filer främst; <digi-code af-code="<komponent>.variables.scss"></digi-code> (innehåller designtokens för komponenten) och <digi-code af-code="<komponent>.variations.scss"></digi-code> (innehåller mixins som applicerar designtokens beroende på komponentens aktiva variation).</li>
<li><span class="breaking-tag">Breaking</span> Designtokens för komponenter skrivs och hanteras nu direkt i <digi-code af-code="@digi/core"></digi-code> och inte längre som SCSS-filer i styles. Dessa variabler/tokens exporteras dock till <digi-code af-code="@digi/styles"></digi-code> under Components.</li>
<li>Nytt verktyg för att skapa komponenter, kör kommandot <digi-code af-code="npm run generate-component:core"></digi-code> och följ dialogen för att skapa en ny komponent i <digi-code af-code="@digi/core"></digi-code> enligt den nya strukturen, med exempelfiler.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/styles"></digi-code></p>
<ul>
<li><span class="breaking-tag">Breaking</span> Tagit bort filerna <digi-code af-code="_entry.scss"></digi-code> och <digi-code af-code="digi.scss"></digi-code>, och ersatt dessa med <digi-code af-code="digi-styles.custom-properties.scss"></digi-code> (för alla designtokens i css/scss-format), <digi-code af-code="digi-styles.utilities.scss"></digi-code> (för alla functions och mixins, e.g. a11y--sr-only) samt <digi-code af-code="digi-styles.scss"></digi-code>(för allt från <digi-code af-code="@digi/styles"></digi-code> samtidigt, ersätter i princip <digi-code af-code="digi.scss"></digi-code>).</li>
<li>CSS-fil som innehåller alla designtokens/custom-properties (global, brand och component) samt CSS-fil som innehåller alla utility-classes finns under dist-mappen.</li>
</ul>
</li>
</ul>
<h3 id="ändrat-5">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><span class="breaking-tag">Breaking</span> Alla enums för storlekar (S, M och L) har nu döpts om till sina fullständiga namn (SMALL, MEDIUM och LARGE).<br><strong>Se till att uppdatera era applikationer där ni använder dessa!</strong></li>
<li><span class="breaking-tag">Breaking</span> Alla komponenters custom properties/designtokens har fått uppdaterade namn. Dessa kan ni se under respektive komponent på dokumentations-sajten.<br><strong>Se till att uppdatera era applikationer där ni använder dessa!</strong></li>
<li>Använder nu nya filer i <digi-code af-code="@digi/styles"></digi-code> för variables och utilities. Laddar in css-variabler i roten/global.</li>
<li><span class="breaking-tag">Breaking</span> Ikoner har fått uppdaterad struktur för sina designtokens. Nu behöver du override:a variabeln direkt på komponenten, precis som för alla andra komponenter. Om du har en dynamisk ikon så kan du hitta den i css via t.ex. <digi-code af-code="[slot^='icon']"></digi-code> (för både icon och icon-secondary).<br><strong>Se till att uppdatera era applikationer där ni använder dessa!</strong></li>
<li><span class="breaking-tag">Breaking</span> Följande komponenter har fått uppdaterade enums: <digi-code af-code="digi-button"></digi-code>, <digi-code af-code="digi-calendar"></digi-code>, <digi-code af-code="digi-form-input"></digi-code>, <digi-code af-code="digi-form-input-search"></digi-code>, <digi-code af-code="digi-form-select"></digi-code>, <digi-code af-code="digi-form-textarea"></digi-code>, <digi-code af-code="digi-link"></digi-code>, <digi-code af-code="digi-link-button"></digi-code>, <digi-code af-code="digi-link-internal"></digi-code>, <digi-code af-code="digi-link-external"></digi-code>, <digi-code af-code="digi-logo"></digi-code>, <digi-code af-code="digi-notification-alert"></digi-code>, <digi-code af-code="digi-table"></digi-code>, <digi-code af-code="digi-tag"></digi-code>, <digi-code af-code="digi-typography"></digi-code>, <digi-code af-code="digi-breakpoint-observer"></digi-code>.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/styles"></digi-code></p>
<ul>
<li><span class="breaking-tag">Breaking</span> Innehåller numera inga SCSS-variabler för komponenterna utan dessa är ersatta med customproperties (css-variabler). Källan till dessa är flyttade till respektive komponent i <digi-code af-code="@digi/core"></digi-code> och exporteras sedan tillbaka till <digi-code af-code="@digi/styles"></digi-code>.</li>
<li>Värdet på flera variabler har justerats, t.ex. avstånd och vissa färger.</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentationen"></digi-code></p>
<ul>
<li>Använder nya designtokens och uppdatera komponenter.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[15.0.0] - 2022-04-12"    >
<h3 id="nytt-2">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li>Nya ikoner!</li>
</ul>
</li>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Lagt till en sida för release notes.</li>
<li>Förbättrade kontaktuppgifter enligt tillgänglighetskrav.</li>
<li>Ny startsida och sida för introduktion av designsystemet.</li>
</ul>
</li>
</ul>
<h3 id="ändrat-6">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="digi-icon-arrow-<up|down|left|right>"></digi-code>; ikonerna för <digi-code af-code="arrow"></digi-code> har bytt namn till <digi-code af-code="digi-icon-chevron-<up|down|left|right>"></digi-code> för att bättre följa namnpraxis. Nya ikoner för arrow införda i dess ställe.
<strong>Glöm inte att byta namn på ikonerna om du lagt till dom manuellt i din applikation.</strong></li>
<li><digi-code af-code="digi-media-image"></digi-code>; Fixat fel som uppstod om man använder komponenten som <digi-code af-code="af-unlazy"></digi-code> utan att ange specifik höjd och bredd på bilden.</li>
<li><digi-code af-code="digi-icon-spinner"></digi-code>; problem med ikonen fixat.</li>
</ul>
</li>
<li><digi-code af-code="Dokumentation"></digi-code><ul>
<li>Förbättrat live-exempel för informationsmeddelanden.</li>
<li>Fixat problem med laddning av bilder på dokumentations-sajten.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[14.0.1] - 2022-04-11"    >
<h3 id="ändrat-7">Ändrat</h3>
<ul>
<li><digi-code af-code="@digi/core-angular"></digi-code><ul>
<li><digi-code af-code="digi-progress-steps"></digi-code> och <digi-code af-code="digi-progress-step"></digi-code> saknades i core-angular. Problemet är patchat.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[14.0.0] - 2022-03-29"    >
<h3 id="nytt-3">Nytt</h3>
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="digi-progress-steps"></digi-code>; Ny komponent för att visualisera ett användarflöde.</li>
<li><digi-code af-code="digi-progress-step"></digi-code>; Ny komponent som används tillsammans med <digi-code af-code="digi-progress-steps"></digi-code>. Detta är varje steg i flödet.</li>
</ul>
</li>
</ul>
<h3 id="ändrat-8">Ändrat</h3>
<ul>
<li><p><digi-code af-code="Dokumentation"></digi-code></p>
<ul>
<li>Rättat länk till mediabanken.</li>
<li>Navigationen på dokumentationssidan använder sig av uppdateringarna i <digi-code af-code="digi-navigation-vertical-menu"></digi-code> och <digi-code af-code="digi-navigation-vertical-menu-item"></digi-code>.</li>
<li>Tagit bort dubbla landmärken runt navigationen.</li>
<li>Fixat valideringsfel på navigationen.</li>
<li>Uppdaterat tillgänglighetsredogörelsen.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-navigation-vertical-menu"></digi-code>; Layout är uppdaterad.</li>
<li><digi-code af-code="digi-navigation-vertical-menu-item"></digi-code>; Layout är uppdaterad.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[13.2.3] - 2022-03-25"    >
<ul>
<li><digi-code af-code="@digi/core-angular"></digi-code><ul>
<li><digi-code af-code="digi-icon-accessibility-universal"></digi-code>, <digi-code af-code="digi-icon-download"></digi-code>, <digi-code af-code="digi-icon-redo"></digi-code>, <digi-code af-code="digi-icon-trash"></digi-code> saknades i core-angular. Problemet är patchat.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[13.2.2] - 2022-03-24"    >
<ul>
<li><digi-code af-code="@digi/core"></digi-code><ul>
<li><digi-code af-code="Code"></digi-code> typen för Code exporterades inte korrekt och skapade problem vid användning av Core i vissa sammanhang. Problemet är patchat.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[13.2.1] - 2022-03-23"    >
<ul>
<li><digi-code af-code="@digi/core-angular"></digi-code><ul>
<li>Tillåter alla peer dependency versioner av angular-paketen för version 12 och 13 och undviker därmed Conflicting Peer Dependency-felet när du gör npm install.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[13.2.0] - 2022-03-22"    >
<h3 id="ändrat-9">Ändrat</h3>
<ul>
<li><p><digi-code af-code="Dokumentationen"></digi-code></p>
<ul>
<li>Ändrat till Stencil Router V2 för att lösa buggar med bl.a. lazy-loadade bilder i dokumentationsapplikationen. (Problem med bildladdning kvarstår för vissa användare och kommer åtgärdas till nästa release.)</li>
<li>Komponentdokumentationen för <digi-code af-code="digi-link-button"></digi-code>, <digi-code af-code="digi-button"></digi-code> och <digi-code af-code="digi-info-card"></digi-code> är uppdaterad med den nya versionen av kodexemepelkomponenten</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-code-example"></digi-code>; förbättrad funktionalitet och möjlighet att kunna växla mellan olika varianter av exempelkomponenten i demoytan.</li>
<li><digi-code af-code="digi-code"></digi-code>; ändrat bakgrundsfärg från gråsvart till mörkblå, samt gjort den ljusa varianten till standard.</li>
<li><digi-code af-code="digi-code-block"></digi-code>; ändrat bakgrundsfärg från gråsvart till mörkblå.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-angular"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/styles"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-fonts"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[13.1.2] - 2022-03-18"    >
<h3 id="ändrat-10">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-angular"></digi-code></p>
<ul>
<li>La till komponenter under Utfällbart (expandable).</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/styles"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-fonts"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[13.1.1] - 2022-03-16"    >
<h3 id="ändrat-11">Ändrat</h3>
<ul>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-navigation-pagination"></digi-code>; aktiv sida markerades inte korrekt om man har väldigt många sidor.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-angular"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/styles"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-fonts"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[13.1.0] - 2022-03-10"    >
<h3 id="nytt-4">Nytt</h3>
<ul>
<li><p><digi-code af-code="Changelog"></digi-code></p>
<ul>
<li>Ny gemensam changelog för hela monorepot. All information om driftsättningar kommer skrivas i samma dokument för att hålla ihop versionsnummer och gemensamma ändringar.</li>
</ul>
</li>
<li><p><digi-code af-code="Dokumentationen"></digi-code></p>
<ul>
<li>Testmetoder; ny sida under Tillgänglighet och design.</li>
<li>Ny förbättrad caching av sidan. Laddar resurser i bakgrunden och tillåter bl.a. offline-läge och snabbare sidladdning.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li><digi-code af-code="digi-icon-trash"></digi-code>; ny ikon som illustrerar en soptunna.</li>
<li><digi-code af-code="digi-icon-accessibility-universal"></digi-code>; ny ikon som illustrerar tillgänglighet.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-angular"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/styles"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-fonts"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
</ul>
<h3 id="ändrat-12">Ändrat</h3>
<ul>
<li><p><digi-code af-code="Dokumentationen"></digi-code></p>
<ul>
<li>Tillgänglighetslistan; ny förbättrad funktionalitet för att kunna följa upp status och kommentarer på olika krav samt bättre filtrering. Även ökad tillgänglighet.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-angular"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/styles"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-fonts"></digi-code></p>
<ul>
<li>n/a</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[13.0.0] - 2022-03-08"    >
<h3 id="nytt-5">Nytt</h3>
<ul>
<li><p><digi-code af-code="@digi/core-angular"></digi-code></p>
<ul>
<li>Nu helt releasad och har gått ur beta.</li>
</ul>
</li>
<li><p><digi-code af-code="@digi/core-fonts"></digi-code></p>
<ul>
<li>Nytt bibliotek för hantering av Arbetsförmedlingens typsnitt.</li>
</ul>
</li>
</ul>
<h3 id="ändrat-13">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-button"></digi-code><ul>
<li>Lagt till saknade design tokens samt dokumenterat tokens.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[12.6.1] - 2022-02-18"    >
<ul>
<li><digi-code af-code="digi-form-select"></digi-code><ul>
<li><digi-code af-code="afStartSelected"></digi-code> är deprecated. Använd <digi-code af-code="afValue"></digi-code> istället.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[12.6.0] - 2022-02-22"    >
<h3 id="nytt-6">Nytt</h3>
<ul>
<li><p><digi-code af-code="digi-expandable-accordion"></digi-code></p>
<ul>
<li>Ny komponent</li>
</ul>
</li>
<li><p><digi-code af-code="digi-expandable-faq"></digi-code></p>
<ul>
<li>Ny komponent</li>
</ul>
</li>
<li><p><digi-code af-code="digi-expandable-faq-item"></digi-code></p>
<ul>
<li>Ny komponent</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[12.5.0] - 2022-02-22"    >
<h3 id="ändrat-14">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-link-(internal|external)"></digi-code><ul>
<li>Justerat så att digi-link-internal och digi-link-external använder sig av digi-link för att de ska ärva regler och fungera lika.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[12.4.0] - 2022-02-15"    >
<ul>
<li><digi-code af-code="digi-typography"></digi-code><ul>
<li>Lagt till maxbredd för rubriker och länkar.</li>
<li>Lagt en l-version för maxbredd.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[12.3.1] - 2022-02-09"    >
<ul>
<li><digi-code af-code="digi-button, digi-link-(internal|external)"></digi-code><ul>
<li>Justerat positionering av ikoner och dess storlek för knappar och länkar.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[12.3.0] - 2022-02-09"    >
<ul>
<li><digi-code af-code="digi-navigation-tabs"></digi-code><ul>
<li>Lagt till en publik metod för att ändra aktiv flik.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[12.1.1-beta.5] - 2022-01-20"    >
<ul>
<li><digi-code af-code="value"></digi-code><ul>
<li>Value och afValue sätts vid afOnInput så attributet korrekt speglas vid target.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[11.2.1-beta.0] - 2022-01-19"    >
<ul>
<li><digi-code af-code="digi-form-radiobutton"></digi-code><ul>
<li>Fixat bugg där radiogroup inte stöds. Fungerar nu på det sätt du önskar!</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[10.2.1] - 2022-01-19"    >
<h3 id="ändrat-15">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-media-image"></digi-code><ul>
<li>Fixat bugg med hur platshållaren fungerar</li>
<li>Ändrat bakgrunsfärg på platshållaren</li>
<li>Lagt till så attributet loading används på bilden. Sätts till <digi-code af-code="lazy"></digi-code> eller <digi-code af-code="eager"></digi-code> beroende på om man använder <digi-code af-code="afUnLazy"></digi-code> eller inte</li>
<li>Bilden går att ändra dynamiskt</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[12.0.0] - 2022-01-18"    >
<ul>
<li><digi-code af-code="version"></digi-code><ul>
<li>Ökade versionsnumret till 12 för att undvika problem med paket i npm.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[10.2.0] - 2022-01-18"    >
<ul>
<li><p><digi-code af-code="digi-layout-block"></digi-code></p>
<ul>
<li>Lade till attributet af-vertical-padding för att kunna addera padding inuti container-elementet.</li>
<li>Lade till attributen af-margin-top och af-margin-bottom för att kunna addera marginal uppe och/eller nere på container-elementet.</li>
</ul>
</li>
<li><p><digi-code af-code="digi-layout-container"></digi-code></p>
<ul>
<li>Lade till attributet af-vertical-padding för att kunna addera padding inuti elementet.</li>
<li>Lade till attributen af-margin-top och af-margin-bottom för att kunna addera marginal uppe och/eller nere på elementet.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[11.1.0] - 2022-01-10"    >
<ul>
<li><p><digi-code af-code="enums"></digi-code></p>
<ul>
<li>Enums finns numera under <digi-code af-code="@digi/core"></digi-code> och fungerar med bl.a. Angular.</li>
</ul>
</li>
<li><p><digi-code af-code="types"></digi-code></p>
<ul>
<li>Ändrat types från <digi-code af-code="components.d.ts"></digi-code> till <digi-code af-code="index.d.ts"></digi-code> som både exporterar components samt enums. På components finns namespace Components som kan användas t.ex. som <digi-code af-code="Components.DigiButton"></digi-code>. Förberett för interfaces om/när det ska användas och exponeras.</li>
</ul>
</li>
<li><p><digi-code af-code="output-target"></digi-code></p>
<ul>
<li>Använder nu <digi-code af-code="dist-custom-elements"></digi-code> istället för <digi-code af-code="dist-custom-elements-bundle"></digi-code> som blivit deprecated.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[10.1.3] - 2022-01-17"    >
<h3 id="ändrat-16">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-form-input"></digi-code>, <digi-code af-code="digi-form-textarea"></digi-code><ul>
<li>Tagit bort kontroll av &#39;dirty&#39; och &#39;touched&#39; av formulärelement vilket gör det mer flexibelt att välja när felmeddelanden ska visas.</li>
</ul>
</li>
<li><digi-code af-code="digi-form-select"></digi-code><ul>
<li>Använder sig av <digi-code af-code="digi-util-mutation-observer"></digi-code> för att se om options-lista ändras programmatiskt.</li>
</ul>
</li>
<li><digi-code af-code="digi-form-fieldset"></digi-code><ul>
<li>Lagt in möjlighet att sätta id på komponenten. Väljer man inget sätts en slumpmässig id.</li>
</ul>
</li>
<li><digi-code af-code="digi-media-image"></digi-code><ul>
<li>Löst bugg som gjorde att komponenten inte använde den bredd och höjd man angett.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[11.0.0] - 2021-12-23"    >
<ul>
<li><p><digi-code af-code="digi-form-*"></digi-code></p>
<ul>
<li>Justerat formulärkomponenterna så att de kan använda <digi-code af-code="value"></digi-code> istället för <digi-code af-code="afValue"></digi-code> (som nu är markerat deprecated). Även <digi-code af-code="checked"></digi-code> ersätter <digi-code af-code="afChecked"></digi-code>. Detta är gjort för att bättre fungera med Angular samt det nya biblioteket Digi Core Angular som är påväg ut.</li>
</ul>
</li>
<li><p><digi-code af-code="digi-form-select"></digi-code></p>
<ul>
<li>Ny logik för att hitta valt värde och skicka rätt event enligt samma practice som de andra formulärelementen.</li>
</ul>
</li>
<li><p><digi-code af-code="digi-form-radiobutton"></digi-code></p>
<ul>
<li>Tog bort <digi-code af-code="afChecked"></digi-code> och använder nu <digi-code af-code="afValue"></digi-code>för att initialt sätta ett värde. När <digi-code af-code="value"></digi-code> är samma som <digi-code af-code="afValue"></digi-code> så är radioknappen icheckad (följer bättre radiogroup).</li>
</ul>
</li>
<li><p><digi-code af-code="övrigt"></digi-code></p>
<ul>
<li>Digi Core är nu förberett för att kunna exportera filer via output-target till Angular. Ett nytt bibliotek <digi-code af-code="@digi/core-angular"></digi-code> kommer snart som är till för angular-appar. Det här biblioteket wrappar Digi Core och gör det enklare att använda t.ex. Reactive Forms.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[10.1.2] - 2021-12-15"    >
<h3 id="ändrat-17">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-typography"></digi-code><ul>
<li>Lagt till så ul- och dl-listor får samma textstorlek som stycken och även samma hantering för att inte få för långa textrader</li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-pagination"></digi-code><ul>
<li>Justerat så att primär och sekundär variant på knapparna används korrekt. För att ändra utseende på knapparna så behöver knapparnas original-variabler ändras direkt, t.ex. <digi-code af-code="--digi-button--background"></digi-code>, dock finns variabler för t.ex. width, padding, m.m.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[10.1.1] - 2021-12-10"    >
<h3 id="ändrat-18">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-form-filter"></digi-code><ul>
<li>Löst en bugg där komponenten tidigare fungerade felaktigt om någon kryssruta var aktiv vid sidladdning.</li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-tabs"></digi-code><ul>
<li>Lagt in stöd så komponenten känner av om man lägger till eller tar bort <digi-code af-code="digi-navigation-tab"></digi-code>-komponenter för att kunna dynamiskt ändra antal flikar.</li>
</ul>
</li>
<li><digi-code af-code="digi-info-card"></digi-code><ul>
<li>Tagit bort felaktig marginal på rubriken.</li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-tab"></digi-code><ul>
<li>Ändrat så fokusram på en panel endast markeras när man navigerar dit med skärmläsare.</li>
</ul>
</li>
<li><digi-code af-code="digi-form-filter"></digi-code><ul>
<li>Fixat så att komponenten fungerar korrekt även om man använder den inuti <digi-code af-code="digi-navigation-tab"></digi-code>. Tidigare stängdes listan när man försökte klicka på en kryssruta.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[10.1.0] - 2021-11-23"    >
<h3 id="nytt-7">Nytt</h3>
<ul>
<li><digi-code af-code="digi-icon-exclamation-triangle-warning"></digi-code><ul>
<li>Lagt till en ny ikon. Denna ikon används bl.a. för varningsmeddelanden i formulär. Tidigare behövdes den ikonen läsas in som en asset i projektet, nu kommer inte detta behövas.</li>
</ul>
</li>
</ul>
<h3 id="ändrat-19">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-link-button"></digi-code><ul>
<li>Uppdaterat komponenten så man kan välja mellan olika storlekar.</li>
<li>Uppdaterat så alla layouter som finns i UI-kit också går att använda i kod.</li>
</ul>
</li>
<li><digi-code af-code="digi-form-input"></digi-code>, <digi-code af-code="digi-form-select"></digi-code>, <digi-code af-code="digi-form-textarea"></digi-code><ul>
<li>Korrigerat så layout av formulärelement när de indikerar &quot;fel&quot;, &quot;varning&quot; och &quot;korrekt&quot; följer UI-kit.</li>
</ul>
</li>
<li><digi-code af-code="digi-icon-arrow-down"></digi-code><ul>
<li>Korrigerat layout så den följer UI-kit</li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-breadcrumbs"></digi-code><ul>
<li>Ändrat uppdelaren mellan länkar till &#39;/&#39;, istället för &#39;&gt;&#39;.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[10.0.0] - 2021-10-19"    >
<h3 id="ändrat-20">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-navigation-menu"></digi-code><ul>
<li>Allt innehåll i Navigation Menu ska ligga i en <digi-code af-code="<ul>"></digi-code>-tagg och listas med respektive <digi-code af-code="<li>"></digi-code>-taggar.</li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-menu-item"></digi-code><ul>
<li>En Navigation Menu Item som ska agera som expanderbart menyalternativ ska följas av en <digi-code af-code="<ul>"></digi-code>-tagg, t.ex.</li>
</ul>
<digi-code-block af-code="<digi-navigation-menu-item></digi-navigation-menu-item>
  <ul>...</ul>"></digi-code-block>
<p>För att göra raden expanderbar måste attributet <digi-code af-code="af-active-subnav"></digi-code> användas med antingen <digi-code af-code="true"></digi-code> för för-expanderad eller <digi-code af-code="false"></digi-code> för stängd.</p>
</li>
<li><digi-code af-code="digi-layout-media"></digi-code><ul>
<li>Lagt till token <digi-code af-code="--digi-layout-media-object--flex-wrap"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="digi-progressbar"></digi-code><ul>
<li>Lagt till möjlighet att tända valfria steg i komponenten, som följer ordningen på vilka formulär-element som är gjorda.</li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-pagination"></digi-code><ul>
<li>Lagt till en publik metod för att ändra aktivt steg i komponenten.</li>
</ul>
</li>
<li><digi-code af-code="digi-form-error-list"></digi-code><ul>
<li>Lade till default värde på linkItems för att undvika JS fel vid tom lista</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.4.6] - 2021-09-30"    >
<h3 id="ändrat-21">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-code-example"></digi-code><ul>
<li>Ändrat bakgrundsfärg på vertygslistan</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.4.5] - 2021-09-28"    >
<h3 id="ändrat-22">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-form-input-search"></digi-code><ul>
<li>Lagt till möjlighet att dölja knappen</li>
<li>Buggfix, knappens text gick inte att ändra</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.4.4] - 2021-09-24"    >
<h3 id="ändrat-23">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-form-error-list"></digi-code><ul>
<li>Ändrat färgen på notifikationen från &quot;info&quot; till &quot;danger&quot;</li>
</ul>
</li>
<li><digi-code af-code="digi-notification-alert"></digi-code><ul>
<li>Justerat padding för medium storlek</li>
</ul>
</li>
<li><digi-code af-code="digi-form-validation-message"></digi-code><ul>
<li>Ändrat default varningsmeddelande till att vara tomt</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.4.3] - 2021-09-16"    >
<h3 id="ändrat-24">Ändrat</h3>
<ul>
<li><digi-code af-code="design-tokens"></digi-code><ul>
<li>Ändrat färgen <digi-code af-code="$digi--ui--color--green"></digi-code></li>
<li>Ändrat färgen <digi-code af-code="$digi--ui--color--pink"></digi-code></li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.4.2] - 2021-08-27"    >
<h3 id="ändrat-25">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-form-filter"></digi-code><ul>
<li>Lagt till afOnChange EventEmitter</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.4.1] - 2021-08-18"    >
<h3 id="ändrat-26">Ändrat</h3>
<ul>
<li>Lagt till en ny outputTarget <digi-code af-code="dist-custom-elements-bundle"></digi-code></li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.4.0] - 2021-07-02"    >
<h3 id="nytt-8">Nytt</h3>
<ul>
<li><digi-code af-code="digi-progressbar"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
</ul>
<h3 id="ändrat-27">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-form-radiobutton"></digi-code><ul>
<li>Korrigeringar av layout</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.3.0] - 2021-06-18"    >
<h3 id="ändrat-28">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-navigation-pagination"></digi-code><ul>
<li>Skriv inte ut pagineringen om det bara är en sida. Däremot visas fortfarande texten &quot;Visar 1-15 av XXX&quot;.</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.2.1] - 2021-06-15"    >
<h3 id="ändrat-29">Ändrat</h3>
<ul>
<li>Lagt till alla enums under <digi-code af-code="@digi/core/dist/enum/"></digi-code></li>
<li>Korrigerat felaktigheter i alla readmefiler. Främst gällande felaktiga paths till enum-importer.</li>
<li>Lagt till enum-mappen till disten.</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.2.0] - 2021-06-15"    >
<h3 id="nytt-9">Nytt</h3>
<ul>
<li><digi-code af-code="digi-calendar"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
<li><digi-code af-code="digi-typography-meta"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
<li><digi-code af-code="digi-logo"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
<li><digi-code af-code="digi-util-mutation-observer"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
<li><digi-code af-code="digi-form-radiobutton"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
</ul>
<h3 id="ändrat-30">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-form-fieldset"></digi-code><ul>
<li>Ändrade padding i fieldset till 0 för att linjera med komponenter utanför fieldset</li>
</ul>
</li>
<li><digi-code af-code="digi-form-error-list"></digi-code><ul>
<li>Lagt till mutation observer för att se ändringar i komponenten</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.1.0] - 2021-05-25"    >
<h3 id="nytt-10">Nytt</h3>
<ul>
<li><digi-code af-code="digi-util-breakpoint-observer"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
<li><digi-code af-code="digi-form-select"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
<li><digi-code af-code="digi-form-fieldset"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
<li><digi-code af-code="digi-tag"></digi-code><ul>
<li>Ny komponent</li>
</ul>
</li>
</ul>
<h3 id="ändrat-31">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-navigation-pagination"></digi-code><ul>
<li>Felplacering av ikon i knapparna föregående/nästa vid navigering av siffer-knapparna.</li>
<li>Centrering av text i siffer-knapparna efter css-reset tagits bort</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[9.0.0] - 2021-05-04"    >
<h3 id="ändrat-32">Ändrat</h3>
<ul>
<li><digi-code af-code="digi-core"></digi-code><ul>
<li>Lagt till ny docs tag <digi-code af-code="@enums"></digi-code> i alla komponenter</li>
</ul>
</li>
<li><digi-code af-code="digi-media-figure"></digi-code><ul>
<li>Ändrat enum <digi-code af-code="MediaFigureAlign"></digi-code> till <digi-code af-code="MediaFigureAlignment"></digi-code></li>
<li>Ändrat prop <digi-code af-code="afAlign"></digi-code> till <digi-code af-code="afAlignment"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="digi-form-error-list"></digi-code><ul>
<li>Ändrat enum <digi-code af-code="ErrorListHeadingLevel"></digi-code> till <digi-code af-code="FormErrorListHeadingLevel"></digi-code></li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[8.1.0] - 2021-05-03"    >
<h3 id="nytt-11">Nytt</h3>
<ul>
<li><digi-code af-code="digi-core"></digi-code><ul>
<li>Lagt till alla <digi-code af-code="enums"></digi-code> till paketet. Dessa hittas under <digi-code af-code="@digi/core/enum/mitt-enum-namn.emum.ts"></digi-code></li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[8.0.0] - 2021-04-30"    >
<h3 id="changed">Changed</h3>
<ul>
<li><digi-code af-code="digi-core"></digi-code><ul>
<li>Ändrat all dokumentation inne i komponenterna till svenska</li>
</ul>
</li>
<li><digi-code af-code="digi-form-filter"></digi-code><ul>
<li>Ändrat event <digi-code af-code="afOnSubmittedFilter"></digi-code> till <digi-code af-code="afOnSubmitFilter"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="digi-form-input-search"></digi-code><ul>
<li>Ändrat prop <digi-code af-code="afButtonLabel"></digi-code> till <digi-code af-code="afButtonText"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-tabs"></digi-code><ul>
<li>Ändrat prop <digi-code af-code="afTablistAriaLabel"></digi-code> till <digi-code af-code="afAriaLabel"></digi-code></li>
<li>Ändrat prop <digi-code af-code="afInitActiveTabIndex"></digi-code> till <digi-code af-code="afInitActiveTab"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-pagination"></digi-code><ul>
<li>Ändrat prop <digi-code af-code="afStartPage"></digi-code> till <digi-code af-code="afInitActivePage"></digi-code></li>
</ul>
</li>
<li><digi-code af-code="dgi-notification-alert"></digi-code><ul>
<li>Ändrat event <digi-code af-code="afOnCloseAlert"></digi-code> till <digi-code af-code="afOnClose"></digi-code></li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[7.2.0] - 2021-04-23"    >
<h3 id="added">Added</h3>
<ul>
<li><digi-code af-code="digi-notification-alert"></digi-code><ul>
<li>Created component</li>
</ul>
</li>
<li><digi-code af-code="digi-form-error-list"></digi-code><ul>
<li>Created component</li>
</ul>
</li>
<li><digi-code af-code="digi-link"></digi-code><ul>
<li>Created component</li>
</ul>
</li>
</ul>
<h3 id="changed-1">Changed</h3>
<ul>
<li><digi-code af-code="digi-layout-media-object"></digi-code><ul>
<li>Included Stretch alignment</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[7.1.0] - 2021-04-19"    >
<h3 id="added-1">Added</h3>
<ul>
<li><digi-code af-code="digi-typography-time"></digi-code><ul>
<li>Created component</li>
</ul>
</li>
</ul>
<h3 id="changed-2">Changed</h3>
<ul>
<li><digi-code af-code="Navigation-pagination"></digi-code> <digi-code af-code="Navigation-tabs"></digi-code> <digi-code af-code="Form-filter"></digi-code> <digi-code af-code="Form-textarea"></digi-code> <digi-code af-code="Form-input"></digi-code> <digi-code af-code="Form-input-search"></digi-code><ul>
<li>Solved style issues after removal of base-reset</li>
</ul>
</li>
</ul>
</digi-expandable-accordion af-heading>
<digi-expandable-accordion af-heading="[7.0.0] - 2021-04-19"    >
<h3 id="added-2">Added</h3>
<ul>
<li><digi-code af-code="digi-navigation-context-menu"></digi-code><ul>
<li>Created component</li>
</ul>
</li>
<li><digi-code af-code="digi-navigation-breadcrumbs"></digi-code><ul>
<li>Created component</li>
</ul>
</li>
</ul>
<h3 id="breaking-changes">Breaking Changes</h3>
<ul>
<li><strong>Removed reset css from all components</strong><ul>
<li>Previously, all components included a reset css to prevent global styles from leaking in. This created a massive duplication of css and is now removed. Every consuming app is now responsible for handling css leakage. We are continuously correcting css problems that this may have caused inside the components themselves. If you encounter any problems, please report them to us.</li>
</ul>
</li>
<li><strong>Removed <digi-code af-code="global"></digi-code> folder</strong><ul>
<li>All uncompiled scss is being moved to a new library called <digi-code af-code="@digi/styles"></digi-code>, which will very soon be available from the package manager. In the meantime, you can use the old <digi-code af-code="@af/digi-core"></digi-code>-package.</li>
</ul>
</li>
</ul>
<h3 id="changed-3">Changed</h3>
<ul>
<li><digi-code af-code="digi-button"></digi-code><ul>
<li>Included hasIconSecondary within handleErrors function</li>
<li>Added outline and text-align variables</li>
</ul>
</li>
</ul>

</digi-expandable-accordion af-heading>
`