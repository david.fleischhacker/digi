import { Component, Prop, h, State, Fragment } from '@stencil/core';
import {
	CodeExampleLanguage,
	CodeBlockVariation,
	CodeBlockLanguage,
	FormSelectVariation
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-code-block-details',
	scoped: true
})
export class DigiCodeBlockDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() codeBlockVariation: CodeBlockVariation = CodeBlockVariation.DARK;
	@State() codeBlockLanguage: CodeBlockLanguage = CodeBlockLanguage.HTML;
	@State() language: string = 'html';
	@State() codeBlockLanguages = {
		html:
			'<digi-typography><p>Detta är ett standardkodblock.</p></digi-typography>',
		javascript: `\
function foo(bar) {
  if (foo + 2 === 3) {
    return true // This is true if foo is 1;
  } else {
    return 'Wrong!';
  }
}`,
		typescript: `\
class SomeClass extends SomeOtherClass {
 @MyDecorator()
  myMethod(arg: MyType[]):void {
    if(arg[1].user?.name === 'Tesla') {
      console.log('username is Tesla');
    }
  }
}`,
		css: `\
.wrapper {
  background: var(--digi--ui--color--background);
  color: var(--digi--typography--color--text);
}`,
		scss: `\
@import 'some-mixin';
.wrapper {
  @include some-mixin($condition: false);
  background: $someVariable;
  color: $someOtherVariable;
}`,
		json: `\
{
  key: val,
  otherKey: 'stringVal',
    someKey: {
      nestedKey: nestedVal
    }
}`,
		bash: `\
npm install some-package
cd some-package
npm run start`
	};

	get codeBlockCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-code
  af-language="${this.language}"
  af-code="${this.codeBlockLanguages[this.language]}"
  af-variation="${Object.keys(CodeBlockVariation)
			.find((key) => CodeBlockVariation[key] === this.codeBlockVariation)
			.toLocaleLowerCase()}"
  >
</digi-code>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-code
  [attr.af-language]="CodeBlockLanguage.${Object.keys(CodeBlockLanguage).find(
			(key) => CodeBlockLanguage[key] === this.language
		)}"
  [attr.af-code]="${this.codeBlockLanguages[this.language]}"
  [attr.af-variation]="CodeBlockVariation.${Object.keys(
			CodeBlockVariation
		).find((key) => CodeBlockVariation[key] === this.codeBlockVariation)}"
  >
</digi-code>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.codeBlockCode)}
				afHideControls={this.afHideControls}
				afHideCode={this.afHideCode}
			>
				{Object.keys(this.codeBlockLanguages).map((key) => {
					if (key === this.language) {
						return (
							<div style={{ width: '100%' }}>
								<digi-code-block
									afVariation={CodeBlockVariation[this.codeBlockVariation.toUpperCase()]}
									afLanguage={CodeBlockLanguage[key.toUpperCase()]}
									afCode={this.codeBlockLanguages[key]}
									afHideToolbar
								></digi-code-block>
							</div>
						);
					}
				})}
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							afName="Variant"
							afLegend="Variant"
							onChange={(e) => (this.codeBlockVariation = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="Variant"
								afLabel="Ljus"
								afValue={CodeBlockVariation.LIGHT}
								afChecked={this.codeBlockVariation === CodeBlockVariation.LIGHT}
							/>
							<digi-form-radiobutton
								afName="Variant"
								afLabel="Mörk"
								afValue={CodeBlockVariation.DARK}
								afChecked={this.codeBlockVariation === CodeBlockVariation.DARK}
							/>
						</digi-form-fieldset>
						<digi-form-select
							afLabel="Kodspråk"
							af-start-selected="0"
							afVariation={FormSelectVariation.SMALL}
							onAfOnChange={(e) => (this.language = (e.target as any).value)}
						>
							<option value="html">HTML</option>
							<option value="javascript">JavaScript</option>
							<option value="typescript">TypeScript</option>
							<option value="css">CSS</option>
							<option value="scss">SCSS</option>
							<option value="json">JSON</option>
							<option value="bash">Bash</option>
						</digi-form-select>
					</div>
				)}
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Kodblockskomponenten används för att visa förformaterade block med kod.
					Den stödjer ett antal olika språk och finns i både mörkt och ljust tema.
					Komponenten har ett verktygsfält med knappar för att kopiera koden
					direkt till Urklipp."
				example={this.example()}
				description={
					<Fragment>
						<h3>Varianter</h3>
						<p>
							Kodblockskomponenten finns både i ett mörkt och en ljust tema som sätts
							med hjälp av attributet <digi-code af-code="af-variation"></digi-code>.
						</p>
						<h3>Språk</h3>
						<p>
							Kodblockskomponenten stödjer olika språk med hjälp av attributet{' '}
							<digi-code af-code="af-language"></digi-code>.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
