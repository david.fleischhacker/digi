import { Component, Prop, h, State, Fragment } from '@stencil/core';
import {
	CodeExampleLanguage,
	FormSelectValidation,
	FormSelectVariation
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-form-select-details',
	scoped: true
})
export class DigiFormSelectDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() formSelectValidation: FormSelectValidation =
		FormSelectValidation.NEUTRAL;
	@State() formSelectVariation: FormSelectVariation = FormSelectVariation.MEDIUM;
	@State() intialText: string = 'placeholder';
	@State() hasDescription: boolean = false;

	get inputCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
       <digi-form-select
          af-label="Etikett"\ \n\t  ${
											this.intialText === 'placeholder'
												? '    af-placeholder="Platshållare"\n\t   '
												: ' '
										}\ ${
				this.hasDescription
					? '  af-description="Beskrivande text"\n\t\t '
					: ' '
			}${
				this.intialText === 'startSelected' ? ' af-value="val1"\n\t\t  ' : ' '
			}af-validation="${this.formSelectValidation}"\ 
      ${
							this.formSelectValidation !== FormSelectValidation.NEUTRAL
								? '    af-validation-text="Det här är ett valideringsmeddelande"\n\t   >'
								: ' >'
						}
        <option value="val1">Val 1</option>
        <option value="val2">Val 2</option>
       </digi-form-select>`,
			[CodeExampleLanguage.ANGULAR]: `\
       <digi-form-select
          [attr.af-label]="'Etikett'"\ \n\t  ${
											this.intialText === 'placeholder'
												? `    [attr.af-placeholder]="'Platshållare'"\n\t   `
												: ' '
										}\ ${
				this.hasDescription
					? `  [attr.af-description]="'Beskrivande text'"\n\t\t `
					: ' '
			}${
				this.intialText === 'startSelected'
					? ` [attr.af-value="'val1'"\n\t\t  `
					: ' '
			}[attr.af-validation]="FormSelectValidation.${this.formSelectValidation.toUpperCase()}"\ 
      ${
							this.formSelectValidation !== FormSelectValidation.NEUTRAL
								? '    [attr.af-validation-text]="Det här är ett valideringsmeddelande"\n\t   >'
								: ' >'
						}
        <option value="val1">Val 1</option>
        <option value="val2">Val 2</option>
      </digi-form-select>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.inputCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<div
					style={{
						'max-width': '500px'
					}}
				>
					{this.intialText == 'none' && !this.hasDescription && (
						<digi-form-select
							afLabel="Etikett"
							af-validation-text="Det här är ett valideringsmeddelande"
							afVariation={this.formSelectVariation}
							afValidation={this.formSelectValidation}
						>
							<option value="val1">Val 1</option>
							<option value="val2">Val 2</option>
						</digi-form-select>
					)}
					{this.intialText == 'placeholder' && !this.hasDescription && (
						<digi-form-select
							afLabel="Etikett"
							af-placeholder="Platshållare  "
							af-validation-text="Det här är ett valideringsmeddelande"
							afVariation={this.formSelectVariation}
							afValidation={this.formSelectValidation}
						>
							<option value="val1">Val 1</option>
							<option value="val2">Val 2</option>
						</digi-form-select>
					)}
					{this.intialText == 'startSelected' && !this.hasDescription && (
						<digi-form-select
							afLabel="Etikett"
							af-value="val1"
							af-validation-text="Det här är ett valideringsmeddelande"
							afVariation={this.formSelectVariation}
							afValidation={this.formSelectValidation}
						>
							<option value="val1">Val 1</option>
							<option value="val2">Val 2</option>
						</digi-form-select>
					)}
					{this.intialText == 'none' && this.hasDescription && (
						<digi-form-select
							afLabel="Etikett"
							af-description="Beskrivande text"
							af-validation-text="Det här är ett valideringsmeddelande"
							afVariation={this.formSelectVariation}
							afValidation={this.formSelectValidation}
						>
							<option value="val1">Val 1</option>
							<option value="val2">Val 2</option>
						</digi-form-select>
					)}
					{this.intialText == 'placeholder' && this.hasDescription && (
						<digi-form-select
							afLabel="Etikett"
							af-description="Beskrivande text"
							af-placeholder="Platshållare  "
							af-validation-text="Det här är ett valideringsmeddelande"
							afVariation={this.formSelectVariation}
							afValidation={this.formSelectValidation}
						>
							<option value="val1">Val 1</option>
							<option value="val2">Val 2</option>
						</digi-form-select>
					)}
					{this.intialText == 'startSelected' && this.hasDescription && (
						<digi-form-select
							afLabel="Etikett"
							af-description="Beskrivande text"
							af-value="val1"
							af-validation-text="Det här är ett valideringsmeddelande"
							afVariation={this.formSelectVariation}
							afValidation={this.formSelectValidation}
						>
							<option value="val1">Val 1</option>
							<option value="val2">Val 2</option>
						</digi-form-select>
					)}
				</div>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset afLegend="Beskrivande text">
							<digi-form-checkbox
								afLabel="Ja"
								afChecked={this.hasDescription}
								onAfOnChange={() =>
									this.hasDescription
										? (this.hasDescription = false)
										: (this.hasDescription = true)
								}
							></digi-form-checkbox>
						</digi-form-fieldset>
						<digi-form-fieldset
							afName="placeholder"
							afLegend="Initial text"
							onChange={(e) => (this.intialText = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="placeholder"
								afLabel="Platshållare"
								afValue="placeholder"
								afChecked={this.intialText === 'placeholder'}
							/>
							<digi-form-radiobutton
								afName="placeholder"
								afLabel="Förvalt värde"
								afValue="startSelected"
								afChecked={this.intialText === 'startSelected'}
							/>
							<digi-form-radiobutton
								afName="placeholder"
								afLabel="Ingen text"
								afValue="none"
								afChecked={this.intialText === 'none'}
							/>
						</digi-form-fieldset>
						<digi-form-select
							afLabel="Validering"
							onAfOnChange={(e) =>
								(this.formSelectValidation = (e.target as any).value)
							}
						>
							<option value="neutral">Ingen</option>
							<option value="error">Felaktig</option>
							<option value="success">Godkänd</option>
							<option value="warning">Varning</option>
						</digi-form-select>
					</div>
				)}
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Väljaren används för att möjliggöra val i ett formulär."
				example={this.example()}
				description={
					<Fragment>
						<h3>Validering</h3>
						<p>
							Väljaren kan få fyra olika valideringsstatusar: Felaktig, godkänd,
							varning eller ingen alls. Attributet{' '}
							<digi-code af-code="af-validation" /> sätter väljarens validering och{' '}
							<digi-code af-code="af-validation-text" /> ger väljaren ett
							valideringsmeddelande. Du kan också avaktivera valideringen med
							attributet
							<digi-code af-code="af-disable-validation" />.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
