import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, Prop, h, State, Fragment } from '@stencil/core';

@Component({
	tag: 'digi-layout-container-details',
	scoped: true
})
export class DigiLayoutContainerDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() hasPadding: boolean = false;
	@State() hasGutter: boolean = true;

	get layoutContainerCode() {
		return `\
<digi-layout-container${this.hasPadding ? ' af-vertical-padding' : ''}\
${!this.hasGutter ? ' af-no-gutter' : ''}\
>
  <h2>Rubrik</h2>
  <p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    Etiam magna neque, interdum vel massa eget, condimentum
    rutrum velit. Sed vitae ullamcorper sem. Aliquam malesuada
    nunc sed purus mollis scelerisque. Curabitur bibendum leo
    quis ante porttitor tincidunt. 
    nibh.{' '}
  </p>
</digi-layout-container>
  `;
	}

	example() {
		return (
			<digi-layout-rows>
				<digi-code-example
					af-code={JSON.stringify(this.layoutContainerCode)}
					af-hide-controls={this.afHideControls ? 'true' : 'false'}
					af-hide-code={this.afHideCode ? 'true' : 'false'}
				>
					{!this.afHideControls && (
						<div class="slot__controls" slot="controls">
							<digi-form-fieldset style={{ marginTop: '10px' }}>
								<digi-form-checkbox
									afLabel="Vertikal padding"
									afChecked={this.hasPadding}
									onAfOnChange={() =>
										this.hasPadding ? (this.hasPadding = false) : (this.hasPadding = true)
									}
								></digi-form-checkbox>
							</digi-form-fieldset>
							<digi-form-fieldset>
								<digi-form-checkbox
									afLabel="Ingen gutter"
									afChecked={!this.hasGutter}
									onAfOnChange={() =>
										this.hasGutter ? (this.hasGutter = false) : (this.hasGutter = true)
									}
								></digi-form-checkbox>
							</digi-form-fieldset>
						</div>
					)}
					<div style={{ background: '#F1F1F1' }}>
						{this.hasPadding && this.hasGutter && (
							<digi-layout-container afVerticalPadding>
								<h2>Rubrik</h2>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam magna
									neque, interdum vel massa eget, condimentum rutrum velit. Sed vitae
									ullamcorper sem. Aliquam malesuada nunc sed purus mollis scelerisque.
									Curabitur bibendum leo quis ante porttitor tincidunt. nibh.
								</p>
							</digi-layout-container>
						)}
						{this.hasPadding && !this.hasGutter && (
							<digi-layout-container afVerticalPadding afNoGutter>
								<h2>Rubrik</h2>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam magna
									neque, interdum vel massa eget, condimentum rutrum velit. Sed vitae
									ullamcorper sem. Aliquam malesuada nunc sed purus mollis scelerisque.
									Curabitur bibendum leo quis ante porttitor tincidunt. nibh.
								</p>
							</digi-layout-container>
						)}
						{!this.hasPadding && !this.hasGutter && (
							<digi-layout-container afNoGutter>
								<h2>Rubrik</h2>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam magna
									neque, interdum vel massa eget, condimentum rutrum velit. Sed vitae
									ullamcorper sem. Aliquam malesuada nunc sed purus mollis scelerisque.
									Curabitur bibendum leo quis ante porttitor tincidunt. nibh.
								</p>
							</digi-layout-container>
						)}
						{!this.hasPadding && this.hasGutter && (
							<digi-layout-container>
								<h2>Rubrik</h2>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam magna
									neque, interdum vel massa eget, condimentum rutrum velit. Sed vitae
									ullamcorper sem. Aliquam malesuada nunc sed purus mollis scelerisque.
									Curabitur bibendum leo quis ante porttitor tincidunt. nibh.
								</p>
							</digi-layout-container>
						)}
					</div>
				</digi-code-example>
				<p>
					Bakgrunden på <digi-code af-code="digi-layout-container" /> är transparent,
					den grå bakgrundsfärgen är endast för att göra live-exemplet tydligare.
				</p>
			</digi-layout-rows>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				example={this.example()}
				preamble="Layoutkomponent som sätter en maxbredd och flyttar innehållet till mitten av sidan. Används av flera andra komponenter internt, som räcker upp mer specifika designmönster."
				description={
					<Fragment>
						<h3>Varianter</h3>
						<p>
							Containerkomponenten finns i varianterna static och fluid som går att
							ställa in med <digi-code af-code="af-variation" />.
						</p>
						<h4>Static</h4>
						<p>
							En statisk container kommer att få fasta bredder i olika skärmstorlekar.
							Detta är standard för komponenten.
						</p>
						<h4>Fluid</h4>
						<p>
							Den här variationen av container-komponenten har ingen max-bredd så den
							kommer alltid att vara 100% bred.
						</p>
						<h3>Paddings och marginaler</h3>
						<p>
							Det går att lägga till paddings och margins med olika attribut.{' '}
							<digi-code af-code="af-margin-bottom" /> lägger till extra margin i
							botten och <digi-code af-code="af-vertical-padding" /> lägger till
							padding på båda sidorna. <digi-code af-code="af-no-gutter" /> tar bort
							marginalen på sidorna av containern.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
