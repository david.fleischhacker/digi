import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, Prop, h, State, Fragment } from '@stencil/core';
import {
	CodeExampleLanguage,
	LayoutGridVerticalSpacing
} from '@digi/skolverket';
import { ComponentLink } from '../components/ComponentLink';
import { LayoutComponentGuidelines } from '../components/LayoutComponentGuidelines';

@Component({
	tag: 'digi-layout-grid-details',
	scoped: true,
	styles: `
		digi-layout-grid div {
			background: var(--digi--color--background--tertiary);
			height: 100px;
			display: flex;
			align-items: center;
			justify-content: center;
		}
	`
})
export class DigiLayoutGridDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() hasSpacing: boolean = true;

	get layoutGridCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-layout-grid${!this.hasSpacing ? ` af-vertical-spacing="none"` : ''}>
  <div>1</div>
  <div>2</div>
  <div>3</div>
  <div>4</div>
  <div>5</div>
  <div>6</div>
  <div>7</div>
  <div>8</div>
  <div>9</div>
  <div>10</div>
  <div>11</div>
  <div>12</div>
</digi-layout-grid>
				`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-layout-grid${
				!this.hasSpacing
					? ` [attr.af-vertical-spacing]="LayoutGridVerticalSpacing.NONE"`
					: ''
			}>
  <div>1</div>
  <div>2</div>
  <div>3</div>
  <div>4</div>
  <div>5</div>
  <div>6</div>
  <div>7</div>
  <div>8</div>
  <div>9</div>
  <div>10</div>
  <div>11</div>
  <div>12</div>
</digi-layout-grid>
				`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.layoutGridCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset af-legend="Vertikal marginal">
							<digi-form-checkbox
								afLabel="Ja"
								afChecked={this.hasSpacing}
								onAfOnChange={() =>
									this.hasSpacing ? (this.hasSpacing = false) : (this.hasSpacing = true)
								}
							></digi-form-checkbox>
						</digi-form-fieldset>
					</div>
				)}

				<digi-layout-grid
					af-vertical-spacing={
						!this.hasSpacing
							? LayoutGridVerticalSpacing.NONE
							: LayoutGridVerticalSpacing.REGULAR
					}
				>
					<div>1</div>
					<div>2</div>
					<div>3</div>
					<div>4</div>
					<div>5</div>
					<div>6</div>
					<div>7</div>
					<div>8</div>
					<div>9</div>
					<div>10</div>
					<div>11</div>
					<div>12</div>
				</digi-layout-grid>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				example={this.example()}
				preamble="Standardkomponent för basgrid."
				description={
					<Fragment>
						<p>
							Gridsystemet används för att anpassa layout efter brytpunkterna i olika
							enheter. Denna grid har en konsekvent yttermarginal för flera
							brytpunkter. Detta skapar en mer konsekvent linjering mellan objekt i och
							utanför boxen som ramar in mycket av innehållet - oavsett vilken
							brytpunkt som är aktuell för tillfället.
						</p>
						<p>
							<digi-notification-detail>
								<h4 slot="heading">Om yttermarginalen</h4>
								<p>
									Basgriden använder sig internt av{' '}
									<ComponentLink
										tag="digi-layout-container"
										text="containerkomponenten"
									/>{' '}
									för att få yttermarginaler och positionerna sig rätt på sidan. Den
									behöver alltså inte omslutas av en sådan.
								</p>
							</digi-notification-detail>
						</p>
						<h3>Kolumner</h3>
						<p>
							På stora skärmar och uppåt så är det 12 kolumner. På mellanskärmar är det
							åtta kolumner och på små är det fyra kolumner. Testa att ändra
							webbläsarens fönsterstorlek för att se hur kolumnerna flödar vid olika
							brytpunkter.
						</p>
						<h3>Positionera element i basgriden</h3>
						<p>
							Komponenten använder sig av css grid, så alla vanliga tekniker för att
							placera ut element gäller. Komponenten exponerar även ut en css custom
							property <digi-code af-code="--digi--layout-grid--columns" /> som
							innehåller totala mängden kolumner för tillfället (fyra, åtta eller 12),
							så man kan slippa hantera eventuella brytpunkter. I exemplet nedan
							används detta för att alltid placera ut ett element över hälften av
							kolumnerna.
						</p>
						<digi-code-example
							af-code={`<digi-layout-grid>
  <div style="grid-column: span calc(var(--digi--layout-grid--columns) / 2);">1</div>
  <div>2</div>
  <div>3</div>
  <div>4</div>
  <div>5</div>
  <div>6</div>
  <div>7</div>
  <div>8</div>
  <div>9</div>
  <div>10</div>
  <div>11</div>
  <div>12</div>
</digi-layput-grid>`}
						>
							<digi-layout-grid>
								<div
									style={{
										'grid-column': 'span calc(var(--digi--layout-grid--columns) / 2)'
									}}
								>
									1
								</div>
								<div>2</div>
								<div>3</div>
								<div>4</div>
								<div>5</div>
								<div>6</div>
								<div>7</div>
								<div>8</div>
								<div>9</div>
								<div>10</div>
								<div>11</div>
								<div>12</div>
							</digi-layout-grid>
						</digi-code-example>
					</Fragment>
				}
				guidelines={<LayoutComponentGuidelines />}
			/>
		);
	}
}
