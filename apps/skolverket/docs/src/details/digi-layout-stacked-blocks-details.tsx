import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, Prop, h, State, Fragment } from '@stencil/core';
import {
	CodeExampleLanguage,
	LayoutStackedBlocksVariation
} from '@digi/skolverket';
import { LayoutComponentGuidelines } from '../components/LayoutComponentGuidelines';

@Component({
	tag: 'digi-layout-stacked-blocks-details',
	scoped: true,
	styles: `
		digi-layout-stacked-blocks div {
			background: var(--digi--color--background--tertiary);
			height: 100px;
			display: flex;
			align-items: center;
			justify-content: center;
		}
	`
})
export class DigiLayoutStackedBlocksDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() isEnhanced: boolean = false;

	get layoutStackedBlocksCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-layout-stacked-blocks${this.isEnhanced ? ` af-variation="enhanced"` : ''}>
  <div>1</div>
  <div>2</div>
  <div>3</div>
</digi-layout-stacked-blocks>
				`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-layout-stacked-blocks${
				this.isEnhanced
					? ` [attr.af-variation]="LayoutStackedBlocksVariation.ENHANCED"`
					: ''
			}>
  <div>1</div>
  <div>2</div>
  <div>3</div>
</digi-layout-stacked-blocks>
				`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.layoutStackedBlocksCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset af-legend="Varianter">
							<digi-form-checkbox
								afLabel="Alternativ variant"
								afChecked={this.isEnhanced}
								onAfOnChange={() =>
									this.isEnhanced ? (this.isEnhanced = false) : (this.isEnhanced = true)
								}
							></digi-form-checkbox>
						</digi-form-fieldset>
					</div>
				)}

				<digi-layout-stacked-blocks
					af-variation={
						this.isEnhanced
							? LayoutStackedBlocksVariation.ENHANCED
							: LayoutStackedBlocksVariation.REGULAR
					}
				>
					<div>1</div>
					<div>2</div>
					<div>3</div>
				</digi-layout-stacked-blocks>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				example={this.example()}
				preamble="Standardkomponent för flerkolumnslayouter."
				description={
					<Fragment>
						<p>
							Kort som länkkort eller andra tydliga block som byggs upp efter tre
							kolumner i de större brytpunkterna staplas över varandra när griden bryts
							ner till färre kolumner i brytpunkterna medium och small. Även formulär
							och annan mer detaljerad layout kan enkelt ställas upp i staplingar vid
							mindre brytpunkter.
						</p>
						<h3>Varianter</h3>
						<p>
							I den vanliga, förvalda, varianten så staplas kolumnerna över varandra
							vid små brytpunkter. I den alternativa varianten så bibehåller den första
							kolumnen fullbredd vid alla brytpunkter.
						</p>
					</Fragment>
				}
				guidelines={<LayoutComponentGuidelines />}
			/>
		);
	}
}
