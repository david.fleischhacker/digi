import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { href } from 'stencil-router-v2';
import { ComponentLink } from '../components/ComponentLink';

@Component({
	tag: 'digi-logo-service-details',
	scoped: true
})
export class DigiLogoServiceDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get logoCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-logo-service af-name="Tjänstens namn" af-description="En beskrivande text"></digi-logo-service>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-logo-service [attr.af-name]="'Tjänstens namn'" [attr.af-description]="'En beskrivande text'"></digi-logo-service>`
		};
	}

	@State() showSystemName: boolean;

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.logoCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{
					<digi-logo-service
						afName={`Tjänstens namn`}
						afDescription={`En beskrivande text`}
					></digi-logo-service>
				}
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Det här är Skolverkets logotyp för tjänster och verktyg."
				example={this.example()}
				description={
					<Fragment>
						<h3>När ska logotypen användas?</h3>
						<p>
							Logotypen används i sidhuvudet på tjänster med eget inlogg och meny. Här
							finns{' '}
							<ComponentLink tag="digi-logo" text="logotypen för huvudwebbplatsen" />{' '}
							och och här finns{' '}
							<ComponentLink
								tag="digi-logo-sister"
								text="logotypen för syskonwebbplatser"
							/>
							.
						</p>
						<p>
							För vidare riktlinjer se{' '}
							<a {...href('/grafisk-profil/logotyp')}>
								vår grafiska profil om logotyper
							</a>
						</p>
					</Fragment>
				}
			/>
		);
	}
}
