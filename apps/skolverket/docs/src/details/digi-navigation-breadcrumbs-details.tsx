import { Component, Prop, h } from '@stencil/core';
import {
	CodeExampleLanguage,
	NavigationBreadcrumbsVariation
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-navigation-breadcrumbs-details',
	scoped: true
})
export class DigiNavigationBreadcrumbsDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get breadcrumbCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-navigation-breadcrumbs
  af-current-page="Nuvarande sida"
  af-variation="compressed"
>
  <li><a href="/">Start</a></li>
  <li><a href="/contact-us">Undersida 1</a></li>
  <li><a href="#">Undersida 2</a></li>
</digi-navigation-breadcrumbs>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-breadcrumbs
  [attr.af-current-page]="'Nuvarande sida'"
  [attr.af-variation]="NavigationBreadcrumbsVariation.COMPRESSED"
>
  <li><a href="/">Start</a></li>
  <li><a href="/contact-us">Undersida 1</a></li>
  <li><a href="#">Undersida 2</a></li>
</digi-navigation-breadcrumbs>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.breadcrumbCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-navigation-breadcrumbs
					afCurrentPage="Nuvarande sida"
					afVariation={NavigationBreadcrumbsVariation.COMPRESSED}
				>
					<li>
						<a href="/">Start</a>
					</li>
					<li>
						<a href="/contact-us">Undersida 1</a>
					</li>
					<li>
						<a href="#">Undersida 2</a>
					</li>
				</digi-navigation-breadcrumbs>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Brödsmulor används för att göra det extra tydligt var i strukturen användaren befinner sig och för att förtydliga sidans sammanhang. Brödsmulor kan också användas för att navigera uppåt i strukturen."
				example={this.example()}
				description={
					<ul>
						<li>
							Nuvarande sida sätts via attributet{' '}
							<digi-code af-code="af-current-page='Nuvarande sida'" /> och förväntar
							sig en textsträng som representerar namnet.
						</li>
						<li>
							Brödsmulorna visas upp via en slot och förväntar sig{' '}
							<digi-code af-code="<a>" />
							-element med ett <digi-code af-code="href" />
							-attribut och en text, inuti ett <digi-code af-code="<li>" />
							-element.
						</li>
						<li>
							På Skolverket använder vi varianten 'compressed', detta justeras med{' '}
							<digi-code af-code="af-variation" />
						</li>
					</ul>
				}
				guidelines={
					<digi-notification-detail>
						<h3 slot="heading">Riktlinjer</h3>
						<ul>
							<li>
								Brödsmulor används aldrig för att visa steg i en process, till exempel i
								ett registreringsflöde. I det fallet ska brödsmulan sluta vid
								ingångssidan till flödet och se likadan ut genom alla sidor i flödet.
							</li>
							<li>
								Brödsmulor kan i vissa fall kompletteras med länk. Till exempel i
								navigering mellan ett sökresultat och en annons/profil.
							</li>
							<li>
								Brödsmulor placeras ovanför innehållet och under sidhuvudet, till
								vänster på sidan.
							</li>
							<li>Den sista brödsmulan ska avse sidan man befinner sig på.</li>
							<li>
								Varje brödsmula, förutom den sista, ska vara länkad till respektive
								sida.
							</li>
							<li>
								Brödsmulans namn ska vara samma som sidans namn, vilken även syns i
								huvudnavigationen. Sidans namn är inte alltid detsamma som rubrikens
								namn (H1).
							</li>
							<li>
								Den första brödsmulan ska vara länkad till startsidan och ha namnet
								"Start".
							</li>
							<li>Ikoner ska aldrig användas i brödsmulor.</li>
						</ul>
					</digi-notification-detail>
				}
			/>
		);
	}
}
