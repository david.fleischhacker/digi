import { Component, Prop, h, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { ComponentLink } from '@digi/skolverket-docs/components/ComponentLink';

@Component({
	tag: 'digi-navigation-tab-in-a-box-details',
	scoped: true
})
export class DigiNavigationTabInABoxDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get navigationTabCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-navigation-tabs
  af-aria-label="En tablist label"
  >
  <digi-navigation-tab-in-a-box 
    af-aria-label="Flik 1" 
    af-id="flikb1"
  >
    <p>Jag är flik 1</p>
  </digi-navigation-tab-in-a-box>
  <digi-navigation-tab-in-a-box 
    af-aria-label="Flik 2" 
    af-id="flikb2"
  >
    <p>Jag är flik 2</p>
  </digi-navigation-tab-in-a-box>
  <digi-navigation-tab-in-a-box 
    af-aria-label="Flik 3" 
    af-id="flikb3"
  >
    <p>Jag är flik 3</p>
  </digi-navigation-tab-in-a-box>
</digi-navigation-tabs>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-tabs
  [attr.af-aria-label]="En tablist label"
  >
  <digi-navigation-tab-in-a-box 
    [attr.af-aria-label]="'Flik 1'" 
    [attr.af-id]="flikb1"
  >
    <p>Jag är flik 1</p>
  </digi-navigation-tab-in-a-box>
  <digi-navigation-tab-in-a-box 
    [attr.af-aria-label]="'Flik 2'" 
    [attr.af-id]="flikb2"
  >
    <p>Jag är flik 2</p>
  </digi-navigation-tab-in-a-box>
  <digi-navigation-tab-in-a-box 
    [attr.af-aria-label]="'Flik 3'" 
    [attr.af-id]="flikb3"
  >
    <p>Jag är flik 3</p>
  </digi-navigation-tab-in-a-box>
</digi-navigation-tabs>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.navigationTabCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-navigation-tabs afAriaLabel="En tablist label">
					<digi-navigation-tab-in-a-box afAriaLabel="Flik 1" afId="flikb1">
						<p>Jag är flik 1</p>
					</digi-navigation-tab-in-a-box>
					<digi-navigation-tab-in-a-box afAriaLabel="Flik 2" afId="flikb2">
						<p>Jag är flik 2</p>
					</digi-navigation-tab-in-a-box>
					<digi-navigation-tab-in-a-box afAriaLabel="Flik 3" afId="flikb3">
						<p>Jag är flik 3</p>
					</digi-navigation-tab-in-a-box>
				</digi-navigation-tabs>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="En flikkomponent som alltid ska användas inuti ett flikfält."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Komponenten ska alltid användas inuti{' '}
							<ComponentLink tag="digi-navigation-tabs" text="flikfältskomponenten" />{' '}
							och fungerar ej fristående utan.
						</p>
						<p>
							Varje flik skapar upp en tabpanel inuti flikfältskomponenten. afId och
							afAriaLabel används för att skapa upp kopplingarna mellan flikfältet och
							de enskilda flikarna.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
