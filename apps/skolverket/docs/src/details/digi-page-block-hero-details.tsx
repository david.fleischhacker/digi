import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeExampleLanguage, PageBlockHeroVariation } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-page-block-hero-details',
	scoped: true
})
export class DigiPageBlockHeroDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() variation: PageBlockHeroVariation = PageBlockHeroVariation.START;
	@State() showBackground: boolean = false;
	@State() isTransparent: boolean = false;

	get code() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-page-block-hero
  af-variation="${this.variation}" ${
				this.showBackground
					? `
  af-background-image="/länk/till/bild.jpg"`
					: ''
			}${
				this.isTransparent
					? `
  af-background="transparent"`
					: ''
			}
>
  <h1 slot="heading">Välkommen till Skolverket</h1>
  <p>
	  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae nisl
	  vitae augue.
  </p>
</digi-page-block-hero>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-page-block-hero
  [attr.af-variation]="PageBlockHeroVariation.${Object.keys(
			PageBlockHeroVariation
		).find((key) => PageBlockHeroVariation[key] === this.variation)}" ${
				this.showBackground
					? `
  af-background-image="/länk/till/bild.jpg"`
					: ''
			}${
				this.isTransparent
					? `
  [attr.af-background]="PageBlockHeroBackground.TRANSPARENT"`
					: ''
			}
>
  <h1 slot="heading">Välkommen till Skolverket</h1>
  <p>
	  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae nisl
	  vitae augue.
  </p>
</digi-page-block-hero>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.code)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							afName="Variation"
							afLegend="Variation"
							onChange={(e) => (this.variation = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Startsida"
								afValue={PageBlockHeroVariation.START}
								afChecked={this.variation === PageBlockHeroVariation.START}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Undersida"
								afValue={PageBlockHeroVariation.SUB}
								afChecked={this.variation === PageBlockHeroVariation.SUB}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Sektionssida"
								afValue={PageBlockHeroVariation.SECTION}
								afChecked={this.variation === PageBlockHeroVariation.SECTION}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Processida"
								afValue={PageBlockHeroVariation.PROCESS}
								afChecked={this.variation === PageBlockHeroVariation.PROCESS}
							/>
						</digi-form-fieldset>
						<digi-form-fieldset afLegend="Visa bakgrundsbild">
							<digi-form-checkbox
								afLabel="Ja"
								afChecked={this.showBackground}
								onAfOnChange={() => (this.showBackground = !this.showBackground)}
							></digi-form-checkbox>
						</digi-form-fieldset>
						<digi-form-fieldset afLegend="Transparent bakgrund?">
							<digi-form-checkbox
								afLabel="Ja"
								afChecked={this.isTransparent}
								onAfOnChange={() => (this.isTransparent = !this.isTransparent)}
							></digi-form-checkbox>
						</digi-form-fieldset>
					</div>
				)}
				<digi-page-block-hero
					af-variation={this.variation}
					af-background-image={
						this.showBackground ? `/assets/images/klarsprak-hero.jpeg` : null
					}
					af-background={this.isTransparent ? 'transparent' : null}
				>
					<h1 slot="heading">Välkommen till Skolverket</h1>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae nisl
						vitae augue.
					</p>
				</digi-page-block-hero>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Sidblockskomponent för att skapa en hero."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Heroblocket måste inte användas på varje sida, men när det gör det så ska
							det vara det första blocket på sidan.
						</p>
						<p>
							Det går att använda en bild som bakgrund till heron och även att välja
							transparent bakgrund (om det till exempel finns en backgrundsbild eller
							liknande på hela sidan som man vill ska synas).
						</p>
					</Fragment>
				}
			/>
		);
	}
}
