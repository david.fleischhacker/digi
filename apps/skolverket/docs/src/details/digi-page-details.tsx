import { Component, Prop, h, Fragment, getAssetPath } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-page-details',
	scoped: true
})
export class DigiPageDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get code() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-page>
  <header slot="header">
	  <digi-page-header>...</digi-page-header>
  </header>
  <main>
    <digi-page-block-hero>...</digi-page-block-hero>
    <digi-page-block-cards>...</digi-page-block-cards>
    <digi-page-block-lists>...</digi-page-block-lists>
    <digi-page-block-cards>...</digi-page-block-cards>
  </main>
  <footer slot="footer">
    <digi-page-footer>...</digi-page-footer>
  </footer>
</digi-page>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-page>
  <header slot="header">
	  <digi-page-header>...</digi-page-header>
  </header>
  <main>
    <digi-page-block-hero>...</digi-page-block-hero>
    <digi-page-block-cards>...</digi-page-block-cards>
    <digi-page-block-lists>...</digi-page-block-lists>
    <digi-page-block-cards>...</digi-page-block-cards>
  </main>
  <footer slot="footer">
    <digi-page-footer>...</digi-page-footer>
  </footer>
</digi-page>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.code)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-page>
					<header slot="header">
						<digi-page-header>
							<p slot="top">Top</p>
							<a href="#" slot="logo">
								<digi-logo></digi-logo>
							</a>
							<ul class="digi-u--cols" slot="eyebrow">
								<li>
									<digi-link-icon>
										<a href="#">
											<digi-icon af-name="search" aria-hidden="true"></digi-icon>Sök
										</a>
									</digi-link-icon>
								</li>
								<li>
									<digi-link-icon>
										<a href="#">
											<digi-icon
												af-name="accessibility-deaf"
												aria-hidden="true"
											></digi-icon>
											Lyssna
										</a>
									</digi-link-icon>
								</li>
								<li>
									<digi-link-icon>
										<a href="#">
											<digi-icon af-name="lattlast" aria-hidden="true"></digi-icon>Lättläst
										</a>
									</digi-link-icon>
								</li>
								<li>
									<digi-link-icon>
										<a href="#">
											<digi-icon af-name="globe" aria-hidden="true"></digi-icon>Languages
										</a>
									</digi-link-icon>
								</li>
							</ul>
							<p slot="nav">Nav</p>
						</digi-page-header>
					</header>
					<digi-page-block-hero af-variation="start">
						<h1 slot="heading">Välkommen till Skolverket</h1>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae nisl
							vitae augue.
						</p>
					</digi-page-block-hero>
					<digi-page-block-cards af-variation="start">
						<h2 slot="heading">Undervisning</h2>
						<digi-card-link>
							<img
								slot="image"
								src={getAssetPath('/assets/images/skv-media-object-example.png')}
								width="392"
								height="220"
								style={{ width: '100%', height: 'auto', display: 'block' }}
							/>
							<h2 slot="link-heading">
								<a href="#">Beredskap vid ett förändrat omvärldsläge</a>
							</h2>
							<p>
								Förskola och skola är samhällsviktig verksamhet som behöver fungera även
								vid olika typer av kriser. Här finns stöd för hur du kan förbereda din
								verksamhet vid olika händelser av kris.
							</p>
						</digi-card-link>
						<digi-card-link>
							<img
								slot="image"
								src={getAssetPath('/assets/images/skv-media-object-example.png')}
								width="392"
								height="220"
								style={{ width: '100%', height: 'auto', display: 'block' }}
							/>
							<h2 slot="link-heading">
								<a href="#" slot="link-heading">
									Nyanlända barn och elever från Ukraina
								</a>
							</h2>
							<p>
								Här hittar du som tar emot barn och elever från Ukraina information vad
								som gäller, det stöd vi erbjuder för mottagande och kartläggning och om
								skolsystemet och de olika skolformerna på engelska.
							</p>
						</digi-card-link>
						<digi-card-link>
							<img
								slot="image"
								src={getAssetPath('/assets/images/skv-media-object-example.png')}
								width="392"
								height="220"
								style={{ width: '100%', height: 'auto', display: 'block' }}
							/>
							<h2 slot="link-heading">
								<a href="#">Och så en länkad rubrik</a>
							</h2>
							<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
						</digi-card-link>
					</digi-page-block-cards>
					<digi-page-block-lists af-variation="start">
						<h2 slot="heading">Vidare läsning</h2>
						<digi-list-link>
							<li>
								<a href="#">Undersida</a>
							</li>
							<li>
								<a href="#">Undersida</a>
							</li>
							<li>
								<a href="#">Undersida</a>
							</li>
							<li>
								<a href="#">Undersida</a>
							</li>
						</digi-list-link>
						<digi-list-link>
							<li>
								<a href="#">Undersida</a>
							</li>
							<li>
								<a href="#">Undersida</a>
							</li>
							<li>
								<a href="#">Undersida</a>
							</li>
							<li>
								<a href="#">Undersida</a>
							</li>
						</digi-list-link>
					</digi-page-block-lists>
					<digi-page-block-cards af-variation="start">
						<h2 slot="heading">Skolutveckling</h2>
						<digi-card-link>
							<img
								slot="image"
								src={getAssetPath('/assets/images/skv-media-object-example.png')}
								width="392"
								height="220"
								style={{ width: '100%', height: 'auto', display: 'block' }}
							/>
							<h2 slot="link-heading">
								<a href="#">Beredskap vid ett förändrat omvärldsläge</a>
							</h2>
							<p>
								Förskola och skola är samhällsviktig verksamhet som behöver fungera även
								vid olika typer av kriser. Här finns stöd för hur du kan förbereda din
								verksamhet vid olika händelser av kris.
							</p>
						</digi-card-link>
						<digi-card-link>
							<img
								slot="image"
								src={getAssetPath('/assets/images/skv-media-object-example.png')}
								width="392"
								height="220"
								style={{ width: '100%', height: 'auto', display: 'block' }}
							/>
							<h2 slot="link-heading">
								<a href="#" slot="link-heading">
									Nyanlända barn och elever från Ukraina
								</a>
							</h2>
							<p>
								Här hittar du som tar emot barn och elever från Ukraina information vad
								som gäller, det stöd vi erbjuder för mottagande och kartläggning och om
								skolsystemet och de olika skolformerna på engelska.
							</p>
						</digi-card-link>
						<digi-card-link>
							<img
								slot="image"
								src={getAssetPath('/assets/images/skv-media-object-example.png')}
								width="392"
								height="220"
								style={{ width: '100%', height: 'auto', display: 'block' }}
							/>
							<h2 slot="link-heading">
								<a href="#">Och så en länkad rubrik</a>
							</h2>
							<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
						</digi-card-link>
					</digi-page-block-cards>
					<footer slot="footer">
						<digi-page-footer>
							<digi-list-link af-variation="compact" slot="top-first">
								<h3 slot="heading">Kontakt</h3>
								<li>
									<a href="#">Alla kontaktuppgifter</a>
								</li>
								<li>
									<a href="#">Lämna en synpunkt</a>
								</li>
								<li>
									<a href="#">Skolverket på sociala medier</a>
								</li>
							</digi-list-link>
							<digi-list-link af-variation="compact" slot="top">
								<h3 slot="heading">Om oss</h3>
								<li>
									<a href="#">Om webbplatsen</a>
								</li>
								<li>
									<a href="#">Vår verksamhet</a>
								</li>
								<li>
									<a href="#">Organisation</a>
								</li>
								<li>
									<a href="#">Jobba hos oss</a>
								</li>
							</digi-list-link>
							<digi-list-link af-variation="compact" slot="top">
								<h3 slot="heading">Nyheter och media</h3>
								<li>
									<a href="#">Publikationer & nyhetsbrev</a>
								</li>
								<li>
									<a href="#">Press</a>
								</li>
								<li>
									<a href="#">Kalender</a>
								</li>
								<li>
									<a href="#">Öppna data</a>
								</li>
								<li>
									<a href="#">Jobba hos oss</a>
								</li>
							</digi-list-link>
							<digi-list-link af-variation="compact" af-layout="inline" slot="bottom">
								<li>
									<a href="#">Behandling av personuppgifter</a>
								</li>
								<li>
									<a href="#">Kakor</a>
								</li>
								<li>
									<a href="#">Tillgänglighet</a>
								</li>
							</digi-list-link>
						</digi-page-footer>
					</footer>
				</digi-page>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Sidmallskomponent med plats för sidhuvud, huvudinnehåll och sidfot."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Sidmallen är till för att snabbt skapa upp standardiserade sidvyer. Den
							har plats för sidhuvud, sidfot och huvudinnehåll. Huvudinnehållet fylls
							med fördel med någon eller några av de färdiga sidblockskomponenterna
						</p>
						<p>
							Exemplet ovan använder sig av flera sidblock och delar för att bygga ut
							en vy.
						</p>
						<digi-notification-detail>
							<h3 slot="heading">Tillgänglighet</h3>
							Notera att komponenten inte innehåller några semantiska HTML-element.
							Sidhuvudsdelen är till exempel inte en <digi-code af-code="<header>" />{' '}
							utan en <digi-code af-code="<div>" />. Detta för att sidhuvudet inte
							alltid är en del av sidan utan kan vara en del av en annan komponent.
							Detta gäller även sidfoten och huvudinnehållet. Det är därför viktigt att
							använda sig av rätt HTML-element i de komponenter som används i
							sidhuvudet, sidfoten och huvudinnehållet.
						</digi-notification-detail>
					</Fragment>
				}
			/>
		);
	}
}
