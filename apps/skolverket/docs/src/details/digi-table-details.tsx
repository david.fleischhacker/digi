import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeExampleLanguage, TableVariation } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

//import { TableVariation } from '@digi/skolverket';

@Component({
	tag: 'digi-table-details',
	scoped: true
})
export class DigiTableDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() tableVariation: TableVariation = TableVariation.PRIMARY;

	get tableCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
      <digi-table 
        af-variation="${this.tableVariation}"
      >
      <table>
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">
              Mass (10<sup>24</sup>kg)
            </th>
            <th scope="col">Diameter (km)</th>
            <th scope="col">
              Density (kg/m<sup>3</sup>)
            </th>
            <th scope="col" data-af-table-cell="active">
              Gravity (m/s<sup>2</sup>)
            </th>
            <th scope="col">Length of day (hours)</th>
          </tr>
        </thead>
        <tbody>
          <tr data-af-table-row="active">
            <th scope="row">Mercury</th>
            <td>0.330</td>
            <td>4,879</td>
            <td>5427</td>
            <td>3.7</td>
            <td>4222.6</td>
          </tr>
          <tr>
            ...
          </tr>
          <tr>
            ...
          </tr>
          <tr>
            ...
          </tr>
        </tbody>
      </table>
    </digi-table>`,
			[CodeExampleLanguage.ANGULAR]: `\
      <digi-table 
        [attr.af-variation]="TableVariation.${Object.keys(TableVariation).find(
									(key) => TableVariation[key] === this.tableVariation
								)}" 
      >
      <table>
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">
              Mass (10<sup>24</sup>kg)
            </th>
            <th scope="col">Diameter (km)</th>
            <th scope="col">
              Density (kg/m<sup>3</sup>)
            </th>
            <th scope="col" data-af-table-cell="active">
              Gravity (m/s<sup>2</sup>)
            </th>
            <th scope="col">Length of day (hours)</th>
          </tr>
        </thead>
        <tbody>
          <tr data-af-table-row="active">
            <th scope="row">Mercury</th>
            <td>0.330</td>
            <td>4,879</td>
            <td>5427</td>
            <td>3.7</td>
            <td>4222.6</td>
          </tr>
          <tr>
            ...
          </tr>
          <tr>
            ...
          </tr>
          <tr>
            ...
          </tr>
        </tbody>
      </table>
    </digi-table>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.tableCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							afName="Variation"
							afLegend="Variation"
							onChange={(e) => (this.tableVariation = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Primär"
								afValue={TableVariation.PRIMARY}
								afChecked={this.tableVariation === TableVariation.PRIMARY}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Sekundär"
								afValue={TableVariation.SECONDARY}
								afChecked={this.tableVariation === TableVariation.SECONDARY}
							/>
						</digi-form-fieldset>
					</div>
				)}
				<digi-table afVariation={this.tableVariation}>
					<table>
						<thead>
							<tr>
								<th scope="col">Name</th>
								<th scope="col">
									Mass (10<sup>24</sup>kg)
								</th>
								<th scope="col">Diameter (km)</th>
								<th scope="col">
									Density (kg/m<sup>3</sup>)
								</th>
								<th scope="col" data-af-table-cell="active">
									Gravity (m/s<sup>2</sup>)
								</th>
								<th scope="col">Length of day (hours)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row">Mercury</th>
								<td>0.330</td>
								<td>4,879</td>
								<td>5427</td>
								<td>3.7</td>
								<td>4222.6</td>
							</tr>
							<tr data-af-table-row="active">
								<th scope="row">Venus</th>
								<td>4.87</td>
								<td>12,104</td>
								<td>5243</td>
								<td>8.9</td>
								<td>2802.0</td>
							</tr>
							<tr>
								<th scope="row">Earth</th>
								<td>5.97</td>
								<td>12,756</td>
								<td>5514</td>
								<td>9.8</td>
								<td>24.0</td>
							</tr>
							<tr>
								<th scope="row">Mars</th>
								<td>0.642</td>
								<td>6,792</td>
								<td>3933</td>
								<td>3.7</td>
								<td>24.7</td>
							</tr>
						</tbody>
					</table>
				</digi-table>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Används för att omsluta
			en HTML-tabell så att den formateras enligt Skolverkets grafiska
			profil."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Omslut en HTML-tabell med den här komponenten så kommer den att se ut
							enligt designriktlinjerna. Tabellen finns i två varianter.
						</p>
						<p>
							Med attributen <digi-code af-code="data-af-table-row='active'" /> och{' '}
							<digi-code af-code="data-af-table-cell='active'" /> kan man markera ut
							rader och celler med en "aktiv" stil.
						</p>
						<digi-notification-detail>
							<h3 slot="heading">Tillgänglighet</h3>
							Notera att komponenten inte påverkar markupen inne i själva tabellen, så
							se till att den följer relevanta tillgänglighetskrav.
						</digi-notification-detail>
					</Fragment>
				}
			/>
		);
	}
}
