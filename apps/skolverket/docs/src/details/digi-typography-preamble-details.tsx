import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, Prop, h } from '@stencil/core';

@Component({
	tag: 'digi-typography-preamble-details',
	scoped: true
})
export class DigiTypographyPeambleDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	example() {
		return (
			<digi-code-example
				af-code={`<digi-typography-preamble>
  Jag är en ingress. Använd mig direkt efter huvudrubriken.
</digi-typography-preamble>`}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-typography-preamble>
					Jag är en ingress. Använd mig direkt efter huvudrubrik.
				</digi-typography-preamble>
			</digi-code-example>
		);
	}
	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Den här komponenten används för att skapa en ingress direkt efter en
      huvudrubriken."
				example={this.example()}
				description={
					<p>
						Ingresskomponenten renderar ut sitt innehåll i en{' '}
						<digi-code af-code="<p>" />
						-tagg, så oftast räcker det att använda sig direkt av en textnod som barn
					</p>
				}
			/>
		);
	}
}
