import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, h, Fragment } from '@stencil/core';

@Component({
	tag: 'digi-util-detect-focus-outside-details',
	scoped: true
})
export class DigiUtilDetectFocusOutSideDetails {
	render() {
		return (
			<ComponentDetails
				showOnlyExample={false}
				preamble="Övervakar fokushändelser och skapar events när fokus hamnar inuti eller
      utanför."
				description={
					<Fragment>
						<p>
							Kan till exempel användas för att stänga en meny när fokus hamnar utanför
							den.
						</p>
						<p>Pseudokodsexempel i Angularformat nedan:</p>
						<digi-code-block
							afCode={`<digi-util-detect-focus-outside (afOnFocusOutside)="isOpen === false">
  <div *ngIf="isOpen">Meny</div>
</digi-util-detect-focus-outside>`}
						/>
					</Fragment>
				}
			/>
		);
	}
}
