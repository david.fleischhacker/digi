import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, h, Fragment } from '@stencil/core';

@Component({
	tag: 'digi-util-keydown-handler-details',
	scoped: true
})
export class DigiUtilKeydownHandlerDetails {
	render() {
		return (
			<ComponentDetails
				showOnlyExample={false}
				preamble="Övervakar när en tangent trycks ner."
				description={
					<Fragment>
						<p>
							De vanligaste tangenterna har egan events, men även ett catch-all event
							finns som fångar upp alla tangenter.
						</p>
						<p>Pseudokodsexempel i Angularformat nedan:</p>
						<digi-code-block
							afCode={`<digi-util-keydown-handler (afOnTab)="isBest === 'tab'" (afOnSpace)="isBest === 'space'">
  <p>Alla måste använda {{ isBest }}!</p>
</digi-util-keydown-handler>`}
						/>
					</Fragment>
				}
			/>
		);
	}
}
