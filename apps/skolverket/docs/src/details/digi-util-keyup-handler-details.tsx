import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, h, Fragment } from '@stencil/core';

@Component({
	tag: 'digi-util-keyup-handler-details',
	scoped: true
})
export class DigiUtilKeyupHandlerDetails {
	render() {
		return (
			<ComponentDetails
				showOnlyExample={false}
				preamble="Övervakar när en tangent släpps efter att ha tryckts ner."
				description={
					<Fragment>
						<p>
							De vanligaste tangenterna har egan events, men även ett catch-all event
							finns som fångar upp alla tangenter.
						</p>
						<p>Pseudokodsexempel i Angularformat nedan:</p>
						<digi-code-block
							afCode={`<digi-util-keyup-handler (afOnTab)="isBest === 'tab'" (afOnSpace)="isBest === 'space'">
  <p>Alla måste använda {{ isBest }}!</p>
</digi-util-keyup-handler>`}
						/>
					</Fragment>
				}
			/>
		);
	}
}
