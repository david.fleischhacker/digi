import { Category } from '@digi/taxonomies';
import { components } from '@digi/skolverket-docs/data/componentData';
import { getComponentRoute } from './routes/helpers';
import { ComponentTag } from './routes/componentTagsAndNames.generated';
export interface NavItem {
	/**
	 * The label of the navigation item in the expansion panel.
	 * If not provided, the label will be the same as the key.
	 */
	mainLinkLabel?: string | boolean;
	/**
	 * The path to the page that the navigation item should link to.
	 * If not provided, it will be a slugified version of the key (nested if it is a child).
	 */
	path?: string;
	/**
	 * Child navigation items.
	 */
	children?: NavItemCollection;
}

const getComponentNavChildren = () => {
	return Object.values(Category)
		.sort()
		.filter((category) => category !== Category.UNCATEGORIZED)
		.reduce((acc, category) => {
			const categoryComponents = components
				.filter((component) => component.category === category)
				.filter((component) => !component.tag.includes('digi-icon-'));

			if (!categoryComponents.length) {
				return acc;
			}

			const children = categoryComponents.reduce((acc, component) => {
				const name =
					component.docsTags?.find((tag) => tag.name === 'swedishName')?.text ||
					component.tag;
				acc[name] = {
					path: getComponentRoute(component.tag as ComponentTag)
				};
				return acc;
			}, {});

			return { ...acc, [category]: { children } };
		}, {});
};

export type NavItemCollection = Record<string, NavItem>;

export const mainNavigation: NavItemCollection = {
	'Kom igång': {
		children: {
			'Jobba med kod': {},
			'Jobba med design': {},
			'Release notes': {}
		}
	},
	// This one is special and will add all components as children automatically
	Komponenter: {
		mainLinkLabel: 'Om komponenter',
		children: getComponentNavChildren()
	},
	'Grafisk profil': {
		mainLinkLabel: 'Om vår grafiska profil',
		children: {
			Logotyp: {},
			Färger: {},
			Typografi: {},
			Grafik: {},
			Illustrationer: {},
			Bildspråk: {}
		}
	},
	Språk: {
		mainLinkLabel: 'Om vårt språk',
		children: {
			Klarspråk: {},
			Översättningar: {}
		}
	},
	Tillgänglighet: {
		mainLinkLabel: 'Om digital tillgänglighet',
		children: {
			Tillgänglighetslista: {},
			'Nivåer i WCAG': {},
			'Lagkrav och riktlinjer': {}
		}
	}
};
