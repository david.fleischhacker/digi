import { Component, h } from '@stencil/core';
import Helmet from '@stencil/helmet';

@Component({
	tag: 'page-varumarket',
	scoped: true
})
export class PageVarumarket {
	render() {
		return (
			<article>
				<Helmet>
					<title>Varumärket</title>
				</Helmet>
				<h1>Varumärket</h1>
				<p>Så här gör man med varumärket</p>
			</article>
		);
	}
}
