import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-tabeller',
	scoped: true
})
export class PageCategoryTabeller {
	render() {
		return (
			<CategoryPage
				category={Category.TABLE}
				preamble="Tabeller och tabellinnehåll."
			></CategoryPage>
		);
	}
}
