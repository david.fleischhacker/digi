import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-utfallbart',
	scoped: true
})
export class PageCategoryUtfallbart {
	render() {
		return (
			<CategoryPage
				category={Category.EXPANDABLE}
				preamble="Utfällbara paneler."
			></CategoryPage>
		);
	}
}
