import { Component, h } from '@stencil/core';
import Helmet from '@stencil/helmet';

@Component({
	tag: 'page-process-for-tillganglighetsredogorelse',
	scoped: true
})
export class PageProcessForTillganglighetsredogorelse {
	render() {
		return (
			<article>
				<Helmet>
					<title>Om process för tillgänglighetsredogörelse</title>
				</Helmet>
				<h1>om process för tillgänglighetsredogörelse</h1>
				<p>Så här gör man med process för tillgänglighetsredogörelse</p>
			</article>
		);
	}
}
