import { StrictMode } from 'react';
import ReactDOMClient from 'react-dom/client';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import App from './app/app';

const root = ReactDOMClient.createRoot(
	document.getElementById('root') as HTMLElement
);
root.render(
	<StrictMode>
		<BrowserRouter>
			<Routes>
				<Route path="/" element={<App />}></Route>
			</Routes>
		</BrowserRouter>
	</StrictMode>
);
