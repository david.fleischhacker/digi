export default {
	projects: [
		'<rootDir>/libs/shared/styles',
		'<rootDir>/libs/shared/taxonomies',
		'<rootDir>/libs/core/fonts',
		'<rootDir>/libs/skolverket/design-tokens',
		'<rootDir>/libs/shared/utils',
		'<rootDir>/apps/arbetsformedlingen/playground/angular',
		'<rootDir>/libs/arbetsformedlingen-react',
		'<rootDir>/apps/arbetsformedlingen-playground-react',
		'<rootDir>/libs/shared/text',
		'<rootDir>/libs/skolverket/text',
		'<rootDir>/apps/arbetsformedlingen/playground/wheel-of-fortune'
	]
};
