import { CommonModule } from '@angular/common';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { BooleanValueAccessor } from './stencil-generated/boolean-value-accessor';
import { NumericValueAccessor } from './stencil-generated/number-value-accessor';
import { RadioValueAccessor } from './stencil-generated/radio-value-accessor';
import { SelectValueAccessor } from './stencil-generated/select-value-accessor';
import { TextValueAccessor } from './stencil-generated/text-value-accessor';

import { defineCustomElements } from '@digi/arbetsformedlingen/loader';

import { DIRECTIVES } from './proxies-list';

const VALUEACCESSORS = [
  BooleanValueAccessor,
  NumericValueAccessor,
  RadioValueAccessor,
  SelectValueAccessor,
  TextValueAccessor
];

@NgModule({
  imports: [CommonModule, DIRECTIVES],
  declarations: [VALUEACCESSORS],
  exports: [DIRECTIVES, VALUEACCESSORS],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: () => defineCustomElements,
      multi: true
    }
  ]
})
export class DigiArbetsformedlingenAngularModule {}
