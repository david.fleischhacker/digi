import { Config } from '@stencil/core';
import { StencilBaseConfig } from '../stencil.config';

console.log('Arbetsformedlingen - stencil.config.dev');

export const config: Config = {
	...StencilBaseConfig,
  enableCache: false,
	srcDir: '../src',
	tsconfig: '../tsconfig.json',
  devServer: {
    reloadStrategy: 'pageReload',
    port: 3333
  },
	outputTargets: [
    ...(StencilBaseConfig.outputTargets as any),
    {
			type: 'www',
			serviceWorker: null,
			dir: '../www'
		},
    {
			type: 'dist',
			dir: '../dist',
			esmLoaderPath: '../loader',
			copy: [
				{ src: '../../fonts', dest: 'fonts' },
			]
		},
		{
			type: 'dist-custom-elements',
			dir: '../components',
			customElementsExportBehavior: 'single-export-module',
      generateTypeDeclarations: true,
			copy: [
				{ src: '../../fonts', dest: 'fonts' }
			]
		},
	]
};
