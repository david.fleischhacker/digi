import { Config } from '@stencil/core';
import { StencilBaseConfig } from '../stencil.config';
import { config as AngularConfig } from './stencil.config.angular';
import { config as ReactConfig } from './stencil.config.react';

console.log('Arbetsformedlingen - stencil.config.prod');

export const config: Config = {
  ...StencilBaseConfig,
	srcDir: '../src',
	tsconfig: '../tsconfig.json',
	outputTargets: [
		...(StencilBaseConfig.outputTargets as any),
    ...(AngularConfig.outputTargets as any),
    ...(ReactConfig.outputTargets as any),
		{
			type: 'dist',
			dir: '../dist',
			esmLoaderPath: '../loader',
			copy: [
				{ src: 'design-tokens', dest: 'design-tokens' },
				{ src: '../../fonts', dest: 'fonts' },
				{ src: '../../../shared/styles', dest: 'styles' },
				{
					src: 'components/**/styles/*.variables.scss',
					dest: 'design-tokens/components',
					warn: true
				},
				{
					src: '__core/**/styles/*.variables.scss',
					dest: 'design-tokens/components',
					warn: true
				}
			]
		},
		{
			type: 'dist-custom-elements',
			dir: '../components',
			customElementsExportBehavior: 'single-export-module',
      generateTypeDeclarations: true,
			copy: [
				{ src: 'design-tokens', dest: 'design-tokens' },
				{ src: '../../fonts', dest: 'fonts' },
				{
					src: 'components/**/styles/*.variables.scss',
					dest: 'design-tokens/components',
					warn: true
				},
				{
					src: '__core/**/styles/*.variables.scss',
					dest: 'design-tokens/components',
					warn: true
				}
			]
		},
		{
			type: 'docs-readme'
		},
		{
			type: 'docs-vscode',
			file: './custom-elements.json'
		}
		{
			type: 'dist-hydrate-script',
			dir: '../hydrate'
		}
	],
  extras: {
    enableImportInjection: true
  }
};
