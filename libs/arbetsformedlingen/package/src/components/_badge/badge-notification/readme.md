# digi-badge-notification

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute       | Description                                | Type               | Default     |
| ------------- | --------------- | ------------------------------------------ | ------------------ | ----------- |
| `afAriaLabel` | `af-aria-label` | Sätt attributet 'aria-label'.              | `string`           | `undefined` |
| `afValue`     | `af-value`      | Värdet som ska skrivas ut i notifieringen. | `number \| string` | `''`        |


## CSS Custom Properties

| Name                                           | Description                                            |
| ---------------------------------------------- | ------------------------------------------------------ |
| `--digi--badge-notification--background-color` | var(--digi--color--background--danger-4);              |
| `--digi--badge-notification--color--border`    | var(--digi--color--border--inverted);                  |
| `--digi--badge-notification--color--text`      | var(--digi--color--text--inverted);                    |
| `--digi--badge-notification--font-weight`      | var(--digi--typography--button--font-weight--desktop); |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
