import { newE2EPage } from '@stencil/core/testing';

describe('calendar-datepicker', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent('<calendar-datepicker></calendar-datepicker>');
		const element = await page.find('calendar-datepicker');
		expect(element).toHaveClass('hydrated');
	});

	it('renders changes to the name data', async () => {
		const page = await newE2EPage();

		await page.setContent('<calendar-datepicker></calendar-datepicker>');
		const component = await page.find('calendar-datepicker');
		const element = await page.find('calendar-datepicker >>> div');
		expect(element.textContent).toEqual(`Hello, World! I'm `);

		component.setProperty('first', 'James');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James`);

		component.setProperty('last', 'Quincy');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

		component.setProperty('middle', 'Earl');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
	});
});
