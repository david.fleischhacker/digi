import { newSpecPage } from '@stencil/core/testing';
import { CalendarDatepicker } from './calendar-datepicker';

describe('calendar-datepicker', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [CalendarDatepicker],
			html: '<calendar-datepicker></calendar-datepicker>'
		});
		expect(root).toEqualHtml(`
      <calendar-datepicker>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </calendar-datepicker>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [CalendarDatepicker],
			html: `<calendar-datepicker first="Stencil" last="'Don't call me a framework' JS"></calendar-datepicker>`
		});
		expect(root).toEqualHtml(`
      <calendar-datepicker first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </calendar-datepicker>
    `);
	});
});
