import {
	Component,
	h,
	Prop,
	State,
	Element,
	Method,
	EventEmitter,
	Event
} from '@stencil/core';
import { _t } from '@digi/shared/text';
import { Fragment, HTMLStencilElement } from '@stencil/core/internal';
import { UtilBreakpointObserverBreakpoints } from '@digi/arbetsformedlingen';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FormFileUploadVariation } from './form-file-upload-variation.enum';
import { FormFileUploadValidation } from './form-file-upload-validation.enum';
import { FormFileUploadHeadingLevel } from './form-file-upload-heading-level.enum';

/**
 *	@enums FormInputFileType - form-input-file-type.enum.ts
 *  @enums FormInputFileVariation - form-input-file-variation.enum.ts
 * 	@swedishName Filuppladdare
 */
@Component({
	tag: 'digi-form-file-upload',
	styleUrl: 'form-file-upload.scss',
	scoped: true
})
export class FormFileUpload {
	@Element() hostElement: HTMLStencilElement;

	@State() _input: HTMLInputElement;
	@State() files: File[] = [];
	@State() error: boolean = false;
	@State() errorMessage: string;
	@State() fileHover: boolean = false;

	/**
	 * Texten till labelelementet
	 * @en The label text
	 */
	@Prop() afLabel: string = _t.form.file_upload;

	/**
	 * Valfri beskrivande text
	 * @en A description text
	 */
	@Prop() afLabelDescription: string;

	/**
	 * Sätter variant. Kan vara 'small', 'medium' eller 'large'. Oberoende på satt storlek så kommer komponenten bli 'small' på mobilskärmar.
	 * @en Set input variation 'small', 'medium' or 'large'. On a mobile screen the variation will be automatically set to 'small'.
	 */
	@Prop() afVariation: FormFileUploadVariation = FormFileUploadVariation.SMALL;
	private aforiginalVariation: FormFileUploadVariation;

	/**
	 * Sätter storleken. Kan vara 'standard' eller 'responsive'.
	 * @en Set component size.
	 */
	//@Prop() afBehavior: FormFileUploadBehavior = FormFileUploadBehavior.STANDARD;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Input id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-form-file-upload');

	/** Sätter attributet 'accept'. Använd för att limitera accepterade filtyper
	 * @en Set input field's accept attribute. Use this to limit accepted file types.
	 */
	@Prop() afFileTypes!: string;

	/**
	 * Sätter en maximal filstorlek i MB, 10MB är standard.
	 * @en Set file max size in MB
	 */
	@Prop() afFileMaxSize: number = 10;

	/**
	 * Sätter maximalt antal filer som kan laddas upp
	 * @en Set maximum number of upload files
	 */
	@Prop() afMaxFiles: number;

	/**
	 * Sätter attributet 'name'.
	 * @en Input name attribute
	 */
	@Prop() afName: string;

	/**
	 * Sätter ladda upp knappens text.
	 * @en Set upload buttons text
	 */
	@Prop() afUploadBtnText: string;

	/**
	 * Sätter attributet 'required'.
	 * @en Input required attribute
	 */
	@Prop() afRequired: boolean;

	/**
	 * Sätter text för afRequired.
	 * @en Set text for afRequired.
	 */
	@Prop() afRequiredText: string;

	/**
	 * Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria.
	 * @en Set this to true if the form contains more required fields than optional fields.
	 */
	@Prop() afAnnounceIfOptional = false;

	/**
	 * Sätt rubrik för uppladdade filer.
	 * @en Set heading for uploaded files.
	 */
	@Prop() afHeadingFiles = _t.form.file_uploaded_files;

	/**
	 * Sätter text för afAnnounceIfOptional.
	 * @en Set text for afAnnounceIfOptional.
	 */
	@Prop() afAnnounceIfOptionalText: string;

	/**
	 * Sätter text vid varning om överskriden filstorlek.
	 * @en Set text for warning on file size exceeded.
	 */
	@Prop() afFileMaxSizeText: string;

	/**
	 * Visar miniatyrbild/ikon på uppladdade filer.
	 * @en Shows thumbnail.
	 */
	@Prop() afShowThumbnail: boolean = false;

	/**
	 * Aktiverar förhandsgranskning.
	 * @en Image preview.
	 */
	@Prop() afEnablePreview: boolean = false;

	/**
	 * Sätter antivirus scan status. Kan vara 'enable' eller 'disabled'.
	 * @en Set antivirus scan status. Enable is default.
	 */
	@Prop() afValidation: FormFileUploadValidation =
		FormFileUploadValidation.DISABLED;

	/**
	 * Sätt rubrikens vikt. 'h2' är förvalt.
	 * @en Set heading level. Default is 'h2'
	 */
	@Prop() afHeadingLevel: FormFileUploadHeadingLevel =
		FormFileUploadHeadingLevel.H2;

	/**
	 * Sänder ut fil vid uppladdning
	 * @en Emits file on upload
	 */
	@Event() afOnUploadFile: EventEmitter;

	/**
	 * Sänder ut vilken fil som tagits bort
	 * @en Emits which file that was deleted.
	 */
	@Event() afOnRemoveFile: EventEmitter;

	/**
	 * Sänder ut vilken fil som har avbrutis uppladdning
	 * @en Emits which file that was canceled
	 */
	@Event() afOnCancelFile: EventEmitter;

	/**
	 * Sänder ut vilken fil som försöker laddas upp igen
	 * @en Emits which file is trying to retry its upload
	 */
	@Event() afOnRetryFile: EventEmitter;

	/**
	* När komponenten och slotsen är laddade och initierade så skickas detta eventet.
	* @en When the component and slots are loaded and initialized this event will trigger.
	*/
	@Event({
		bubbles: false,
		cancelable: true,
	}) afOnReady: EventEmitter;

	breakpointHandler(e: any) {
			if (e.detail.value !== undefined) {
				if (e.detail.value === UtilBreakpointObserverBreakpoints.SMALL) {
					this.afVariation = FormFileUploadVariation.SMALL;
				} else {
					this.afVariation = this.aforiginalVariation;
				}
			}
	}
	

	/**
	 * Validera filens status.
	 * @en Validate file status
	 */
	@Method()
	async afMValidateFile(scanInfo: any) {
		this.validateFile(scanInfo);
	}

	/**
	 * Få ut alla uppladdade filer
	 * @en Get all uploaded files
	 */
	@Method()
	async afMGetAllFiles() {
		if (this.afValidation === 'enabled') {
			const validatedFiles = this.files.filter((file) => file['status'] === 'OK');
			return validatedFiles;
		} else {
			return this.files;
		}
	}

	/**
	 * Importera lista med filer utan att trigga uppladdningsevent. Ett objekt måste innehålla id, status och filen själv.
	 * @en Import array with files without triggering upload event. A file object needs to contain an id, status and the file itself.
	 */
	@Method()
	async afMImportFiles(files: File[]) {
		for (const importedFile of files) {
			let duplicate = false;
			for (const item of this.files) {
				if (item.name == importedFile.name) {
					this.error = true;
					this.errorMessage = `Filen (${importedFile.name}) finns redan`;
					duplicate = true;
				}
			}

			if (!duplicate) {
				this.files = [...this.files, importedFile];
			}
		}
	}

	@Method()
	async afMGetFormControlElement() {
		return this._input;
	}

	get fileMaxSize() {
		// 1MB is 1048576 in Bytes
		return this.afFileMaxSize * 1048576;
	}

	checkAcceptedTypes(file) {
		const acceptedTypes = this.afFileTypes.replace(/\s/g, '').split(',')
		const fileMimeType = file.type;
		const fileNameExtension = file.name.split('.').slice(-1).map((item) => '.' + item.toLowerCase()).toString()

		return acceptedTypes.some((item) => {

			// Checking against group of mime types
			if (item.includes('/*')) {
				const mimeTypeCategory = item.split('/').slice(0, 1).toString()
				const fileMimeTypeCategory = fileMimeType.split('/').slice(0, 1).toString()
				return mimeTypeCategory == fileMimeTypeCategory
			}

			// Checking against mimetype and extension
			return item === fileMimeType || item === fileNameExtension || item === '*'
		})
	}

	validateFile(scanInfo: any) {
		const index = this.files.findIndex(
			(file) => file['name'] == scanInfo['name'] || file['id'] == scanInfo['id']
		);

		if (index === -1) {
			return { message: 'Something went wrong' };
		}

		let file = this.files[index];

		if (scanInfo.status === 'OK') {
			file['status'] = 'OK';
			delete file['error'];

			this.removeFileFromList(file['id']);

			this.files = [...this.files, file];
		} else {
			file['status'] = 'error';
			file['error'] =
				scanInfo.error === undefined ? 'Filen gick inte att ladda upp.' : scanInfo.error;
			this.removeFileFromList(file['id']);
			this.files = [...this.files, file];
		}
	}

	emitFile(incomingFile: File) {
		const index = this.getIndexOfFile(incomingFile['id']);

		if (this.afValidation === 'enabled') {
			this.afOnUploadFile.emit(this.files[index]);
		} else {
			this.files[index]['status'] = 'OK';
			this.afOnUploadFile.emit(this.files[index]);
		}
	}

	onRetryFileHandler(e: Event, id: string) {
		e.preventDefault();

		const fileFromList = this.removeFileFromList(id);
		fileFromList['status'] = 'pending';
		delete fileFromList['error'];

		this.files = [...this.files, fileFromList];

		this.afOnRetryFile.emit(fileFromList);
	}

	onCancelFileHandler(e: Event, id: string) {
		e.preventDefault();
		const fileFromList = this.removeFileFromList(id);
		fileFromList['status'] = 'error';
		fileFromList['error'] = 'Uppladdning avbruten';
		this.files = [...this.files, fileFromList];
		this.afOnCancelFile.emit(fileFromList);
	}

	removeFileFromList(id: string) {
		const index = this.getIndexOfFile(id);

		if (index === -1) {
			return;
		}

		const removedFile = this.files[index];
		this.files = [...this.files.slice(0, index), ...this.files.slice(index + 1)];
		return removedFile;
	}

	getIndexOfFile(id: string) {
		const index = this.files.findIndex((file) => file['id'] == id);
		return index;
	}

	handleFiles(e) {
		const formData = new FormData();

		const t = e.dataTransfer ? e.dataTransfer : e.target;

		if (t.files.length > this.afMaxFiles) {
			this.error = true;
			this.errorMessage = _t.form.file_error_upload_limit_exceeded(this.afMaxFiles);
			return;
		}

		for (let i = 0; i < t.files.length; i++) {
			formData.append("files", t.files[i]);
		}

		for (const obj of formData.entries()) {
			this.addUploadedFiles(obj[1]);
		}
	}

	onButtonUploadFileHandler(e) {
		e.preventDefault();

		if (!e.target.files) return;

		this.handleFiles(e);
	}

	onRemoveFileHandler(e: Event, id: any) {
		e.preventDefault();

		const fileToRemove = this.removeFileFromList(id);
		this.afOnRemoveFile.emit(fileToRemove);
		this._input.value = '';

		if (this.files.length < 1) this.error = false;
	}

	async addUploadedFiles(file) {
		this.error = false;

		if (this.files.length >= this.afMaxFiles) {
			this.error = true;
			this.errorMessage = _t.form.file_error_too_many;
			return;
		} else if (file.size > this.fileMaxSize) {
			this.error = true;
			this.errorMessage = this.afFileMaxSizeText || _t.form.file_error_too_large;
			return;
		} else {
			for (const item of this.files) {
				if (item.name == file.name) {
					this.error = true;
					this.errorMessage = _t.form.file_error_exists;
					return;
				}
			}
		}

		file['id'] = randomIdGenerator('file');
		file['status'] = 'pending';
		file['base64'] = await this.toBase64(file);

		this.files = [...this.files, file];

		this.emitFile(file);
	}

	toBase64 = (file: Blob) =>
		new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = () => {
				let encoded = reader.result.toString().replace(/^data:(.*,)?/, '');
				// if (encoded.length % 4 > 0) {
				// 	encoded += '='.repeat(4 - (encoded.length % 4));
				// }
				resolve(encoded);
			};
			reader.onerror = (error) => reject(error);
		});

	handleDrop(e: DragEvent) {
		e.stopPropagation();
		e.preventDefault();
		this.fileHover = false;

		if (e.dataTransfer.files) {
			this.handleFiles(e);

			// const file = e.dataTransfer.files[0];

			if (this.checkAcceptedTypes(e)) {
				this.addUploadedFiles(e);
			} else {
				this.error = true;
				this.errorMessage = 'Filtypen tillåts inte att ladda upp';
			}
		}
	}

	handleAllowDrop(e: DragEvent) {
		if (this.afVariation === FormFileUploadVariation.SMALL) return;
		e.stopPropagation();
		e.preventDefault();
	}

	handleOnDragEnterLeave(e: DragEvent) {
		this.handleAllowDrop(e);
		this.fileHover = !this.fileHover;
	}

	handleInputClick(e: Event) {
		e.stopPropagation();
		this._input.click();
	}

	componentWillLoad() {
		this.aforiginalVariation = this.afVariation;
	 }
	componentDidLoad() {
		this.afOnReady.emit();
	}
	// componentWillUpdate() {}

	getFiletype(file) {
		let fileType = null;
		let fileName = file.name;
		const fileExtension = fileName.split('.').pop();
		const fileWithoutExtension = fileName.replace('.' + fileExtension, '');
		const minChars = window.innerWidth < 769 ? 6 : fileName.length;
		const maxChars = 14;
		if (String(file['type']).split('/')[0] === 'image') {
			fileType = 'image';
		} else if (String(file['type']).split('/')[1] === 'pdf') {
			fileType = 'pdf';
		} else {
			fileType = 'default';
		}
		if (fileName.length > minChars) {
			fileName = fileWithoutExtension.substring(0, minChars) + '...' + fileExtension;
		} else if (fileName.length > maxChars) {
			fileName = fileWithoutExtension.substring(0,6)+ "..." + fileExtension;
		}else {
			fileName = file.name;
		}
		return {type: fileType, name: fileName};
	}


	// If its a pdf or an image the file is opened on another tab, any other file formats are downloaded
	async viewUploadedFile(e, file){
		e.preventDefault();
		if (this.getFiletype(file).type === 'image'){
		var w = window.open();
		w.document.write(`<img src="data:${file['type']};base64,${file['base64']}" alt="${file.name}" />`);
		w.document.title = file['name'];
	}
		else if (this.getFiletype(file).type === 'pdf'){
			(async () => {
				const pdfstr = await fetch(`data:application/pdf;base64,${file['base64']}`);
				const blobFromFetch = await pdfstr.blob();
				var blob = new Blob([blobFromFetch], { type: "application/pdf" });
				const blobUrl = URL.createObjectURL(blob);
				setTimeout(() => {
					window.open(blobUrl, '_blank');
				}, 30);
			})();
		}
		else {
		const fileURL = URL.createObjectURL(file);
		const a = document.createElement('a');
    a.href = fileURL;
    a.target = '_blank';
    a.download = file.name;
    a.click();
		}
	}

	get cssModifiers() {
		return {
			'digi-form-file-upload--small': this.afVariation == 'small',
			'digi-form-file-upload--medium': this.afVariation == 'medium',
			'digi-form-file-upload--large': this.afVariation == 'large'
		};
	}

	render() {
		return (
			<Fragment>
				<digi-util-breakpoint-observer
					onAfOnChange={(e) =>
						this.breakpointHandler(e)
					}>
				</digi-util-breakpoint-observer>
				<div
					class={{
						'digi-form-file-upload': true,
						...this.cssModifiers
					}}
				>
					<digi-form-label
						afFor={this.afId}
						afLabel={this.afLabel}
						afDescription={this.afLabelDescription}
						afRequired={this.afRequired}
						afAnnounceIfOptional={this.afAnnounceIfOptional}
						afRequiredText={this.afRequiredText}
						afAnnounceIfOptionalText={this.afAnnounceIfOptionalText}
					></digi-form-label>
					<div
						class={{
							'digi-form-file-upload__upload-area': true,
							'digi-form-file-upload__upload-area--hover': this.fileHover
						}}
						onDrop={(e) => this.handleDrop(e)}
						onDragOver={(e) => this.handleAllowDrop(e)}
						onDragEnter={(e) => this.handleOnDragEnterLeave(e)}
						onDragLeave={(e) => this.handleOnDragEnterLeave(e)}
						onClick={(e) => this.handleInputClick(e)}
					>
						{this.fileHover && this.afVariation != FormFileUploadVariation.SMALL && (
							<div class="digi-form-file-upload__upload-area-overlay"></div>
						)}
						<div class="digi-form-file-upload__text-area">
							<button
								type="button"
								onClick={(e) => this.handleInputClick(e)}
								class="digi-form-file-upload__button hidden_mobile"
								style={{
									'margin':'0'
								}}
							>
								<digi-icon-paperclip
									aria-hidden="true"
									class="digi-form-file-upload__button-icon"
									slot="icon"
									// style={{
									// 	'--digi--icon--height': '20px',
									// 	'--digi--icon--color': 'var(--digi--color--icons--complementary-1)',
									// 	'padding': 'var(--digi--padding--medium)',
									// 	'padding-left': '0'
									// }}
								></digi-icon-paperclip>
								{this.afUploadBtnText ? this.afUploadBtnText : _t.form.file_choose}
							</button>
							<input
								ref={(el) => {
									this._input = el as HTMLInputElement;
								}}
								onChange={(e) => this.onButtonUploadFileHandler(e)}
								class="digi-form-file-upload__input"
								multiple
								type="file"
								id={this.afId}
								accept={this.afFileTypes}
								name={this.afName ? this.afName : null}
								required={this.afRequired ? this.afRequired : null}
								onDrop={(e) => this.handleDrop(e)}
								onDragOver={(e) => this.handleAllowDrop(e)}
								aria-errormessage="digi-form-file-upload__error"
								aria-describedBy="digi-form-file-upload__error"
							/>
							<span>
								{this.afVariation !== FormFileUploadVariation.SMALL &&
									_t.form.file_dropzone
								}
							</span>
						</div>
					</div>
					{this.error && (
						<digi-form-validation-message
							id="digi-form-file-upload__error"
							class="digi-form-file-upload__error"
							role="alert"
							aria-label={this.errorMessage}
							af-variation="error"
						>
							{this.errorMessage}
						</digi-form-validation-message>
					)}
					{this.files.length > 0 && (
						<div class="digi-form-file-upload__files">
							<this.afHeadingLevel class="digi-form-file-upload__files-heading">
								{this.afHeadingFiles}
							</this.afHeadingLevel>
							<ul>
								{this.files.map((file) => {
									return (
										<li class="digi-form-file-upload__item-container">
											{/* Status Pending */}
											{file['status'] == 'pending' && (
												<div class="digi-form-file-upload__item">
													<div class="digi-form-file-upload__item-header">
														<digi-icon-spinner class="digi-form-file-upload__spinner"></digi-icon-spinner>
														<span style={{ 'margin': '0' }}>
															{_t.form.file_upload_progress} <span class={'hidden-mobile'}>|</span>
														</span>
														<p class="digi-form-file-upload__item-name-pending hidden-mobile"
															style={{ 'margin-left': '10px !important' }}
														>
															{this.getFiletype(file).name}
														</p>
														<button
															type="button"
															onClick={(e) => this.onCancelFileHandler(e, file['id'])}
															class="digi-form-file-upload__button hidden-mobile"
															aria-label={`Avbryt uppladdningen av ${file.name}`}
														>
															Avbryt uppladdning
														</button>
														<button
															type="button"
															onClick={(e) => this.onCancelFileHandler(e, file['id'])}
															class="digi-form-file-upload__button digi-form-file-upload__button--mobile"
															aria-label={`Avbryt uppladdningen av ${file.name}`}
														>
															<digi-icon-x-button-outline
																style={{
																	'--digi--icon--height': '20px',
																	'--digi--icon--color':
																	'var(--digi--color--text--link)'
																}}
																aria-hidden="true"
															></digi-icon-x-button-outline>
														</button>
													</div>
												</div>
											)}

											{/* Status OK */}
											{file['status'] == 'OK' && (
												<div class="digi-form-file-upload__item">
													<div class="digi-form-file-upload__item-text-container">
														<div class="digi-form-file-upload__item-text-container-inner">
															{this.getFiletype(file).type === 'image' && this.afShowThumbnail && (
																<figure class="digi-form-file-upload__item-text-container-figure" style={{'margin': '0' }}>
																	{this.afEnablePreview
																		?
																		<a href="#" onClick={(e) => this.viewUploadedFile(e, file)}>
																			<img class="thumbnail"
																				src={`data:${file['type']};base64,${file['base64']}`}
																				alt={file.name} />
																		</a>
																		:
																		<img class="thumbnail"
																			src={`data:${file['type']};base64,${file['base64']}`}
																			alt={file.name} />
																	}
																</figure>
															)}

															{this.getFiletype(file).type === 'pdf' && (this.afShowThumbnail && this.afEnablePreview) && (
																<a href="#" aria-label={file.name} onClick={(e) => this.viewUploadedFile(e, file)}>
																	<span class={`${this.afShowThumbnail ? 'digi-form-file-upload__item-pdf-preview' : null}`} aria-label={file.name}>
																		{/* pdf-icon-light.svg */}
																		<svg style={{ 'scale': '1' }} width="23.76" height="32" xmlns="http://www.w3.org/2000/svg">
																			<path d="M22.888 6.124 17.696.88A2.955 2.955 0 0 0 15.6 0H2.97C1.33.006 0 1.35 0 3.006V29C0 30.657 1.33 32 2.97 32h17.82c1.64 0 2.97-1.343 2.97-3V8.249a3.03 3.03 0 0 0-.872-2.124Zm-2.34 1.88H15.84V3.25l4.709 4.756ZM2.97 29.002V3.006h9.9v6.498c0 .831.662 1.5 1.485 1.5h6.435v17.997H2.97Zm15.481-8.98c-.755-.75-2.908-.544-3.985-.406-1.064-.656-1.775-1.562-2.277-2.893.242-1.006.625-2.537.335-3.5-.26-1.637-2.34-1.474-2.636-.368-.273 1.006-.025 2.406.433 4.193-.619 1.493-1.54 3.499-2.19 4.649-1.238.643-2.909 1.637-3.156 2.887-.204.987 1.609 3.449 4.708-1.95 1.386-.462 2.896-1.031 4.233-1.256 1.17.637 2.537 1.062 3.452 1.062 1.578 0 1.733-1.762 1.083-2.418ZM6.194 24.883c.315-.856 1.516-1.844 1.88-2.187-1.175 1.893-1.88 2.23-1.88 2.187Zm5.049-11.91c.458 0 .414 2.005.111 2.549-.272-.869-.266-2.55-.111-2.55Zm-1.51 8.535c.6-1.056 1.114-2.312 1.528-3.418.514.944 1.17 1.7 1.863 2.219-1.287.268-2.407.818-3.391 1.2Zm8.143-.312s-.31.375-2.308-.488c2.172-.162 2.53.338 2.308.488Z" fill="#1616B2" fill-rule="evenodd" /></svg>
																	</span>
																</a>
															)}
															{this.getFiletype(file).type === 'pdf' && (this.afShowThumbnail && !this.afEnablePreview) && (
																	<span class={`${this.afShowThumbnail ? 'digi-form-file-upload__item-pdf-preview-bis' : null}`} aria-label={file.name}>
																		{/* pdf-icon-light.svg */}
																		<svg style={{ 'scale': '1' }} width="23.76" height="32" xmlns="http://www.w3.org/2000/svg">
																			<path d="M22.888 6.124 17.696.88A2.955 2.955 0 0 0 15.6 0H2.97C1.33.006 0 1.35 0 3.006V29C0 30.657 1.33 32 2.97 32h17.82c1.64 0 2.97-1.343 2.97-3V8.249a3.03 3.03 0 0 0-.872-2.124Zm-2.34 1.88H15.84V3.25l4.709 4.756ZM2.97 29.002V3.006h9.9v6.498c0 .831.662 1.5 1.485 1.5h6.435v17.997H2.97Zm15.481-8.98c-.755-.75-2.908-.544-3.985-.406-1.064-.656-1.775-1.562-2.277-2.893.242-1.006.625-2.537.335-3.5-.26-1.637-2.34-1.474-2.636-.368-.273 1.006-.025 2.406.433 4.193-.619 1.493-1.54 3.499-2.19 4.649-1.238.643-2.909 1.637-3.156 2.887-.204.987 1.609 3.449 4.708-1.95 1.386-.462 2.896-1.031 4.233-1.256 1.17.637 2.537 1.062 3.452 1.062 1.578 0 1.733-1.762 1.083-2.418ZM6.194 24.883c.315-.856 1.516-1.844 1.88-2.187-1.175 1.893-1.88 2.23-1.88 2.187Zm5.049-11.91c.458 0 .414 2.005.111 2.549-.272-.869-.266-2.55-.111-2.55Zm-1.51 8.535c.6-1.056 1.114-2.312 1.528-3.418.514.944 1.17 1.7 1.863 2.219-1.287.268-2.407.818-3.391 1.2Zm8.143-.312s-.31.375-2.308-.488c2.172-.162 2.53.338 2.308.488Z" fill="#1616B2" fill-rule="evenodd" /></svg>
																	</span>
															)}
															{(this.getFiletype(file).type !== 'image' && this.getFiletype(file).type !== 'pdf') && this.afShowThumbnail && (
																<span class='digi-form-file-upload__item-name--preview'></span>
															)}
															<span class={`digi-form-file-upload__item-name ${this.afShowThumbnail && (this.getFiletype(file).type !== 'image' || this.getFiletype(file).type !== 'pdf') ? null : 'digi-form-file-upload__item-name--preview'}`}>
																{this.afEnablePreview ?
																	<a href="#"
																		aria-label={file.name}
																		onClick={(e) => {
																			// if (this.getFiletype(file).type === 'image')
																			// 	this.viewUploadedImg(e, file)

																			// if (this.getFiletype(file).type === 'pdf')
																			// 	this.viewUploadedPdf(e, file)
																			
																			// else
																			this.viewUploadedFile(e, file)
																		}}>
																		{this.getFiletype(file).name}
																	</a>
																	:
																	<span
																	>
																		{this.getFiletype(file).name}
																	</span>}
															</span>
														</div>
														<button
															type="button"
															onClick={(e) => this.onRemoveFileHandler(e, file['id'])}
															class="digi-form-file-upload__button hidden-mobile"
															aria-label={_t.form.file_remove(file.name)}
															style={{ 'margin': '0' }}
														>
															Ta bort fil
														</button>

														<button
															type="button"
															onClick={(e) => this.onRemoveFileHandler(e, file['id'])}
															class="digi-form-file-upload__button digi-form-file-upload__button--mobile"
															aria-label={_t.form.file_remove(file.name)}
															style={{'margin':'0'}}
														>
															<digi-icon-trash
																style={{
																	'--digi--icon--height': '20px',
																	'--digi--icon--color': 'var(--digi--color--text--link)'
																}}
															></digi-icon-trash>
														</button>
													</div>
												</div>
											)}

											{/* Status Error */}
											{file['status'] == 'error' && (
												<div class="digi-form-file-upload__item">
													<div class="digi-form-file-upload__item-header">
														<digi-icon-validation-error
															style={{
																'--digi--icon--height': '20px',
																'--digi--icon--color': 'var(--digi--color--text--danger)'
															}}
															aria-hidden="true"
														></digi-icon-validation-error>
														<span
															class="digi-form-file-upload__item--error"
															aria-label={_t.form.file_error}
															style={{'margin-left':'10px'}}
														>
															Avbruten
														</span>
														{/* <p
															class="digi-form-file-upload__item-name
													digi-form-file-upload__item--error hidden-mobile
													"
													style={{ 'margin': '0 auto 0 0 !important' }}
														>
															{file.name}
														</p> */}
														{this.afValidation === 'disabled' ||
															(file['error'] === 'Uppladdning avbruten' && (
																<div style={{ display: 'flex'}}>
																	<button
																		type="button"
																		onClick={(e) => this.onRetryFileHandler(e, file['id'])}
																		class="digi-form-file-upload__button digi-form-file-upload__button--file hidden-mobile"
																		aria-label={_t.form.file_error_try_again}
																	>
																		Försök igen
																		<span style={{'margin-left' :'10px' }}>|</span>
																	</button>
																	<button
																		type="button"
																		onClick={(e) => this.onRetryFileHandler(e, file['id'])}
																		class="digi-form-file-upload__button digi-form-file-upload__button--mobile digi-form-file-upload__button--file"
																		aria-label={_t.form.file_error_try_again}
																		style={{
																			'margin-right': '10px'
																		}}
																	>
																		<digi-icon-update
																			style={{
																				'--digi--icon--height': '20px',
																				'--digi--icon--color':
																					'var(--digi--color--text--link)'
																			}}
																		></digi-icon-update>
																	</button>
																</div>
															))}
														<button
															type="button"
															onClick={(e) => this.onRemoveFileHandler(e, file['id'])}
															class="digi-form-file-upload__button digi-form-file-upload__button--file hidden-mobile"
															aria-label={_t.form.file_error_remove(file.name)}
															style={{ 'margin': '0' }}
														>
															Ta bort fil
														</button>
														<button
															type="button"
															onClick={(e) => this.onRemoveFileHandler(e, file['id'])}
															class="digi-form-file-upload__button digi-form-file-upload__button--mobile digi-form-file-upload__button--file"
															aria-label={_t.form.file_error_remove(file.name)}
															style={{ 'margin': '0' }}
														>
															<digi-icon-trash
																style={{
																	'--digi--icon--height': '20px',
																	'--digi--icon--color':
																		'var(--digi--color--text--link)'
																}}
															></digi-icon-trash>
														</button>
													</div>
												</div>
											)}
										</li>
									);
								})}
							</ul>
						</div>
					)}
				</div>
			</Fragment>
		);
	}
}
