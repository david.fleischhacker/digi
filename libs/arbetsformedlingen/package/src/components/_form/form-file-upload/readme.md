# digi-form-file-upload

<!-- Auto Generated Below -->


## Properties

| Property                   | Attribute                      | Description                                                                                                                            | Type                                                                                                                                                                                                 | Default                                      |
| -------------------------- | ------------------------------ | -------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- |
| `afAnnounceIfOptional`     | `af-announce-if-optional`      | Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria.                                                      | `boolean`                                                                                                                                                                                            | `false`                                      |
| `afAnnounceIfOptionalText` | `af-announce-if-optional-text` | Sätter text för afAnnounceIfOptional.                                                                                                  | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afEnablePreview`          | `af-enable-preview`            | Aktiverar förhandsgranskning.                                                                                                          | `boolean`                                                                                                                                                                                            | `false`                                      |
| `afFileMaxSize`            | `af-file-max-size`             | Sätter en maximal filstorlek i MB, 10MB är standard.                                                                                   | `number`                                                                                                                                                                                             | `10`                                         |
| `afFileMaxSizeText`        | `af-file-max-size-text`        | Sätter text vid varning om överskriden filstorlek.                                                                                     | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afFileTypes` _(required)_ | `af-file-types`                | Sätter attributet 'accept'. Använd för att limitera accepterade filtyper                                                               | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afHeadingFiles`           | `af-heading-files`             | Sätt rubrik för uppladdade filer.                                                                                                      | `string`                                                                                                                                                                                             | `_t.form.file_uploaded_files`                |
| `afHeadingLevel`           | `af-heading-level`             | Sätt rubrikens vikt. 'h2' är förvalt.                                                                                                  | `FormFileUploadHeadingLevel.H1 \| FormFileUploadHeadingLevel.H2 \| FormFileUploadHeadingLevel.H3 \| FormFileUploadHeadingLevel.H4 \| FormFileUploadHeadingLevel.H5 \| FormFileUploadHeadingLevel.H6` | `FormFileUploadHeadingLevel.H2`              |
| `afId`                     | `af-id`                        | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                                                                  | `string`                                                                                                                                                                                             | `randomIdGenerator('digi-form-file-upload')` |
| `afLabel`                  | `af-label`                     | Texten till labelelementet                                                                                                             | `string`                                                                                                                                                                                             | `_t.form.file_upload`                        |
| `afLabelDescription`       | `af-label-description`         | Valfri beskrivande text                                                                                                                | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afMaxFiles`               | `af-max-files`                 | Sätter maximalt antal filer som kan laddas upp                                                                                         | `number`                                                                                                                                                                                             | `undefined`                                  |
| `afName`                   | `af-name`                      | Sätter attributet 'name'.                                                                                                              | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afRequired`               | `af-required`                  | Sätter attributet 'required'.                                                                                                          | `boolean`                                                                                                                                                                                            | `undefined`                                  |
| `afRequiredText`           | `af-required-text`             | Sätter text för afRequired.                                                                                                            | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afShowThumbnail`          | `af-show-thumbnail`            | Visar miniatyrbild/ikon på uppladdade filer.                                                                                           | `boolean`                                                                                                                                                                                            | `false`                                      |
| `afUploadBtnText`          | `af-upload-btn-text`           | Sätter ladda upp knappens text.                                                                                                        | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afValidation`             | `af-validation`                | Sätter antivirus scan status. Kan vara 'enable' eller 'disabled'.                                                                      | `FormFileUploadValidation.DISABLED \| FormFileUploadValidation.ENABLED`                                                                                                                              | `FormFileUploadValidation.DISABLED`          |
| `afVariation`              | `af-variation`                 | Sätter variant. Kan vara 'small', 'medium' eller 'large'. Oberoende på satt storlek så kommer komponenten bli 'small' på mobilskärmar. | `FormFileUploadVariation.LARGE \| FormFileUploadVariation.MEDIUM \| FormFileUploadVariation.SMALL`                                                                                                   | `FormFileUploadVariation.SMALL`              |


## Events

| Event            | Description                                                                     | Type               |
| ---------------- | ------------------------------------------------------------------------------- | ------------------ |
| `afOnCancelFile` | Sänder ut vilken fil som har avbrutis uppladdning                               | `CustomEvent<any>` |
| `afOnReady`      | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>` |
| `afOnRemoveFile` | Sänder ut vilken fil som tagits bort                                            | `CustomEvent<any>` |
| `afOnRetryFile`  | Sänder ut vilken fil som försöker laddas upp igen                               | `CustomEvent<any>` |
| `afOnUploadFile` | Sänder ut fil vid uppladdning                                                   | `CustomEvent<any>` |


## Methods

### `afMGetAllFiles() => Promise<File[]>`

Få ut alla uppladdade filer

#### Returns

Type: `Promise<File[]>`



### `afMGetFormControlElement() => Promise<HTMLInputElement>`



#### Returns

Type: `Promise<HTMLInputElement>`



### `afMImportFiles(files: File[]) => Promise<void>`

Importera lista med filer utan att trigga uppladdningsevent. Ett objekt måste innehålla id, status och filen själv.

#### Parameters

| Name    | Type     | Description |
| ------- | -------- | ----------- |
| `files` | `File[]` |             |

#### Returns

Type: `Promise<void>`



### `afMValidateFile(scanInfo: any) => Promise<void>`

Validera filens status.

#### Parameters

| Name       | Type  | Description |
| ---------- | ----- | ----------- |
| `scanInfo` | `any` |             |

#### Returns

Type: `Promise<void>`




## CSS Custom Properties

| Name                                                           | Description                                                                     |
| -------------------------------------------------------------- | ------------------------------------------------------------------------------- |
| `--digi--form-file-upload--border-radius--large--default`      | var(--digi--border-radius--secondary);                                          |
| `--digi--form-file-upload--border-radius--medium--default`     | var(--digi--border-radius--secondary);                                          |
| `--digi--form-file-upload--border-radius--small--default`      | var(--digi--border-radius--secondary);                                          |
| `--digi--form-file-upload--border-style--large--default`       | var(--digi--global--border-style--dashed);                                      |
| `--digi--form-file-upload--border-style--medium--default`      | var(--digi--global--border-style--dashed);                                      |
| `--digi--form-file-upload--border-style--small--default`       | var(--digi--global--border-style--none);                                        |
| `--digi--form-file-upload--border-width--large--default`       | var(--digi--border-width--secondary);                                           |
| `--digi--form-file-upload--border-width--large--hover`         | var(--digi--border-width--primary);                                             |
| `--digi--form-file-upload--border-width--medium--default`      | var(--digi--border-width--secondary);                                           |
| `--digi--form-file-upload--border-width--medium--hover`        | var(--digi--border-width--primary);                                             |
| `--digi--form-file-upload--border-width--small--default`       | var(--digi--border-width--secondary);                                           |
| `--digi--form-file-upload--border-width--small--hover`         | var(--digi--border-width--primary);                                             |
| `--digi--form-file-upload--button-text--color--default`        | var(--digi--global--color--cta--link--base);                                    |
| `--digi--form-file-upload--button-text--color--hover`          | var(--digi--global--color--function--info--light);                              |
| `--digi--form-file-upload--color--background--large--default`  | var(--digi--color--background--secondary);                                      |
| `--digi--form-file-upload--color--background--large--hover`    | var(--digi--global--color--cta--hover--base);                                   |
| `--digi--form-file-upload--color--background--medium--default` | var(--digi--color--background--secondary);                                      |
| `--digi--form-file-upload--color--background--medium--hover`   | var(--digi--global--color--cta--hover--base);                                   |
| `--digi--form-file-upload--color--background--small--default`  | var(--digi--color--background--secondary);                                      |
| `--digi--form-file-upload--color--background--small--hover`    | var(--digi--global--color--cta--hover--base);                                   |
| `--digi--form-file-upload--color--border--large--default`      | var(--digi--global--color--neutral--grayscale--darkest);                        |
| `--digi--form-file-upload--color--border--large--hover`        | var(--digi--global--color--cta--hover--base);                                   |
| `--digi--form-file-upload--color--border--medium--default`     | var(--digi--global--color--neutral--grayscale--darkest);                        |
| `--digi--form-file-upload--color--border--medium--hover`       | var(--digi--global--color--cta--hover--base);                                   |
| `--digi--form-file-upload--color--border--small--default`      | var(--digi--global--color--neutral--grayscale--darkest);                        |
| `--digi--form-file-upload--color--border--small--hover`        | var(--digi--global--color--cta--hover--base);                                   |
| `--digi--form-file-upload--font-family`                        | var(--digi--global--typography--font-family--default);                          |
| `--digi--form-file-upload--font-size--large`                   | var(--digi--global--typography--font-size--large);                              |
| `--digi--form-file-upload--font-size--medium`                  | var(--digi--global--typography--font-size--base);                               |
| `--digi--form-file-upload--font-size--small`                   | var(--digi--global--typography--font-size--small);                              |
| `--digi--form-file-upload--font-weight`                        | var(--digi--typography--form-file-upload--font-weight--desktop);                |
| `--digi--form-file-upload--heading--font-family`               | var(--digi--global--typography--font-family--default);                          |
| `--digi--form-file-upload--heading--font-size`                 | var(--digi--typography--heading-4--font-size--desktop);                         |
| `--digi--form-file-upload--heading--font-size--large`          | var(--digi--typography--heading-4--font-size--desktop-large);                   |
| `--digi--form-file-upload--heading--font-weight`               | var(--digi--typography--heading-4--font-weight--desktop);                       |
| `--digi--form-file-upload--padding--large`                     | var(--digi--gutter--medium) var(--digi--gutter--largest);                       |
| `--digi--form-file-upload--padding--medium`                    | var(--digi--gutter--medium) var(--digi--gutter--largest);                       |
| `--digi--form-file-upload--padding--medium--icon`              | var(--digi--global--spacing--base) var(--digi--global--spacing--smallest-2);    |
| `--digi--form-file-upload--padding--small`                     | var(--digi--gutter--medium) var(--digi--gutter--largest);                       |
| `--digi--form-file-upload--padding--small--icon`               | var(--digi--global--spacing--smaller) var(--digi--global--spacing--smallest-2); |
| `--digi--form-file-upload--width--large`                       | inherit;                                                                        |
| `--digi--form-file-upload--width--medium`                      | 304px;                                                                          |
| `--digi--form-file-upload--width--small`                       | 132px;                                                                          |


## Dependencies

### Depends on

- [digi-util-breakpoint-observer](../../../__core/_util/util-breakpoint-observer)
- [digi-form-label](../../../__core/_form/form-label)
- [digi-icon-paperclip](../../../__core/_icon/icon-paperclip)
- [digi-form-validation-message](../../../__core/_form/form-validation-message)
- [digi-icon-spinner](../../../__core/_icon/icon-spinner)
- [digi-icon-x-button-outline](../../_icon/icon-x-button-outline)
- [digi-icon-trash](../../../__core/_icon/icon-trash)
- [digi-icon-validation-error](../../_icon/icon-validation-error)
- [digi-icon-update](../../_icon/icon-update)

### Graph
```mermaid
graph TD;
  digi-form-file-upload --> digi-util-breakpoint-observer
  digi-form-file-upload --> digi-form-label
  digi-form-file-upload --> digi-icon-paperclip
  digi-form-file-upload --> digi-form-validation-message
  digi-form-file-upload --> digi-icon-spinner
  digi-form-file-upload --> digi-icon-x-button-outline
  digi-form-file-upload --> digi-icon-trash
  digi-form-file-upload --> digi-icon-validation-error
  digi-form-file-upload --> digi-icon-update
  digi-form-validation-message --> digi-icon
  style digi-form-file-upload fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
