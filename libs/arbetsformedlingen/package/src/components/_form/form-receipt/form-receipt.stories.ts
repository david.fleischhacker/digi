import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { FormReceiptHeadingLevel } from './form-receipt-heading-level.enum';
import { FormReceiptVariation } from './form-receipt-variation.enum';
import { FormReceiptType } from './form-receipt-type.enum';

export default {
	title: 'receipt/digi-form-receipt',
	parameters: {
		actions: {
			handles: ['afOnClickLink']
		}
	},
	argTypes: {
		'af-heading-level': enumSelect(FormReceiptHeadingLevel),
		'af-variation': enumSelect(FormReceiptVariation),
		'af-type': enumSelect(FormReceiptType)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-form-receipt',
	'af-text': "These are just words to illustrate how FULLWITH with text aligned CENTER looks like.",
	'af-heading-level': FormReceiptHeadingLevel.H2,
	'af-variation': FormReceiptVariation.FULLWIDTH,
	'af-type': FormReceiptType.CENTER
};

export const WithGridCenter = Template.bind({});
WithGridCenter.args = {
	component: 'digi-form-receipt',
	'af-text': "These are just words to illustrate how GRID with text aligned CENTER looks like.",
	'af-heading-level': FormReceiptHeadingLevel.H1,
	'af-variation': FormReceiptVariation.GRID,
	'af-type': FormReceiptType.CENTER
};

export const WithFullLeft = Template.bind({});
WithFullLeft.args = {
	component: 'digi-form-receipt',
	'af-text': "These are just words to illustrate how FULLWITH with text aligned START looks like.",
	'af-heading-level': null,
	'af-variation': FormReceiptVariation.FULLWIDTH,
	'af-type': FormReceiptType.START
};

export const WithGridLeft = Template.bind({});
WithGridLeft.args = {
	component: 'digi-form-receipt',
	'af-text': "These are just words to illustrate how GRID with text aligned START looks like.",
	'af-heading-level': null,
	'af-variation': FormReceiptVariation.GRID,
	'af-type': FormReceiptType.START
};