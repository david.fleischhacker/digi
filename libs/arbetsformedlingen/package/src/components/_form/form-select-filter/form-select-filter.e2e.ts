import { newE2EPage } from '@stencil/core/testing';

describe('form-select-filter', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent('<form-select-filter></form-select-filter>');
		const element = await page.find('form-select-filter');
		expect(element).toHaveClass('hydrated');
	});

	it('renders changes to the name data', async () => {
		const page = await newE2EPage();

		await page.setContent('<form-select-filter></form-select-filter>');
		const component = await page.find('form-select-filter');
		const element = await page.find('form-select-filter >>> div');
		expect(element.textContent).toEqual(`Hello, World! I'm `);

		component.setProperty('first', 'James');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James`);

		component.setProperty('last', 'Quincy');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

		component.setProperty('middle', 'Earl');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
	});
});
