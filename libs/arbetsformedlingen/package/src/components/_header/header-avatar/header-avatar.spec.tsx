import { newSpecPage } from '@stencil/core/testing';
import { NavigationAvatar } from './header-avatar';

describe('header-avatar', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [NavigationAvatar],
			html: '<header-avatar></header-avatar>'
		});
		expect(root).toEqualHtml(`
      <header-avatar>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </header-avatar>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [NavigationAvatar],
			html: `<header-avatar first="Stencil" last="'Don't call me a framework' JS"></header-avatar>`
		});
		expect(root).toEqualHtml(`
      <header-avatar first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </header-avatar>
    `);
	});
});
