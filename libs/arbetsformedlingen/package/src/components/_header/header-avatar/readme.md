# digi-header-avatar



<!-- Auto Generated Below -->


## Properties

| Property                 | Attribute                   | Description                                    | Type      | Default      |
| ------------------------ | --------------------------- | ---------------------------------------------- | --------- | ------------ |
| `afAlt` _(required)_     | `af-alt`                    | Sätter attributet 'alt'.                       | `string`  | `undefined`  |
| `afHideImage`            | `af-hide-image`             | Döljer profilbilden.                           | `boolean` | `false`      |
| `afHideName`             | `af-hide-name`              | Döljer användarens namn                        | `boolean` | `false`      |
| `afHideSignature`        | `af-hide-signature`         | Döljer användarens signatur                    | `boolean` | `false`      |
| `afHideUser`             | `af-hide-user`              | Döljer användaren helt                         | `boolean` | `false`      |
| `afIsLoggedIn`           | `af-is-logged-in`           | För att visa avataren om man är inloggad       | `boolean` | `true`       |
| `afName`                 | `af-name`                   | Sätter namnet på användaren                    | `string`  | `undefined`  |
| `afPlacerholderTitle`    | `af-placerholder-title`     | Title för placeholderikonen                    | `string`  | `""`         |
| `afShowPlaceholderImage` | `af-show-placeholder-image` | Visar placeholderbild om profilbild inte finns | `boolean` | `undefined`  |
| `afSignature`            | `af-signature`              | Sätter användarens signatur                    | `string`  | `undefined`  |
| `afSrc`                  | `af-src`                    | Sätter attributet 'src'.                       | `string`  | `undefined`  |
| `afUserPlacerholder`     | `af-user-placerholder`      | Visar en placeholdertext om användaren är dold | `string`  | `"Inloggad"` |


## CSS Custom Properties

| Name                                 | Description                                            |
| ------------------------------------ | ------------------------------------------------------ |
| `--digi--header-avatar--color`       | var(--digi--color--text--primary);                     |
| `--digi--header-avatar--font-family` | var(--digi--global--typography--font-family--default); |
| `--digi--header-avatar--font-size`   | var(--digi--typography--label--font-size--desktop);    |
| `--digi--header-avatar--line-height` | var(--digi--typography--label--line-height--desktop);  |
| `--digi--header-avatar--margin`      | var(--digi--gutter--medium);                           |


## Dependencies

### Depends on

- [digi-media-image](../../../__core/_media/media-image)

### Graph
```mermaid
graph TD;
  digi-header-avatar --> digi-media-image
  digi-media-image --> digi-util-intersection-observer
  style digi-header-avatar fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
