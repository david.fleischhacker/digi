import { h } from '@stencil/core';
import { HeaderNavigation } from './header-navigation';

export default {
	title: 'HeaderNavigation',
	component: HeaderNavigation
};

const Template = (args) => <header-navigation {...args}></header-navigation>;

export const Primary = Template.bind({});
Primary.args = { first: 'Hello', last: 'World' };
