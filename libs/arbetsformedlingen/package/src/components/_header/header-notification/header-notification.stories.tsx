import { h } from '@stencil/core';
import { HeaderNotification } from './header-notification';


export default {
	title: 'HeaderNotification',
	component: HeaderNotification
};

const Template = (args) => <header-notification {...args}></header-notification>;

export const Primary = Template.bind({});
Primary.args = { first: 'Hello', last: 'World' }
