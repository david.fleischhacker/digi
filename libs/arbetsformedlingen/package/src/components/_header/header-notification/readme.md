# header-notification



<!-- Auto Generated Below -->


## Properties

| Property               | Attribute                | Description                            | Type     | Default     |
| ---------------------- | ------------------------ | -------------------------------------- | -------- | ----------- |
| `afNotificationAmount` | `af-notification-amount` | Numret som ska skrivas ut som innehåll | `number` | `undefined` |


## CSS Custom Properties

| Name                                        | Description                                            |
| ------------------------------------------- | ------------------------------------------------------ |
| `--digi--header-notification--color`        | var(--digi--color--text--primary);                     |
| `--digi--header-notification--color-active` | var(--digi--color--text--link);                        |
| `--digi--header-notification--color-hover`  | var(--digi--color--text--link);                        |
| `--digi--header-notification--font-family`  | var(--digi--global--typography--font-family--default); |
| `--digi--header-notification--font-size`    | var(--digi--typography--body--font-size--desktop);     |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
