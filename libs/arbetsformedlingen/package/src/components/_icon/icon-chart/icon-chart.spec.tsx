import { newSpecPage } from '@stencil/core/testing';
import { IconChart } from './icon-chart';

describe('icon-chart', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [IconChart],
			html: '<icon-chart></icon-chart>'
		});
		expect(root).toEqualHtml(`
      <icon-chart>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </icon-chart>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [IconChart],
			html: `<icon-chart first="Stencil" last="'Don't call me a framework' JS"></icon-chart>`
		});
		expect(root).toEqualHtml(`
      <icon-chart first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </icon-chart>
    `);
	});
});
