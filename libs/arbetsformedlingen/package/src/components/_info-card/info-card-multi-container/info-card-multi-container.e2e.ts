import { newE2EPage } from '@stencil/core/testing';

describe('digi-info-card-multi-container', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-info-card-multi-container></digi-info-card-multi-container>');

    const element = await page.find('digi-info-card-multi-container');
    expect(element).toHaveClass('hydrated');
  });
});
