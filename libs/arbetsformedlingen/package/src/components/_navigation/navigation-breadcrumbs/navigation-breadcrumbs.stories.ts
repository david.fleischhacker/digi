import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'navigation/digi-navigation-breadcrumbs',
	parameters: {
		actions: {
			handles: ['afOnClick']
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-navigation-breadcrumbs',
	'af-aria-label': 'breadcrumbs',
	'a-id': null,
	'af-current-page': 'Current page is required',
	/* html */
	children: `
    <a href="/">Start</a>
    <a href="/contact-us">Sub-page 1</a>
    <a href="#">Sub-page 2</a>
		`
};

export const WithDelayedChildren = () => {
	return `
	<digi-navigation-breadcrumbs
		id="the-breadcrumbs"
		af-current-page="Current page is required"
	>
		<a href="/">Start</a>
		<a href="/contact-us">Sub-page 1</a>
		<a href="#">Sub-page 2</a>
	</digi-navigation-breadcrumbs>
	<script>
		const bc = document.getElementById('the-breadcrumbs');
		console.log(bc.innerHTML);
		setTimeout(() => bc.innerHTML += '<a href="#">Added after the fact</a>', 1000);
	</script>
	`;
};
