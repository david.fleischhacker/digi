import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'navigation/digi-navigation-tabs',
	parameters: {
		actions: {
			handles: ['afOnChange', 'afOnClick', 'afOnFocus']
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-navigation-tabs',
	'af-aria-label': '',
	'af-init-active-tab': 0,
	'af-id': null,
	/* html */
	children: `
    <digi-navigation-tab af-aria-label="Tab 1" af-id="tab1">
        <p>I am the first tabpanel</p>
        </digi-navigation-tab>
        <digi-navigation-tab af-aria-label="Tab 2" af-id="tab2">
        <p>I am the second tabpanel</p>
        </digi-navigation-tab>
        <digi-navigation-tab af-aria-label="Tab 3" af-id="tab3">
        <p>I am the third tabpanel</p>
    </digi-navigation-tab>`
};
