import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { NotificationAlertSize } from './notification-alert-size.enum';
import { NotificationAlertHeadingLevel } from './notification-alert-heading-level.enum';
import { NotificationAlertVariation } from './notification-alert-variation.enum';

export default {
	title: 'notification/digi-notification-alert',
	parameters: {
		actions: {
			handles: ['afOnClose']
		}
	},
	argTypes: {
		'af-heading-level': enumSelect(NotificationAlertHeadingLevel),
		'af-size': enumSelect(NotificationAlertSize),
		'af-variation': enumSelect(NotificationAlertVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-notification-alert',
	afHeading: '',
	'af-heading-level': NotificationAlertHeadingLevel.H2,
	'af-size': NotificationAlertSize.LARGE,
	'af-variation': NotificationAlertVariation.INFO,
	'af-closeable': false,
	'af-close-aria-label': 'Stäng meddelandet',
	'af-id': null,
	/* html */
	children: '<p>Large alert! Check the knobs section and play around!</p>'
};
