import { h } from '@stencil/core';
import { NotificationErrorPage } from './notification-error-page';

export default {
	title: 'NotificationErrorPage',
	component: NotificationErrorPage
};

const Template = (args) => (
	<notification-error-page {...args}></notification-error-page>
);

export const Primary = Template.bind({});
Primary.args = { first: 'Hello', last: 'World' };
