# digi-tools-feedback-rating

<!-- Auto Generated Below -->


## Properties

| Property          | Attribute            | Description                                                                                                                                      | Type                                                                                                                                                                                                 | Default                                                            |
| ----------------- | -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------ |
| `afApplication`   | `af-application`     | Sätter applikationsnamn, användbart tex vid spårning                                                                                             | `string`                                                                                                                                                                                             | `undefined`                                                        |
| `afHeadingLevel`  | `af-heading-level`   | Sätt rubrikens vikt. 'h2' är förvalt.                                                                                                            | `FeedbackRatingHeadingLevel.H1 \| FeedbackRatingHeadingLevel.H2 \| FeedbackRatingHeadingLevel.H3 \| FeedbackRatingHeadingLevel.H4 \| FeedbackRatingHeadingLevel.H5 \| FeedbackRatingHeadingLevel.H6` | `FeedbackRatingHeadingLevel.H2`                                    |
| `afId`            | `af-id`              | Sätter ett id-attribut. Om inget anges används en slumpmässig sträng.                                                                            | `string`                                                                                                                                                                                             | `randomIdGenerator('digi-tools-feedback-rating')`                  |
| `afIsDone`        | `af-is-done`         | Gör det möjligt att visa ett tack-meddelande som visas när användaren har delat med sig av feedback.                                             | `boolean`                                                                                                                                                                                            | `undefined`                                                        |
| `afLink`          | `af-link`            | Frivillig hyperlänk                                                                                                                              | `string`                                                                                                                                                                                             | `undefined`                                                        |
| `afLinkText`      | `af-link-text`       | Sätter hyperlänktext                                                                                                                             | `string`                                                                                                                                                                                             | `undefined`                                                        |
| `afName`          | `af-name`            | Sätter beskrivning för målgrupp, skärning, videonamn (t.ex. Blivit föräldraledig, Fått arbete, Börjat studera), användbart tex vid spårning      | `string`                                                                                                                                                                                             | `undefined`                                                        |
| `afProduct`       | `af-product`         | Sätter produktnamn, användbart tex vid spårning                                                                                                  | `string`                                                                                                                                                                                             | `undefined`                                                        |
| `afProductArea`   | `af-product-area`    | Sätter produktområdesnamn, användbart tex vid spårning                                                                                           | `string`                                                                                                                                                                                             | `undefined`                                                        |
| `afQuestion`      | `af-question`        | Anger frågan på det som ska betygsättas.                                                                                                         | `string`                                                                                                                                                                                             | `'Hur nöjd är du med tjänsten?'`                                   |
| `afQuestionType`  | `af-question-type`   | Definierar typ av fråga såsom Innehåll, Måluppfyllnad eller Kundnöjdhet för korrekt redovisning i webbanalysverktyg, användbart tex vid spårning | `string`                                                                                                                                                                                             | `undefined`                                                        |
| `afRatings`       | `af-ratings`         | Anger svarsalternativen som visas i komponenten och spåras med Piwik. Behöver separeras med semikolon.                                           | `string`                                                                                                                                                                                             | `'mycket dåligt;ganska dåligt;varken eller;ganska bra;mycket bra'` |
| `afType`          | `af-type`            | Sätter typ som kan vara 'fullwidth', 'grid' eller 'minimal'.                                                                                     | `FeedbackRatingType.FULLWIDTH \| FeedbackRatingType.GRID \| FeedbackRatingType.MINIMAL`                                                                                                              | `FeedbackRatingType.FULLWIDTH`                                     |
| `afUseCustomLink` | `af-use-custom-link` | Om afLink används, kombineras den med URL-variabler för inkludering av betyg och URL:en som användaren kommer ifrån                              | `boolean`                                                                                                                                                                                            | `false`                                                            |
| `afVariation`     | `af-variation`       | Sätter variant, kan vara primär, sekundär eller tertiär                                                                                          | `FeedbackRatingVariation.PRIMARY \| FeedbackRatingVariation.SECONDARY \| FeedbackRatingVariation.TERTIARY`                                                                                           | `undefined`                                                        |


## Events

| Event                    | Description                               | Type               |
| ------------------------ | ----------------------------------------- | ------------------ |
| `afOnClickFeedback`      | Emit av eventet sker vid klick på stjärna | `CustomEvent<any>` |
| `afOnSubmitFeedbackLink` | Emit av eventet sker vid klick på länk    | `CustomEvent<any>` |


## Methods

### `afMReset() => Promise<void>`

Används för att ändra till steg 1 och återställa betyget.

#### Returns

Type: `Promise<void>`



### `afMSetStep(step: 1 | 2) => Promise<void>`

Används för att ändra steg mellan 1-2.

#### Parameters

| Name   | Type     | Description |
| ------ | -------- | ----------- |
| `step` | `2 \| 1` |             |

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [digi-typography](../../../__core/_typography/typography)
- [digi-link-external](../../../__core/_link/link-external)
- [digi-layout-block](../../../__core/_layout/layout-block)

### Graph
```mermaid
graph TD;
  digi-tools-feedback-rating --> digi-typography
  digi-tools-feedback-rating --> digi-link-external
  digi-tools-feedback-rating --> digi-layout-block
  digi-link-external --> digi-link
  digi-link-external --> digi-icon
  digi-layout-block --> digi-layout-container
  style digi-tools-feedback-rating fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
