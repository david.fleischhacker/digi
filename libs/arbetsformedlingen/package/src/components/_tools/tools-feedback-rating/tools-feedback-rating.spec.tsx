import { newSpecPage } from '@stencil/core/testing';
import { ToolsFeedbackRating } from './tools-feedback-rating';

describe('tools-feedback-rating', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [ToolsFeedbackRating],
			html: '<tools-feedback-rating></tools-feedback-rating>'
		});
		expect(root).toEqualHtml(`
      <tools-feedback-rating>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </tools-feedback-rating>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [ToolsFeedbackRating],
			html: `<tools-feedback-rating first="Stencil" last="'Don't call me a framework' JS"></tools-feedback-rating>`
		});
		expect(root).toEqualHtml(`
      <tools-feedback-rating first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </tools-feedback-rating>
    `);
	});
});
