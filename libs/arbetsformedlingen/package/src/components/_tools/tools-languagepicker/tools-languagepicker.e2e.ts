import { newE2EPage } from '@stencil/core/testing';

describe('tools-languagepicker', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent('<digi-tools-languagepicker></digi-tools-languagepicker>');
		const element = await page.find('tools-languagepicker');
		expect(element).toHaveClass('hydrated');
	});
});
