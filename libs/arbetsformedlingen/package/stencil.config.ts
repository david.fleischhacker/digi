import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';

console.log('Arbetsformedlingen - stencil.config');

// Path is relative from .config/

export const StencilBaseConfig: Config = {
	namespace: 'digi-arbetsformedlingen',
	taskQueue: 'async',
  autoprefixCss: false,
  sourceMap: false,
	srcDir: '../src',
	tsconfig: '../tsconfig.json',
	plugins: [
		sass()
	],
  enableCache: false,
  cacheDir: '../../../.digi/af-stencil',
	outputTargets: [],
	globalStyle: '../src/global/styles/index.scss',
	globalScript: '../src/global/scripts/index.ts',
};
