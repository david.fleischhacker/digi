import styles from './arbetsformedlingen-react.module.scss';

/* eslint-disable-next-line */
export interface ArbetsformedlingenReactProps {}

export function ArbetsformedlingenReact(props: ArbetsformedlingenReactProps) {
	return (
		<div className={styles['container']}>
			<h1>Welcome to ArbetsformedlingenReact!</h1>
		</div>
	);
}

export default ArbetsformedlingenReact;
