// DIRECTIVES
export { DIRECTIVES } from './lib/proxies-list';
export * from './lib/stencil-generated/components';


// VALUE ACCESSORS
export * from './lib/stencil-generated/boolean-value-accessor';
export * from './lib/stencil-generated/number-value-accessor';
export * from './lib/stencil-generated/radio-value-accessor';
export * from './lib/stencil-generated/select-value-accessor';
export * from './lib/stencil-generated/text-value-accessor';

// PACKAGE MODULE
export { DigiCoreAngularModule } from './lib/digi-core-angular.module';
