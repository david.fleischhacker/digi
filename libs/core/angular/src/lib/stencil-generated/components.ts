/* tslint:disable */
/* auto-generated angular directive proxies */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, NgZone } from '@angular/core';

import { ProxyCmp, proxyOutputs } from './angular-component-lib/utils';

import type { Components } from '@digi/core/components';

import { defineCustomElement as defineDigiBarChart } from '@digi/core/components/digi-bar-chart.js';
import { defineCustomElement as defineDigiButton } from '@digi/core/components/digi-button.js';
import { defineCustomElement as defineDigiCalendar } from '@digi/core/components/digi-calendar.js';
import { defineCustomElement as defineDigiCalendarWeekView } from '@digi/core/components/digi-calendar-week-view.js';
import { defineCustomElement as defineDigiChartLine } from '@digi/core/components/digi-chart-line.js';
import { defineCustomElement as defineDigiCode } from '@digi/core/components/digi-code.js';
import { defineCustomElement as defineDigiCodeBlock } from '@digi/core/components/digi-code-block.js';
import { defineCustomElement as defineDigiCodeExample } from '@digi/core/components/digi-code-example.js';
import { defineCustomElement as defineDigiExpandableAccordion } from '@digi/core/components/digi-expandable-accordion.js';
import { defineCustomElement as defineDigiFormCheckbox } from '@digi/core/components/digi-form-checkbox.js';
import { defineCustomElement as defineDigiFormFieldset } from '@digi/core/components/digi-form-fieldset.js';
import { defineCustomElement as defineDigiFormFilter } from '@digi/core/components/digi-form-filter.js';
import { defineCustomElement as defineDigiFormFilterPresentational } from '@digi/core/components/digi-form-filter-presentational.js';
import { defineCustomElement as defineDigiFormInput } from '@digi/core/components/digi-form-input.js';
import { defineCustomElement as defineDigiFormInputSearch } from '@digi/core/components/digi-form-input-search.js';
import { defineCustomElement as defineDigiFormLabel } from '@digi/core/components/digi-form-label.js';
import { defineCustomElement as defineDigiFormRadiobutton } from '@digi/core/components/digi-form-radiobutton.js';
import { defineCustomElement as defineDigiFormRadiogroup } from '@digi/core/components/digi-form-radiogroup.js';
import { defineCustomElement as defineDigiFormSelect } from '@digi/core/components/digi-form-select.js';
import { defineCustomElement as defineDigiFormTextarea } from '@digi/core/components/digi-form-textarea.js';
import { defineCustomElement as defineDigiFormValidationMessage } from '@digi/core/components/digi-form-validation-message.js';
import { defineCustomElement as defineDigiIcon } from '@digi/core/components/digi-icon.js';
import { defineCustomElement as defineDigiIconBars } from '@digi/core/components/digi-icon-bars.js';
import { defineCustomElement as defineDigiIconCheck } from '@digi/core/components/digi-icon-check.js';
import { defineCustomElement as defineDigiIconCheckCircleRegAlt } from '@digi/core/components/digi-icon-check-circle-reg-alt.js';
import { defineCustomElement as defineDigiIconCheckCircleSolid } from '@digi/core/components/digi-icon-check-circle-solid.js';
import { defineCustomElement as defineDigiIconChevronDown } from '@digi/core/components/digi-icon-chevron-down.js';
import { defineCustomElement as defineDigiIconChevronLeft } from '@digi/core/components/digi-icon-chevron-left.js';
import { defineCustomElement as defineDigiIconChevronRight } from '@digi/core/components/digi-icon-chevron-right.js';
import { defineCustomElement as defineDigiIconChevronUp } from '@digi/core/components/digi-icon-chevron-up.js';
import { defineCustomElement as defineDigiIconCopy } from '@digi/core/components/digi-icon-copy.js';
import { defineCustomElement as defineDigiIconDangerOutline } from '@digi/core/components/digi-icon-danger-outline.js';
import { defineCustomElement as defineDigiIconDownload } from '@digi/core/components/digi-icon-download.js';
import { defineCustomElement as defineDigiIconExclamationCircleFilled } from '@digi/core/components/digi-icon-exclamation-circle-filled.js';
import { defineCustomElement as defineDigiIconExclamationTriangle } from '@digi/core/components/digi-icon-exclamation-triangle.js';
import { defineCustomElement as defineDigiIconExclamationTriangleWarning } from '@digi/core/components/digi-icon-exclamation-triangle-warning.js';
import { defineCustomElement as defineDigiIconExternalLinkAlt } from '@digi/core/components/digi-icon-external-link-alt.js';
import { defineCustomElement as defineDigiIconMinus } from '@digi/core/components/digi-icon-minus.js';
import { defineCustomElement as defineDigiIconNotificationSucces } from '@digi/core/components/digi-icon-notification-succes.js';
import { defineCustomElement as defineDigiIconNotificationWarning } from '@digi/core/components/digi-icon-notification-warning.js';
import { defineCustomElement as defineDigiIconPaperclip } from '@digi/core/components/digi-icon-paperclip.js';
import { defineCustomElement as defineDigiIconPlus } from '@digi/core/components/digi-icon-plus.js';
import { defineCustomElement as defineDigiIconSearch } from '@digi/core/components/digi-icon-search.js';
import { defineCustomElement as defineDigiIconSpinner } from '@digi/core/components/digi-icon-spinner.js';
import { defineCustomElement as defineDigiIconTrash } from '@digi/core/components/digi-icon-trash.js';
import { defineCustomElement as defineDigiIconValidationSuccess } from '@digi/core/components/digi-icon-validation-success.js';
import { defineCustomElement as defineDigiIconX } from '@digi/core/components/digi-icon-x.js';
import { defineCustomElement as defineDigiLayoutBlock } from '@digi/core/components/digi-layout-block.js';
import { defineCustomElement as defineDigiLayoutColumns } from '@digi/core/components/digi-layout-columns.js';
import { defineCustomElement as defineDigiLayoutContainer } from '@digi/core/components/digi-layout-container.js';
import { defineCustomElement as defineDigiLayoutMediaObject } from '@digi/core/components/digi-layout-media-object.js';
import { defineCustomElement as defineDigiLink } from '@digi/core/components/digi-link.js';
import { defineCustomElement as defineDigiLinkExternal } from '@digi/core/components/digi-link-external.js';
import { defineCustomElement as defineDigiLinkInternal } from '@digi/core/components/digi-link-internal.js';
import { defineCustomElement as defineDigiLoaderSkeleton } from '@digi/core/components/digi-loader-skeleton.js';
import { defineCustomElement as defineDigiLoaderSpinner } from '@digi/core/components/digi-loader-spinner.js';
import { defineCustomElement as defineDigiMediaFigure } from '@digi/core/components/digi-media-figure.js';
import { defineCustomElement as defineDigiMediaImage } from '@digi/core/components/digi-media-image.js';
import { defineCustomElement as defineDigiNavigationContextMenu } from '@digi/core/components/digi-navigation-context-menu.js';
import { defineCustomElement as defineDigiNavigationContextMenuItem } from '@digi/core/components/digi-navigation-context-menu-item.js';
import { defineCustomElement as defineDigiNavigationSidebar } from '@digi/core/components/digi-navigation-sidebar.js';
import { defineCustomElement as defineDigiNavigationSidebarButton } from '@digi/core/components/digi-navigation-sidebar-button.js';
import { defineCustomElement as defineDigiNavigationVerticalMenu } from '@digi/core/components/digi-navigation-vertical-menu.js';
import { defineCustomElement as defineDigiNavigationVerticalMenuItem } from '@digi/core/components/digi-navigation-vertical-menu-item.js';
import { defineCustomElement as defineDigiProgressIndicator } from '@digi/core/components/digi-progress-indicator.js';
import { defineCustomElement as defineDigiProgressStep } from '@digi/core/components/digi-progress-step.js';
import { defineCustomElement as defineDigiProgressSteps } from '@digi/core/components/digi-progress-steps.js';
import { defineCustomElement as defineDigiProgressbar } from '@digi/core/components/digi-progressbar.js';
import { defineCustomElement as defineDigiTag } from '@digi/core/components/digi-tag.js';
import { defineCustomElement as defineDigiTypography } from '@digi/core/components/digi-typography.js';
import { defineCustomElement as defineDigiTypographyMeta } from '@digi/core/components/digi-typography-meta.js';
import { defineCustomElement as defineDigiTypographyPreamble } from '@digi/core/components/digi-typography-preamble.js';
import { defineCustomElement as defineDigiTypographyTime } from '@digi/core/components/digi-typography-time.js';
import { defineCustomElement as defineDigiUtilBreakpointObserver } from '@digi/core/components/digi-util-breakpoint-observer.js';
import { defineCustomElement as defineDigiUtilDetectClickOutside } from '@digi/core/components/digi-util-detect-click-outside.js';
import { defineCustomElement as defineDigiUtilDetectFocusOutside } from '@digi/core/components/digi-util-detect-focus-outside.js';
import { defineCustomElement as defineDigiUtilIntersectionObserver } from '@digi/core/components/digi-util-intersection-observer.js';
import { defineCustomElement as defineDigiUtilKeydownHandler } from '@digi/core/components/digi-util-keydown-handler.js';
import { defineCustomElement as defineDigiUtilKeyupHandler } from '@digi/core/components/digi-util-keyup-handler.js';
import { defineCustomElement as defineDigiUtilMutationObserver } from '@digi/core/components/digi-util-mutation-observer.js';
import { defineCustomElement as defineDigiUtilResizeObserver } from '@digi/core/components/digi-util-resize-observer.js';
@ProxyCmp({
  defineCustomElementFn: defineDigiBarChart,
  inputs: ['afChartData', 'afHeadingLevel', 'afId', 'afVariation']
})
@Component({
  selector: 'digi-bar-chart',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afChartData', 'afHeadingLevel', 'afId', 'afVariation'],
  standalone: true
})
export class DigiBarChart {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiBarChart extends Components.DigiBarChart {}


@ProxyCmp({
  defineCustomElementFn: defineDigiButton,
  inputs: ['afAriaChecked', 'afAriaControls', 'afAriaCurrent', 'afAriaExpanded', 'afAriaHaspopup', 'afAriaLabel', 'afAriaLabelledby', 'afAriaPressed', 'afAutofocus', 'afDir', 'afForm', 'afFullWidth', 'afId', 'afInputRef', 'afLang', 'afRole', 'afSize', 'afTabindex', 'afType', 'afVariation'],
  methods: ['afMGetButtonElement']
})
@Component({
  selector: 'digi-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afAriaChecked', 'afAriaControls', 'afAriaCurrent', 'afAriaExpanded', 'afAriaHaspopup', 'afAriaLabel', 'afAriaLabelledby', 'afAriaPressed', 'afAutofocus', 'afDir', 'afForm', 'afFullWidth', 'afId', 'afInputRef', 'afLang', 'afRole', 'afSize', 'afTabindex', 'afType', 'afVariation'],
  standalone: true
})
export class DigiButton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick', 'afOnFocus', 'afOnBlur', 'afOnReady']);
  }
}


export declare interface DigiButton extends Components.DigiButton {
  /**
   * Buttonelementets 'onclick'-event. @en The button element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * Buttonelementets 'onfocus'-event. @en The button element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<FocusEvent>>;
  /**
   * Buttonelementets 'onblur'-event. @en The button element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiCalendar,
  inputs: ['afActive', 'afId', 'afInitSelectedDate', 'afInitSelectedMonth', 'afMaxDate', 'afMinDate', 'afMultipleDates', 'afSelectedDate', 'afSelectedDates', 'afShowWeekNumber', 'afYearSelect']
})
@Component({
  selector: 'digi-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afActive', 'afId', 'afInitSelectedDate', 'afInitSelectedMonth', 'afMaxDate', 'afMinDate', 'afMultipleDates', 'afSelectedDate', 'afSelectedDates', 'afShowWeekNumber', 'afYearSelect'],
  standalone: true
})
export class DigiCalendar {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnDateSelectedChange', 'afOnFocusOutside', 'afOnClickOutside', 'afOnDirty']);
  }
}


export declare interface DigiCalendar extends Components.DigiCalendar {
  /**
   * Sker när ett datum har valts eller avvalts. Returnerar datumen i en array. @en When a date is selected. Return the dates in an array.
   */
  afOnDateSelectedChange: EventEmitter<CustomEvent<any>>;
  /**
   * Sker när fokus flyttas utanför kalendern @en When focus moves out from the calendar
   */
  afOnFocusOutside: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid klick utanför kalendern @en When click outside the calendar
   */
  afOnClickOutside: EventEmitter<CustomEvent<any>>;
  /**
   * Sker när kalenderdatumen har rörts första gången, när värdet på focusedDate har ändrats första gången. @en When the calendar dates are touched the first time, when focusedDate is changed the first time.
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiCalendarWeekView,
  inputs: ['afDates', 'afHeadingLevel', 'afId', 'afMaxWeek', 'afMinWeek'],
  methods: ['afMSetActiveDate']
})
@Component({
  selector: 'digi-calendar-week-view',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDates', 'afHeadingLevel', 'afId', 'afMaxWeek', 'afMinWeek'],
  standalone: true
})
export class DigiCalendarWeekView {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnWeekChange', 'afOnDateChange', 'afOnReady']);
  }
}


export declare interface DigiCalendarWeekView extends Components.DigiCalendarWeekView {
  /**
   * Vid byte av vecka @en When week changes
   */
  afOnWeekChange: EventEmitter<CustomEvent<string>>;
  /**
   * Vid byte av dag @en When day changes
   */
  afOnDateChange: EventEmitter<CustomEvent<string>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiChartLine,
  inputs: ['afChartData', 'afHeadingLevel', 'afId']
})
@Component({
  selector: 'digi-chart-line',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afChartData', 'afHeadingLevel', 'afId'],
  standalone: true
})
export class DigiChartLine {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiChartLine extends Components.DigiChartLine {}


@ProxyCmp({
  defineCustomElementFn: defineDigiCode,
  inputs: ['afCode', 'afLang', 'afLanguage', 'afVariation']
})
@Component({
  selector: 'digi-code',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afCode', 'afLang', 'afLanguage', 'afVariation'],
  standalone: true
})
export class DigiCode {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiCode extends Components.DigiCode {}


@ProxyCmp({
  defineCustomElementFn: defineDigiCodeBlock,
  inputs: ['afCode', 'afHideToolbar', 'afLang', 'afLanguage', 'afVariation']
})
@Component({
  selector: 'digi-code-block',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afCode', 'afHideToolbar', 'afLang', 'afLanguage', 'afVariation'],
  standalone: true
})
export class DigiCodeBlock {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiCodeBlock extends Components.DigiCodeBlock {}


@ProxyCmp({
  defineCustomElementFn: defineDigiCodeExample,
  inputs: ['afCode', 'afCodeBlockVariation', 'afExampleVariation', 'afHideCode', 'afHideControls', 'afLanguage']
})
@Component({
  selector: 'digi-code-example',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afCode', 'afCodeBlockVariation', 'afExampleVariation', 'afHideCode', 'afHideControls', 'afLanguage'],
  standalone: true
})
export class DigiCodeExample {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiCodeExample extends Components.DigiCodeExample {}


@ProxyCmp({
  defineCustomElementFn: defineDigiExpandableAccordion,
  inputs: ['afAnimation', 'afCollapseText', 'afExpandText', 'afExpanded', 'afHeading', 'afHeadingLevel', 'afId', 'afVariation']
})
@Component({
  selector: 'digi-expandable-accordion',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afAnimation', 'afCollapseText', 'afExpandText', 'afExpanded', 'afHeading', 'afHeadingLevel', 'afId', 'afVariation'],
  standalone: true
})
export class DigiExpandableAccordion {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiExpandableAccordion extends Components.DigiExpandableAccordion {
  /**
   * Buttonelementets 'onclick'-event. @en The button element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiFormCheckbox,
  inputs: ['afAriaDescribedby', 'afAriaLabel', 'afAriaLabelledby', 'afAutofocus', 'afChecked', 'afDescription', 'afId', 'afIndeterminate', 'afInputRef', 'afLabel', 'afLayout', 'afName', 'afRequired', 'afTabIndex', 'afValidation', 'afValue', 'afVariation', 'checked', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-checkbox',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afAriaDescribedby', 'afAriaLabel', 'afAriaLabelledby', 'afAutofocus', 'afChecked', 'afDescription', 'afId', 'afIndeterminate', 'afInputRef', 'afLabel', 'afLayout', 'afName', 'afRequired', 'afTabIndex', 'afValidation', 'afValue', 'afVariation', 'checked', 'value'],
  standalone: true
})
export class DigiFormCheckbox {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnBlur', 'afOnFocus', 'afOnFocusout', 'afOnInput', 'afOnDirty', 'afOnTouched', 'afOnReady']);
  }
}


export declare interface DigiFormCheckbox extends Components.DigiFormCheckbox {
  /**
   * Inputelementets 'onchange'-event. @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onblur'-event. @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocus'-event. @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocusout'-event. @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'oninput'-event. @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'oninput' @en First time the input element receives an input
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'onblur' @en First time the input element is blurred
   */
  afOnTouched: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiFormFieldset,
  inputs: ['afForm', 'afId', 'afLegend', 'afName']
})
@Component({
  selector: 'digi-form-fieldset',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afForm', 'afId', 'afLegend', 'afName'],
  standalone: true
})
export class DigiFormFieldset {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiFormFieldset extends Components.DigiFormFieldset {}


@ProxyCmp({
  defineCustomElementFn: defineDigiFormFilter,
  inputs: ['afAlignRight', 'afAutofocus', 'afCheckItems', 'afFilterButtonAriaDescribedby', 'afFilterButtonAriaLabel', 'afFilterButtonText', 'afHideResetButton', 'afId', 'afListItems', 'afName', 'afResetButtonText', 'afResetButtonTextAriaLabel', 'afSubmitButtonText', 'afSubmitButtonTextAriaLabel']
})
@Component({
  selector: 'digi-form-filter',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afAlignRight', 'afAutofocus', 'afCheckItems', 'afFilterButtonAriaDescribedby', 'afFilterButtonAriaLabel', 'afFilterButtonText', 'afHideResetButton', 'afId', 'afListItems', 'afName', 'afResetButtonText', 'afResetButtonTextAriaLabel', 'afSubmitButtonText', 'afSubmitButtonTextAriaLabel'],
  standalone: true
})
export class DigiFormFilter {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnFocusout', 'afOnSubmitFilters', 'afOnFilterClosed', 'afOnResetFilters', 'afOnChange']);
  }
}


import type { FormFilterState as IDigiFormFilterFormFilterState } from '@digi/core/components';
import type { FilterChange as IDigiFormFilterFilterChange } from '@digi/core/components';

export declare interface DigiFormFilter extends Components.DigiFormFilter {
  /**
   * När fokus går över till ett annat element @en When focus moves on to another element
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Vid uppdatering av filtret @en At filter submit
   */
  afOnSubmitFilters: EventEmitter<CustomEvent<any>>;
  /**
   * När filtret stängs utan att valda alternativ bekräftats @en When the filter is closed without confirming the selected options
   */
  afOnFilterClosed: EventEmitter<CustomEvent<IDigiFormFilterFormFilterState>>;
  /**
   * Vid reset av filtret @en At filter reset
   */
  afOnResetFilters: EventEmitter<CustomEvent<any>>;
  /**
   * När ett filter ändras @en When a filter is changed
   */
  afOnChange: EventEmitter<CustomEvent<IDigiFormFilterFilterChange>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiFormFilterPresentational,
  inputs: ['afAlignRight', 'afFilterButtonAriaDescribedby', 'afFilterButtonAriaLabel', 'afFilterButtonText', 'afHideResetButton', 'afName', 'afResetButtonText', 'afResetButtonTextAriaLabel', 'afSubmitButtonText', 'afSubmitButtonTextAriaLabel', 'autoFocus', 'checked', 'listItems', 'showMenu', 'tabbable']
})
@Component({
  selector: 'digi-form-filter-presentational',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afAlignRight', 'afFilterButtonAriaDescribedby', 'afFilterButtonAriaLabel', 'afFilterButtonText', 'afHideResetButton', 'afName', 'afResetButtonText', 'afResetButtonTextAriaLabel', 'afSubmitButtonText', 'afSubmitButtonTextAriaLabel', 'autoFocus', 'checked', 'listItems', 'showMenu', 'tabbable'],
  standalone: true
})
export class DigiFormFilterPresentational {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['toggleMenu', 'submitFilter', 'resetFilter', 'changeFilter', 'refs']);
  }
}


import type { FormFilterState as IDigiFormFilterPresentationalFormFilterState } from '@digi/core/components';
import type { FilterChange as IDigiFormFilterPresentationalFilterChange } from '@digi/core/components';
import type { RefIndex as IDigiFormFilterPresentationalRefIndex } from '@digi/core/components';
import type { FocusElement as IDigiFormFilterPresentationalFocusElement } from '@digi/core/components';

export declare interface DigiFormFilterPresentational extends Components.DigiFormFilterPresentational {
  /**
   * När menyn öppnas / stängs. Skickar med information om meny visas eller inte. @en On menu open / close along with information if menu is shown or not.
   */
  toggleMenu: EventEmitter<CustomEvent<boolean>>;
  /**
   * När filtervalet är godkänt. Skickar information med alla filter samt ikryssad status. @en On filter submission, contains current filters and corresponding check status.
   */
  submitFilter: EventEmitter<CustomEvent<IDigiFormFilterPresentationalFormFilterState>>;
  /**
   * Vid reset @en On reset
   */
  resetFilter: EventEmitter<CustomEvent<any>>;
  /**
   * När ett filter-alternativ blir markerat / avmarkerat @en When a filter-item is checked / unchecked
   */
  changeFilter: EventEmitter<CustomEvent<IDigiFormFilterPresentationalFilterChange>>;
  /**
   * När referenser till HTML-noder är redo @en When references for HTML nodes are ready
   */
  refs: EventEmitter<CustomEvent<IDigiFormFilterPresentationalRefIndex<IDigiFormFilterPresentationalFocusElement>>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiFormInput,
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAriaActivedescendant', 'afAriaAutocomplete', 'afAriaControls', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutocomplete', 'afAutofocus', 'afButtonVariation', 'afDirname', 'afId', 'afInputmode', 'afLabel', 'afLabelDescription', 'afList', 'afMax', 'afMaxlength', 'afMin', 'afMinlength', 'afName', 'afRequired', 'afRequiredText', 'afRole', 'afStep', 'afType', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-input',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAriaActivedescendant', 'afAriaAutocomplete', 'afAriaControls', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutocomplete', 'afAutofocus', 'afButtonVariation', 'afDirname', 'afId', 'afInputmode', 'afLabel', 'afLabelDescription', 'afList', 'afMax', 'afMaxlength', 'afMin', 'afMinlength', 'afName', 'afRequired', 'afRequiredText', 'afRole', 'afStep', 'afType', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value'],
  standalone: true
})
export class DigiFormInput {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnBlur', 'afOnKeyup', 'afOnFocus', 'afOnFocusout', 'afOnInput', 'afOnDirty', 'afOnTouched', 'afOnReady']);
  }
}


export declare interface DigiFormInput extends Components.DigiFormInput {
  /**
   * Inputelementets 'onchange'-event. @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onblur'-event. @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onkeyup'-event. @en The input element's 'onkeyup' event.
   */
  afOnKeyup: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocus'-event. @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocusout'-event. @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'oninput'-event. @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'oninput' @en First time the input element receives an input
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'onblur' @en First time the input element is blurred
   */
  afOnTouched: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiFormInputSearch,
  inputs: ['afAriaActivedescendant', 'afAriaAutocomplete', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutocomplete', 'afAutofocus', 'afButtonAriaLabel', 'afButtonAriaLabelledby', 'afButtonText', 'afButtonType', 'afButtonVariation', 'afHideButton', 'afId', 'afLabel', 'afLabelDescription', 'afName', 'afType', 'afValue', 'afVariation', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-input-search',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afAriaActivedescendant', 'afAriaAutocomplete', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutocomplete', 'afAutofocus', 'afButtonAriaLabel', 'afButtonAriaLabelledby', 'afButtonText', 'afButtonType', 'afButtonVariation', 'afHideButton', 'afId', 'afLabel', 'afLabelDescription', 'afName', 'afType', 'afValue', 'afVariation', 'value'],
  standalone: true
})
export class DigiFormInputSearch {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnFocusOutside', 'afOnFocusInside', 'afOnClickOutside', 'afOnClickInside', 'afOnChange', 'afOnBlur', 'afOnKeyup', 'afOnFocus', 'afOnFocusout', 'afOnInput', 'afOnClick', 'afOnSubmitSearch', 'afOnReady']);
  }
}


export declare interface DigiFormInputSearch extends Components.DigiFormInputSearch {
  /**
   * Vid fokus utanför komponenten. @en When focus is outside component.
   */
  afOnFocusOutside: EventEmitter<CustomEvent<any>>;
  /**
   * Vid fokus inuti komponenten. @en When focus is inside component.
   */
  afOnFocusInside: EventEmitter<CustomEvent<any>>;
  /**
   * Vid klick utanför komponenten. @en When click outside component.
   */
  afOnClickOutside: EventEmitter<CustomEvent<any>>;
  /**
   * Vid klick inuti komponenten. @en When click inside component.
   */
  afOnClickInside: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onchange'-event. @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onblur'-event. @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onkeyup'-event. @en The input element's 'onkeyup' event.
   */
  afOnKeyup: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocus'-event. @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocusout'-event. @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'oninput'-event. @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Knappelementets 'onclick'-event. @en The button element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<any>>;
  /**
   * Komponent skickar detta Event när sökfältets knapp är klickat eller enter knapp är tryckt när man har fokus på sökfältet. Returnerar fältets value @en Component sends this event when the search field button is clicked or enter button is pressed when focus is on the search field. Returns field value
   */
  afOnSubmitSearch: EventEmitter<CustomEvent<string>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiFormLabel,
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afDescription', 'afFor', 'afId', 'afLabel', 'afRequired', 'afRequiredText']
})
@Component({
  selector: 'digi-form-label',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afDescription', 'afFor', 'afId', 'afLabel', 'afRequired', 'afRequiredText'],
  standalone: true
})
export class DigiFormLabel {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiFormLabel extends Components.DigiFormLabel {}


@ProxyCmp({
  defineCustomElementFn: defineDigiFormRadiobutton,
  inputs: ['afAriaDescribedby', 'afAriaLabelledby', 'afAutofocus', 'afChecked', 'afId', 'afLabel', 'afLayout', 'afName', 'afRequired', 'afValidation', 'afValue', 'afVariation', 'checked', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-radiobutton',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afAriaDescribedby', 'afAriaLabelledby', 'afAutofocus', 'afChecked', 'afId', 'afLabel', 'afLayout', 'afName', 'afRequired', 'afValidation', 'afValue', 'afVariation', 'checked', 'value'],
  standalone: true
})
export class DigiFormRadiobutton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnBlur', 'afOnFocus', 'afOnFocusout', 'afOnInput', 'afOnDirty', 'afOnTouched', 'afOnReady']);
  }
}


export declare interface DigiFormRadiobutton extends Components.DigiFormRadiobutton {
  /**
   * Inputelementets 'onchange'-event. @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onblur'-event. @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocus'-event. @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocusout'-event. @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'oninput'-event. @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'oninput' @en First time the input element receives an input
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'onblur' @en First time the input element is blurred
   */
  afOnTouched: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiFormRadiogroup,
  inputs: ['afName', 'afValue', 'value']
})
@Component({
  selector: 'digi-form-radiogroup',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afName', 'afValue', 'value'],
  standalone: true
})
export class DigiFormRadiogroup {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnGroupChange']);
  }
}


export declare interface DigiFormRadiogroup extends Components.DigiFormRadiogroup {
  /**
   * Event när värdet ändras @en Event when value changes
   */
  afOnGroupChange: EventEmitter<CustomEvent<string>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiFormSelect,
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAriaLabel', 'afAutofocus', 'afDescription', 'afDisableValidation', 'afId', 'afLabel', 'afName', 'afPlaceholder', 'afRequired', 'afRequiredText', 'afStartSelected', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-select',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAriaLabel', 'afAutofocus', 'afDescription', 'afDisableValidation', 'afId', 'afLabel', 'afName', 'afPlaceholder', 'afRequired', 'afRequiredText', 'afStartSelected', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value'],
  standalone: true
})
export class DigiFormSelect {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnSelect', 'afOnFocus', 'afOnFocusout', 'afOnBlur', 'afOnDirty', 'afOnReady']);
  }
}


export declare interface DigiFormSelect extends Components.DigiFormSelect {

  afOnChange: EventEmitter<CustomEvent<any>>;

  afOnSelect: EventEmitter<CustomEvent<any>>;

  afOnFocus: EventEmitter<CustomEvent<any>>;

  afOnFocusout: EventEmitter<CustomEvent<any>>;

  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Sker första gången väljarelementet ändras. @en First time the select element is changed.
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiFormTextarea,
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAriaActivedescendant', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutofocus', 'afId', 'afLabel', 'afLabelDescription', 'afMaxlength', 'afMinlength', 'afName', 'afRequired', 'afRequiredText', 'afRole', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value'],
  methods: ['afMGetFormControlElement']
})
@Component({
  selector: 'digi-form-textarea',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afAnnounceIfOptional', 'afAnnounceIfOptionalText', 'afAriaActivedescendant', 'afAriaDescribedby', 'afAriaLabelledby', 'afAutofocus', 'afId', 'afLabel', 'afLabelDescription', 'afMaxlength', 'afMinlength', 'afName', 'afRequired', 'afRequiredText', 'afRole', 'afValidation', 'afValidationText', 'afValue', 'afVariation', 'value'],
  standalone: true
})
export class DigiFormTextarea {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnBlur', 'afOnKeyup', 'afOnFocus', 'afOnFocusout', 'afOnInput', 'afOnDirty', 'afOnTouched', 'afOnReady']);
  }
}


export declare interface DigiFormTextarea extends Components.DigiFormTextarea {
  /**
   * Textareatelementets 'onchange'-event. @en The textarea element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'onblur'-event. @en The textarea element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'onkeyup'-event. @en The textarea element's 'onkeyup' event.
   */
  afOnKeyup: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'onfocus'-event. @en The textarea element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'onfocusout'-event. @en The textarea element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'oninput'-event. @en The textarea element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid textareaelementets första 'oninput' @en First time the textarea element receives an input
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid textareaelementets första 'onblur' @en First time the textelement is blurred
   */
  afOnTouched: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiFormValidationMessage,
  inputs: ['afVariation']
})
@Component({
  selector: 'digi-form-validation-message',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afVariation'],
  standalone: true
})
export class DigiFormValidationMessage {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiFormValidationMessage extends Components.DigiFormValidationMessage {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIcon,
  inputs: ['afDesc', 'afName', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afName', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIcon {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIcon extends Components.DigiIcon {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconBars,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-bars',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconBars {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconBars extends Components.DigiIconBars {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconCheck,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-check',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconCheck {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCheck extends Components.DigiIconCheck {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconCheckCircleRegAlt,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-check-circle-reg-alt',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconCheckCircleRegAlt {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCheckCircleRegAlt extends Components.DigiIconCheckCircleRegAlt {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconCheckCircleSolid,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-check-circle-solid',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconCheckCircleSolid {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCheckCircleSolid extends Components.DigiIconCheckCircleSolid {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconChevronDown,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-chevron-down',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconChevronDown {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconChevronDown extends Components.DigiIconChevronDown {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconChevronLeft,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-chevron-left',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconChevronLeft {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconChevronLeft extends Components.DigiIconChevronLeft {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconChevronRight,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-chevron-right',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconChevronRight {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconChevronRight extends Components.DigiIconChevronRight {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconChevronUp,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-chevron-up',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconChevronUp {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconChevronUp extends Components.DigiIconChevronUp {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconCopy,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-copy',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconCopy {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconCopy extends Components.DigiIconCopy {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconDangerOutline,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-danger-outline',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconDangerOutline {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconDangerOutline extends Components.DigiIconDangerOutline {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconDownload,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-download',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconDownload {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconDownload extends Components.DigiIconDownload {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconExclamationCircleFilled,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-exclamation-circle-filled',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconExclamationCircleFilled {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconExclamationCircleFilled extends Components.DigiIconExclamationCircleFilled {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconExclamationTriangle,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-exclamation-triangle',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconExclamationTriangle {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconExclamationTriangle extends Components.DigiIconExclamationTriangle {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconExclamationTriangleWarning,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-exclamation-triangle-warning',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconExclamationTriangleWarning {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconExclamationTriangleWarning extends Components.DigiIconExclamationTriangleWarning {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconExternalLinkAlt,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-external-link-alt',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconExternalLinkAlt {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconExternalLinkAlt extends Components.DigiIconExternalLinkAlt {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconMinus,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-minus',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconMinus {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconMinus extends Components.DigiIconMinus {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconNotificationSucces,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-notification-succes',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconNotificationSucces {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconNotificationSucces extends Components.DigiIconNotificationSucces {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconNotificationWarning,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-notification-warning',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconNotificationWarning {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconNotificationWarning extends Components.DigiIconNotificationWarning {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconPaperclip,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-paperclip',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconPaperclip {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconPaperclip extends Components.DigiIconPaperclip {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconPlus,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-plus',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconPlus {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconPlus extends Components.DigiIconPlus {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconSearch,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-search',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconSearch {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconSearch extends Components.DigiIconSearch {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconSpinner,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-spinner',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconSpinner {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconSpinner extends Components.DigiIconSpinner {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconTrash,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-trash',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconTrash {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconTrash extends Components.DigiIconTrash {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconValidationSuccess,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-validation-success',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconValidationSuccess {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconValidationSuccess extends Components.DigiIconValidationSuccess {}


@ProxyCmp({
  defineCustomElementFn: defineDigiIconX,
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle']
})
@Component({
  selector: 'digi-icon-x',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDesc', 'afSvgAriaHidden', 'afSvgAriaLabelledby', 'afTitle'],
  standalone: true
})
export class DigiIconX {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiIconX extends Components.DigiIconX {}


@ProxyCmp({
  defineCustomElementFn: defineDigiLayoutBlock,
  inputs: ['afContainer', 'afMarginBottom', 'afMarginTop', 'afVariation', 'afVerticalPadding']
})
@Component({
  selector: 'digi-layout-block',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afContainer', 'afMarginBottom', 'afMarginTop', 'afVariation', 'afVerticalPadding'],
  standalone: true
})
export class DigiLayoutBlock {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLayoutBlock extends Components.DigiLayoutBlock {}


@ProxyCmp({
  defineCustomElementFn: defineDigiLayoutColumns,
  inputs: ['afElement', 'afVariation']
})
@Component({
  selector: 'digi-layout-columns',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afElement', 'afVariation'],
  standalone: true
})
export class DigiLayoutColumns {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLayoutColumns extends Components.DigiLayoutColumns {}


@ProxyCmp({
  defineCustomElementFn: defineDigiLayoutContainer,
  inputs: ['afMarginBottom', 'afMarginTop', 'afNoGutter', 'afVariation', 'afVerticalPadding']
})
@Component({
  selector: 'digi-layout-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afMarginBottom', 'afMarginTop', 'afNoGutter', 'afVariation', 'afVerticalPadding'],
  standalone: true
})
export class DigiLayoutContainer {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLayoutContainer extends Components.DigiLayoutContainer {}


@ProxyCmp({
  defineCustomElementFn: defineDigiLayoutMediaObject,
  inputs: ['afAlignment']
})
@Component({
  selector: 'digi-layout-media-object',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afAlignment'],
  standalone: true
})
export class DigiLayoutMediaObject {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLayoutMediaObject extends Components.DigiLayoutMediaObject {}


@ProxyCmp({
  defineCustomElementFn: defineDigiLink,
  inputs: ['afDescribedby', 'afHref', 'afLinkContainer', 'afOverrideLink', 'afTarget', 'afVariation'],
  methods: ['afMGetLinkElement']
})
@Component({
  selector: 'digi-link',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDescribedby', 'afHref', 'afLinkContainer', 'afOverrideLink', 'afTarget', 'afVariation'],
  standalone: true
})
export class DigiLink {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick', 'afOnReady']);
  }
}


export declare interface DigiLink extends Components.DigiLink {
  /**
   * Länkelementets 'onclick'-event. @en The link element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiLinkExternal,
  inputs: ['afDescribedby', 'afHref', 'afTarget', 'afVariation']
})
@Component({
  selector: 'digi-link-external',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDescribedby', 'afHref', 'afTarget', 'afVariation'],
  standalone: true
})
export class DigiLinkExternal {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiLinkExternal extends Components.DigiLinkExternal {
  /**
   * Länkelementets 'onclick'-event. @en The link element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiLinkInternal,
  inputs: ['afDescribedby', 'afHref', 'afLinkContainer', 'afOverrideLink', 'afVariation']
})
@Component({
  selector: 'digi-link-internal',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDescribedby', 'afHref', 'afLinkContainer', 'afOverrideLink', 'afVariation'],
  standalone: true
})
export class DigiLinkInternal {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiLinkInternal extends Components.DigiLinkInternal {
  /**
   * Länkelementets 'onclick'-event. @en The link element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiLoaderSkeleton,
  inputs: ['afCount', 'afVariation']
})
@Component({
  selector: 'digi-loader-skeleton',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afCount', 'afVariation'],
  standalone: true
})
export class DigiLoaderSkeleton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLoaderSkeleton extends Components.DigiLoaderSkeleton {}


@ProxyCmp({
  defineCustomElementFn: defineDigiLoaderSpinner,
  inputs: ['afSize', 'afText']
})
@Component({
  selector: 'digi-loader-spinner',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afSize', 'afText'],
  standalone: true
})
export class DigiLoaderSpinner {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiLoaderSpinner extends Components.DigiLoaderSpinner {}


@ProxyCmp({
  defineCustomElementFn: defineDigiMediaFigure,
  inputs: ['afAlignment', 'afFigcaption']
})
@Component({
  selector: 'digi-media-figure',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afAlignment', 'afFigcaption'],
  standalone: true
})
export class DigiMediaFigure {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiMediaFigure extends Components.DigiMediaFigure {}


@ProxyCmp({
  defineCustomElementFn: defineDigiMediaImage,
  inputs: ['afAlt', 'afAriaLabel', 'afFullwidth', 'afHeight', 'afObserverOptions', 'afSrc', 'afSrcset', 'afTitle', 'afUnlazy', 'afWidth']
})
@Component({
  selector: 'digi-media-image',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afAlt', 'afAriaLabel', 'afFullwidth', 'afHeight', 'afObserverOptions', 'afSrc', 'afSrcset', 'afTitle', 'afUnlazy', 'afWidth'],
  standalone: true
})
export class DigiMediaImage {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnLoad']);
  }
}


export declare interface DigiMediaImage extends Components.DigiMediaImage {
  /**
   * Bildlementets 'onload'-event. @en The image element's 'onload' event.
   */
  afOnLoad: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationContextMenu,
  inputs: ['afHorizontalPosition', 'afIcon', 'afId', 'afNavigationContextMenuItems', 'afStartSelected', 'afText']
})
@Component({
  selector: 'digi-navigation-context-menu',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afHorizontalPosition', 'afIcon', 'afId', 'afNavigationContextMenuItems', 'afStartSelected', 'afText'],
  standalone: true
})
export class DigiNavigationContextMenu {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnInactive', 'afOnActive', 'afOnBlur', 'afOnChange', 'afOnToggle', 'afOnSelect']);
  }
}


export declare interface DigiNavigationContextMenu extends Components.DigiNavigationContextMenu {
  /**
   * När komponenten stängs @en When component gets inactive
   */
  afOnInactive: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten öppnas @en When component gets active
   */
  afOnActive: EventEmitter<CustomEvent<any>>;
  /**
   * När fokus sätts utanför komponenten @en When focus is move outside of component
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Vid navigering till nytt listobjekt @en When navigating to a new list item
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Toggleknappens 'onclick'-event @en The toggle button's 'onclick'-event
   */
  afOnToggle: EventEmitter<CustomEvent<any>>;
  /**
   * 'onclick'-event på knappelementen i listan @en List item buttons' 'onclick'-event
   */
  afOnSelect: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationContextMenuItem,
  inputs: ['afDir', 'afHref', 'afLang', 'afText', 'afType']
})
@Component({
  selector: 'digi-navigation-context-menu-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDir', 'afHref', 'afLang', 'afText', 'afType'],
  standalone: true
})
export class DigiNavigationContextMenuItem {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiNavigationContextMenuItem extends Components.DigiNavigationContextMenuItem {}


@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationSidebar,
  inputs: ['afActive', 'afBackdrop', 'afCloseButtonAriaLabel', 'afCloseButtonPosition', 'afCloseButtonText', 'afCloseFocusableElement', 'afFocusableElement', 'afHeading', 'afHeadingLevel', 'afHideHeader', 'afId', 'afMobilePosition', 'afMobileVariation', 'afPosition', 'afStickyHeader', 'afVariation']
})
@Component({
  selector: 'digi-navigation-sidebar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afActive', 'afBackdrop', 'afCloseButtonAriaLabel', 'afCloseButtonPosition', 'afCloseButtonText', 'afCloseFocusableElement', 'afFocusableElement', 'afHeading', 'afHeadingLevel', 'afHideHeader', 'afId', 'afMobilePosition', 'afMobileVariation', 'afPosition', 'afStickyHeader', 'afVariation'],
  standalone: true
})
export class DigiNavigationSidebar {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClose', 'afOnEsc', 'afOnBackdropClick', 'afOnToggle']);
  }
}


export declare interface DigiNavigationSidebar extends Components.DigiNavigationSidebar {
  /**
   * Stängknappens 'onclick'-event @en Close button's 'onclick' event
   */
  afOnClose: EventEmitter<CustomEvent<any>>;
  /**
   * Stängning av sidofält med esc-tangenten @en At close/open of sidebar using esc-key
   */
  afOnEsc: EventEmitter<CustomEvent<any>>;
  /**
   * Stängning av sidofält med klick på skuggan bakom menyn @en At close of sidebar when clicking on backdrop
   */
  afOnBackdropClick: EventEmitter<CustomEvent<any>>;
  /**
   * Vid öppning/stängning av sidofält @en At close/open of sidebar
   */
  afOnToggle: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationSidebarButton,
  inputs: ['afAriaLabel', 'afId', 'afText']
})
@Component({
  selector: 'digi-navigation-sidebar-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afAriaLabel', 'afId', 'afText'],
  standalone: true
})
export class DigiNavigationSidebarButton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnToggle']);
  }
}


export declare interface DigiNavigationSidebarButton extends Components.DigiNavigationSidebarButton {
  /**
   * Toggleknappens 'onclick'-event @en The toggle button's 'onclick'-event
   */
  afOnToggle: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationVerticalMenu,
  inputs: ['afActiveIndicatorSize', 'afAriaLabel', 'afId', 'afVariation'],
  methods: ['afMSetCurrentActiveLevel']
})
@Component({
  selector: 'digi-navigation-vertical-menu',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afActiveIndicatorSize', 'afAriaLabel', 'afId', 'afVariation'],
  standalone: true
})
export class DigiNavigationVerticalMenu {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnReady']);
  }
}


export declare interface DigiNavigationVerticalMenu extends Components.DigiNavigationVerticalMenu {
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiNavigationVerticalMenuItem,
  inputs: ['afActive', 'afActiveSubnav', 'afAriaCurrent', 'afHasSubnav', 'afHref', 'afId', 'afLang', 'afOverrideLink', 'afText']
})
@Component({
  selector: 'digi-navigation-vertical-menu-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afActive', 'afActiveSubnav', 'afAriaCurrent', 'afHasSubnav', 'afHref', 'afId', 'afLang', 'afOverrideLink', 'afText'],
  standalone: true
})
export class DigiNavigationVerticalMenuItem {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiNavigationVerticalMenuItem extends Components.DigiNavigationVerticalMenuItem {
  /**
   * Länken och toggle-knappens 'onclick'-event @en Link and toggle buttons 'onclick' event
   */
  afOnClick: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiProgressIndicator,
  inputs: ['afCompletedSteps', 'afErrorStep', 'afId', 'afRole', 'afStepHeading', 'afStepNextLabel', 'afStepNextText', 'afStepText', 'afStepsSeparator', 'afTotalSteps', 'afValidationLabel', 'afVariation', 'afWarningStep', 'radius', 'stroke']
})
@Component({
  selector: 'digi-progress-indicator',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afCompletedSteps', 'afErrorStep', 'afId', 'afRole', 'afStepHeading', 'afStepNextLabel', 'afStepNextText', 'afStepText', 'afStepsSeparator', 'afTotalSteps', 'afValidationLabel', 'afVariation', 'afWarningStep', 'radius', 'stroke'],
  standalone: true
})
export class DigiProgressIndicator {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiProgressIndicator extends Components.DigiProgressIndicator {}


@ProxyCmp({
  defineCustomElementFn: defineDigiProgressStep,
  inputs: ['afHeading', 'afHeadingLevel', 'afId', 'afIsLast', 'afStepStatus', 'afVariation']
})
@Component({
  selector: 'digi-progress-step',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afHeading', 'afHeadingLevel', 'afId', 'afIsLast', 'afStepStatus', 'afVariation'],
  standalone: true
})
export class DigiProgressStep {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiProgressStep extends Components.DigiProgressStep {}


@ProxyCmp({
  defineCustomElementFn: defineDigiProgressSteps,
  inputs: ['afCurrentStep', 'afHeadingLevel', 'afVariation'],
  methods: ['afMNext', 'afMPrevious']
})
@Component({
  selector: 'digi-progress-steps',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afCurrentStep', 'afHeadingLevel', 'afVariation'],
  standalone: true
})
export class DigiProgressSteps {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnReady']);
  }
}


export declare interface DigiProgressSteps extends Components.DigiProgressSteps {
  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet. @en When the component and slots are loaded and initialized this event will trigger.
   */
  afOnReady: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiProgressbar,
  inputs: ['afCompletedSteps', 'afId', 'afRole', 'afStepsLabel', 'afStepsSeparator', 'afTotalSteps', 'afVariation']
})
@Component({
  selector: 'digi-progressbar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afCompletedSteps', 'afId', 'afRole', 'afStepsLabel', 'afStepsSeparator', 'afTotalSteps', 'afVariation'],
  standalone: true
})
export class DigiProgressbar {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiProgressbar extends Components.DigiProgressbar {}


@ProxyCmp({
  defineCustomElementFn: defineDigiTag,
  inputs: ['afAriaLabel', 'afNoIcon', 'afSize', 'afText']
})
@Component({
  selector: 'digi-tag',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afAriaLabel', 'afNoIcon', 'afSize', 'afText'],
  standalone: true
})
export class DigiTag {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClick']);
  }
}


export declare interface DigiTag extends Components.DigiTag {
  /**
   * Taggelementets 'onclick'-event. @en The tag elements 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiTypography,
  inputs: ['afVariation']
})
@Component({
  selector: 'digi-typography',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afVariation'],
  standalone: true
})
export class DigiTypography {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiTypography extends Components.DigiTypography {}


@ProxyCmp({
  defineCustomElementFn: defineDigiTypographyMeta,
  inputs: ['afVariation']
})
@Component({
  selector: 'digi-typography-meta',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afVariation'],
  standalone: true
})
export class DigiTypographyMeta {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiTypographyMeta extends Components.DigiTypographyMeta {}


@ProxyCmp({
  defineCustomElementFn: defineDigiTypographyPreamble
})
@Component({
  selector: 'digi-typography-preamble',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: [],
  standalone: true
})
export class DigiTypographyPreamble {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiTypographyPreamble extends Components.DigiTypographyPreamble {}


@ProxyCmp({
  defineCustomElementFn: defineDigiTypographyTime,
  inputs: ['afDateTime', 'afLocale', 'afVariation']
})
@Component({
  selector: 'digi-typography-time',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDateTime', 'afLocale', 'afVariation'],
  standalone: true
})
export class DigiTypographyTime {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface DigiTypographyTime extends Components.DigiTypographyTime {}


@ProxyCmp({
  defineCustomElementFn: defineDigiUtilBreakpointObserver
})
@Component({
  selector: 'digi-util-breakpoint-observer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: [],
  standalone: true
})
export class DigiUtilBreakpointObserver {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnSmall', 'afOnMedium', 'afOnLarge', 'afOnXLarge']);
  }
}


export declare interface DigiUtilBreakpointObserver extends Components.DigiUtilBreakpointObserver {
  /**
   * Vid inläsning av sida samt vid uppdatering av brytpunkt @en At page load and at change of breakpoint on page
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Vid brytpunkt 'small' @en At breakpoint 'small'
   */
  afOnSmall: EventEmitter<CustomEvent<any>>;
  /**
   * Vid brytpunkt 'medium' @en At breakpoint 'medium'
   */
  afOnMedium: EventEmitter<CustomEvent<any>>;
  /**
   * Vid brytpunkt 'large' @en At breakpoint 'large'
   */
  afOnLarge: EventEmitter<CustomEvent<any>>;
  /**
   * Vid brytpunkt 'xlarge' @en At breakpoint 'xlarge'
   */
  afOnXLarge: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiUtilDetectClickOutside,
  inputs: ['afDataIdentifier']
})
@Component({
  selector: 'digi-util-detect-click-outside',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDataIdentifier'],
  standalone: true
})
export class DigiUtilDetectClickOutside {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnClickOutside', 'afOnClickInside']);
  }
}


export declare interface DigiUtilDetectClickOutside extends Components.DigiUtilDetectClickOutside {
  /**
   * Vid klick utanför komponenten @en When click detected outside of component
   */
  afOnClickOutside: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * Vid klick inuti komponenten @en When click detected inside of component
   */
  afOnClickInside: EventEmitter<CustomEvent<MouseEvent>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiUtilDetectFocusOutside,
  inputs: ['afDataIdentifier']
})
@Component({
  selector: 'digi-util-detect-focus-outside',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afDataIdentifier'],
  standalone: true
})
export class DigiUtilDetectFocusOutside {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnFocusOutside', 'afOnFocusInside']);
  }
}


export declare interface DigiUtilDetectFocusOutside extends Components.DigiUtilDetectFocusOutside {
  /**
   * Vid fokus utanför komponenten @en When focus detected outside of component
   */
  afOnFocusOutside: EventEmitter<CustomEvent<FocusEvent>>;
  /**
   * Vid fokus inuti komponenten @en When focus detected inside of component
   */
  afOnFocusInside: EventEmitter<CustomEvent<FocusEvent>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiUtilIntersectionObserver,
  inputs: ['afOnce', 'afOptions']
})
@Component({
  selector: 'digi-util-intersection-observer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afOnce', 'afOptions'],
  standalone: true
})
export class DigiUtilIntersectionObserver {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange', 'afOnIntersect', 'afOnUnintersect']);
  }
}


export declare interface DigiUtilIntersectionObserver extends Components.DigiUtilIntersectionObserver {
  /**
   * Vid statusförändring mellan 'unintersected' och 'intersected' @en On change between unintersected and intersected
   */
  afOnChange: EventEmitter<CustomEvent<boolean>>;
  /**
   * Vid statusförändring till 'intersected' @en On every change to intersected.
   */
  afOnIntersect: EventEmitter<CustomEvent<any>>;
  /**
   * Vid statusförändring till 'unintersected'
On every change to unintersected
   */
  afOnUnintersect: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiUtilKeydownHandler
})
@Component({
  selector: 'digi-util-keydown-handler',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: [],
  standalone: true
})
export class DigiUtilKeydownHandler {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnEsc', 'afOnEnter', 'afOnTab', 'afOnSpace', 'afOnShiftTab', 'afOnUp', 'afOnDown', 'afOnLeft', 'afOnRight', 'afOnHome', 'afOnEnd', 'afOnKeyDown']);
  }
}


export declare interface DigiUtilKeydownHandler extends Components.DigiUtilKeydownHandler {
  /**
   * Vid keydown på escape @en At keydown on escape key
   */
  afOnEsc: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på enter @en At keydown on enter key
   */
  afOnEnter: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på tab @en At keydown on tab key
   */
  afOnTab: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på space @en At keydown on space key
   */
  afOnSpace: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på skift och tabb @en At keydown on shift and tab key
   */
  afOnShiftTab: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på pil upp @en At keydown on up arrow key
   */
  afOnUp: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på pil ner @en At keydown on down arrow key
   */
  afOnDown: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på pil vänster @en At keydown on left arrow key
   */
  afOnLeft: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på pil höger @en At keydown on right arrow key
   */
  afOnRight: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på home @en At keydown on home key
   */
  afOnHome: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på end @en At keydown on end key
   */
  afOnEnd: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på alla tangenter @en At keydown on any key
   */
  afOnKeyDown: EventEmitter<CustomEvent<KeyboardEvent>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiUtilKeyupHandler
})
@Component({
  selector: 'digi-util-keyup-handler',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: [],
  standalone: true
})
export class DigiUtilKeyupHandler {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnEsc', 'afOnEnter', 'afOnTab', 'afOnSpace', 'afOnShiftTab', 'afOnUp', 'afOnDown', 'afOnLeft', 'afOnRight', 'afOnHome', 'afOnEnd', 'afOnKeyUp']);
  }
}


export declare interface DigiUtilKeyupHandler extends Components.DigiUtilKeyupHandler {
  /**
   * Vid keyup på escape @en At keyup on escape key
   */
  afOnEsc: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på enter @en At keyup on enter key
   */
  afOnEnter: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på tab @en At keyup on tab key
   */
  afOnTab: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på space @en At keyup on space key
   */
  afOnSpace: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på skift och tabb @en At keyup on shift and tab key
   */
  afOnShiftTab: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på pil upp @en At keyup on up arrow key
   */
  afOnUp: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på pil ner @en At keyup on down arrow key
   */
  afOnDown: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på pil vänster @en At keyup on left arrow key
   */
  afOnLeft: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på pil höger @en At keyup on right arrow key
   */
  afOnRight: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på home @en At keyup on home key
   */
  afOnHome: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på end @en At keyup on end key
   */
  afOnEnd: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på alla tangenter @en At keyup on any key
   */
  afOnKeyUp: EventEmitter<CustomEvent<KeyboardEvent>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiUtilMutationObserver,
  inputs: ['afHidden', 'afId', 'afOptions']
})
@Component({
  selector: 'digi-util-mutation-observer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['afHidden', 'afId', 'afOptions'],
  standalone: true
})
export class DigiUtilMutationObserver {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange']);
  }
}


export declare interface DigiUtilMutationObserver extends Components.DigiUtilMutationObserver {
  /**
   * När DOM-element läggs till eller tas bort inuti Mutation Observer:n @en When DOM-elements is added or removed inside the Mutation Observer
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineDigiUtilResizeObserver
})
@Component({
  selector: 'digi-util-resize-observer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: [],
  standalone: true
})
export class DigiUtilResizeObserver {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['afOnChange']);
  }
}


export declare interface DigiUtilResizeObserver extends Components.DigiUtilResizeObserver {
  /**
   * När komponenten ändrar storlek @en When the component changes size
   */
  afOnChange: EventEmitter<CustomEvent<ResizeObserverEntry>>;
}


