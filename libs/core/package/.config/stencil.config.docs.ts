import { Config } from '@stencil/core';
import { StencilBaseConfig } from '../stencil.config';

console.log('Core - stencil.config.docs');

export const config: Config = {
	...StencilBaseConfig,
	srcDir: '../src',
	outputTargets: [
		{
			type: 'docs-json',
			file: '../../../../dist/libs/core/docs.json'
		}
	]
};
