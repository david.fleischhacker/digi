import { Config } from '@stencil/core';
import { StencilBaseConfig } from '../stencil.config';
import { reactOutputTarget } from '@stencil/react-output-target';

console.log('Core - stencil.config.react');

export const config: Config = {
	srcDir: '../src',
	tsconfig: '../tsconfig.json',
	outputTargets: [
    ...(StencilBaseConfig.outputTargets as any),
		reactOutputTarget({
			componentCorePackage: '@digi/core',
			proxiesFile:
				'../../../libs/core/react/src/lib/stencil-generated/components.ts',
			includeDefineCustomElements: true,
      loaderDir: 'loader'
		})
	]
};
