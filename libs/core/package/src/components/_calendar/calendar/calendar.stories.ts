import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'calendar/digi-calendar',
	parameters: {
		actions: {
			handles: [
				'afOnDateSelectedChange',
				'afOnFocusOutside',
				'afOnClickOutside',
				'afOnDirty'
			]
		}
	},
	argTypes: {
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-calendar',
	'af-id': null,
	'af-init-selected-month': 0,
	'af-selected-date': null,
	'af-prev-aria-label': null,
	'af-next-aria-label': null,
	'af-name-select': '',
	'af-label-select': 'A required label',
	'af-month-selector': true,
	'af-active': true,
	/* html */
	children: `
  <digi-button
    slot="calendar-footer"
    af-variation="primary"
    aria-label="Välj"
    af-size="medium">Välj</digi-button>
  <digi-button
    slot="calendar-footer"
    af-variation="secondary"
    aria-label="Rensa"
    af-size="medium">Rensa</digi-button>`
};
