import { newSpecPage } from '@stencil/core/testing';
import { barChart } from './bar-chart';

describe('bar-chart', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [barChart],
			html: '<bar-chart></bar-chart>'
		});
		expect(root).toEqualHtml(`
      <bar-chart>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </bar-chart>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [barChart],
			html: `<bar-chart first="Stencil" last="'Don't call me a framework' JS"></bar-chart>`
		});
		expect(root).toEqualHtml(`
      <bar-chart first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </bar-chart>
    `);
	});
});
