import { newE2EPage } from '@stencil/core/testing';

describe('digi-code-block', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-code-block></digi-code-block>');

    const element = await page.find('digi-code-block');
    expect(element).toHaveClass('hydrated');
  });
});
