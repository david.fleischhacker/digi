import { newE2EPage } from '@stencil/core/testing';

describe('digi-code', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-code></digi-code>');

    const element = await page.find('digi-code');
    expect(element).toHaveClass('hydrated');
  });
});
