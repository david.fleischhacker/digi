import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { CodeVariation } from './code-variation.enum';
import { CodeLanguage } from './code-language.enum';

export default {
	title: 'code/digi-code',
	argTypes: {
		'af-variation': enumSelect(CodeVariation),
		'af-language': enumSelect(CodeLanguage)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-code',
	'af-code': 'npm run start',
	'af-variation': CodeVariation.LIGHT,
	'af-language': CodeLanguage.HTML
};
