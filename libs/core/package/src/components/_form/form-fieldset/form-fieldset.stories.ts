import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'form/digi-form-fieldset'
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-form-fieldset',
	'af-legend': '',
	'af-name': '',
	'af-form': '',
	'af-id': null,
	/* html */
	children: `
    <digi-form-input af-label="Label"></digi-form-input>
    <br/>
    <digi-button>Submit</digi-button>`
};
