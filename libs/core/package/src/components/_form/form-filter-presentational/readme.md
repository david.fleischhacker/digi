# digi-form-filter

This is a filter component. The component uses a slot to receive and display data, iterating over each item, wraping it with a li-element and
updating the slot input tabindex to -1, focused input to 0.

The component only supports form related input-elements eg. checkboxes and radio buttons.

The component is equipped with keydown handler and emits a list of filter results on submit.

## NOTE

The component is only compatible with checkboxes through its slot at the moment.

<!-- Auto Generated Below -->


## Properties

| Property                          | Attribute                           | Description                                                                                           | Type               | Default     |
| --------------------------------- | ----------------------------------- | ----------------------------------------------------------------------------------------------------- | ------------------ | ----------- |
| `afAlignRight`                    | `af-align-right`                    | Justera dropdown från höger till vänster                                                              | `boolean`          | `undefined` |
| `afFilterButtonAriaDescribedby`   | `af-filter-button-aria-describedby` | Sätter attributet 'aria-describedby' på filterknappen                                                 | `string`           | `undefined` |
| `afFilterButtonAriaLabel`         | `af-filter-button-aria-label`       | Sätter attributet 'aria-label' på filterknappen                                                       | `string`           | `undefined` |
| `afFilterButtonText` _(required)_ | `af-filter-button-text`             | Texten i filterknappen                                                                                | `string`           | `undefined` |
| `afHideResetButton`               | `af-hide-reset-button`              | Döljer rensaknappen när värdet sätts till true                                                        | `boolean`          | `false`     |
| `afName`                          | `af-name`                           | Namnet på filterlistans valda filter vid submit. Används även av legendelementet inne i filterlistan. | `string`           | `undefined` |
| `afResetButtonText` _(required)_  | `af-reset-button-text`              | Texten i rensaknappen                                                                                 | `string`           | `undefined` |
| `afResetButtonTextAriaLabel`      | `af-reset-button-text-aria-label`   | Skärmläsartext för resetknappen                                                                       | `string`           | `undefined` |
| `afSubmitButtonText` _(required)_ | `af-submit-button-text`             | Texten i submitknappen                                                                                | `string`           | `undefined` |
| `afSubmitButtonTextAriaLabel`     | `af-submit-button-text-aria-label`  | Skärmläsartext för submitknappen                                                                      | `string`           | `undefined` |
| `autoFocus`                       | `auto-focus`                        | Sätter attributet 'autofocus'.                                                                        | `boolean`          | `undefined` |
| `checked`                         | --                                  | Indikerar om list-alternativet är ikryssat eller inte. Måste vara id på list-alternativ.              | `string[]`         | `[]`        |
| `listItems`                       | --                                  | Lista med val                                                                                         | `FormFilterItem[]` | `undefined` |
| `showMenu`                        | `show-menu`                         | Indikerar om filter-menyn är synlig                                                                   | `boolean`          | `false`     |
| `tabbable`                        | --                                  |                                                                                                       | `string[]`         | `[]`        |


## Events

| Event          | Description                                                                           | Type                                                                                                                                                                                                                                                                                                                                                    |
| -------------- | ------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `changeFilter` | När ett filter-alternativ blir markerat / avmarkerat                                  | `CustomEvent<FilterChange>`                                                                                                                                                                                                                                                                                                                             |
| `refs`         | När referenser till HTML-noder är redo                                                | `CustomEvent<{ menu: HTMLElement \| { [id: string]: HTMLElement; }; root: HTMLElement \| { [id: string]: HTMLElement; }; toggle: HTMLElement \| { [id: string]: HTMLElement; }; filter: HTMLElement \| { [id: string]: HTMLElement; }; submit: HTMLElement \| { [id: string]: HTMLElement; }; reset: HTMLElement \| { [id: string]: HTMLElement; }; }>` |
| `resetFilter`  | Vid reset                                                                             | `CustomEvent<any>`                                                                                                                                                                                                                                                                                                                                      |
| `submitFilter` | När filtervalet är godkänt. Skickar information med alla filter samt ikryssad status. | `CustomEvent<FormFilterState>`                                                                                                                                                                                                                                                                                                                          |
| `toggleMenu`   | När menyn öppnas / stängs. Skickar med information om meny visas eller inte.          | `CustomEvent<boolean>`                                                                                                                                                                                                                                                                                                                                  |


## Slots

| Slot        | Description                                                 |
| ----------- | ----------------------------------------------------------- |
| `"default"` | Ska vara formulärselement. Till exempel digi-form-checkbox. |


## CSS Custom Properties

| Name                                                           | Description                                                                     |
| -------------------------------------------------------------- | ------------------------------------------------------------------------------- |
| `--digi--form-filter--dropdown--background`                    | var(--digi--color--background--primary);                                        |
| `--digi--form-filter--dropdown--border-radius`                 | var(--digi--border--radius--primary);                                           |
| `--digi--form-filter--dropdown--box-shadow`                    | 0 0.125rem 0.375rem 0 rgba(0, 0, 0, 0.7);                                       |
| `--digi--form-filter--dropdown--margin-top`                    | var(--digi--gutter--small);                                                     |
| `--digi--form-filter--dropdown--min-width`                     | 18.75rem;                                                                       |
| `--digi--form-filter--dropdown--z-index`                       | 999;                                                                            |
| `--digi--form-filter--footer--background`                      | var(--digi--color--background--secondary);                                      |
| `--digi--form-filter--item--margin`                            | var(--digi--gutter--small) 0;                                                   |
| `--digi--form-filter--legend--font-family`                     | var(--digi--global--typography--font-family--default);                          |
| `--digi--form-filter--legend--font-size`                       | var(--digi--typography--preamble--font-size--desktop);                          |
| `--digi--form-filter--legend--font-weight`                     | var(--digi--typography--preamble--font-weight--desktop);                        |
| `--digi--form-filter--legend__border-bottom`                   | var(--digi--border-width--primary) solid var(--digi--color--border--neutral-2); |
| `--digi--form-filter--list--margin`                            | 0;                                                                              |
| `--digi--form-filter--list--padding`                           | var(--digi--gutter--small) 0 0 0;                                               |
| `--digi--form-filter--reset-button--margin-left`               | var(--digi--padding--small);                                                    |
| `--digi--form-filter--scroll--max-height`                      | 18.75rem;                                                                       |
| `--digi--form-filter--scroll--max-height--large`               | 23.4275rem;                                                                     |
| `--digi--form-filter--scroll--padding`                         | var(--digi--padding--small);                                                    |
| `--digi--form-filter--toggle-button--text--margin-right`       | var(--digi--margin--smaller);                                                   |
| `--digi--form-filter--toggle-button-indicator--color--active`  | var(--digi--color--border--success);                                            |
| `--digi--form-filter--toggle-button-indicator--color--default` | transparent;                                                                    |
| `--digi--form-filter--toggle-button-indicator--height`         | 0.625rem;                                                                       |
| `--digi--form-filter--toggle-button-indicator--margin-left`    | calc(var(--digi--margin--smaller) * -1);                                        |
| `--digi--form-filter--toggle-button-indicator--margin-right`   | var(--digi--margin--small);                                                     |
| `--digi--form-filter--toggle-button-indicator--width`          | 0.625rem;                                                                       |
| `--digi--form-filter--toggle-icon--height`                     | 0.875rem;                                                                       |
| `--digi--form-filter--toggle-icon--transition`                 | ease-in-out 0.2s all;                                                           |
| `--digi--form-filter--toggle-icon--width`                      | 0.875rem;                                                                       |


## Dependencies

### Used by

 - [digi-form-filter](../form-filter)

### Depends on

- [digi-button](../../_button/button)
- [digi-icon](../../_icon/icon)
- [digi-form-checkbox](../form-checkbox)

### Graph
```mermaid
graph TD;
  digi-form-filter-presentational --> digi-button
  digi-form-filter-presentational --> digi-icon
  digi-form-filter-presentational --> digi-form-checkbox
  digi-form-filter --> digi-form-filter-presentational
  style digi-form-filter-presentational fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
