import { newE2EPage } from '@stencil/core/testing';

describe('digi-select', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-form-select></digi-form-select>');

    const element = await page.find('digi-form-select');
    expect(element).toHaveClass('hydrated');
  });
});
