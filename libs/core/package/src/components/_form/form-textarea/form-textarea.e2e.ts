import { newE2EPage } from '@stencil/core/testing';

describe('digi-form-textarea', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-form-textarea></digi-form-textarea>');

    const element = await page.find('digi-form-textarea');
    expect(element).toHaveClass('hydrated');
  });
});
