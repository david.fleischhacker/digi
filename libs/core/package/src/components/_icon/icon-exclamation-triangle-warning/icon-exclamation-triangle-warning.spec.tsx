import { newSpecPage } from '@stencil/core/testing';
import { IconExclamationTriangleWarning } from './icon-exclamation-triangle-warning';

describe('icon-exclamation-triangle-warning', () => {
  it('renders', async () => {
    const {root} = await newSpecPage({
      components: [IconExclamationTriangleWarning],
      html: '<icon-exclamation-triangle-warning></icon-exclamation-triangle-warning>'
    });
    expect(root).toEqualHtml(`
      <icon-exclamation-triangle-warning>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </icon-exclamation-triangle-warning>
    `);
  });

  it('renders with values', async () => {
    const {root} = await newSpecPage({
      components: [IconExclamationTriangleWarning],
      html: `<icon-exclamation-triangle-warning first="Stencil" last="'Don't call me a framework' JS"></icon-exclamation-triangle-warning>`
    });
    expect(root).toEqualHtml(`
      <icon-exclamation-triangle-warning first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </icon-exclamation-triangle-warning>
    `);
  });
});
