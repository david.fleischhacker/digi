import { newSpecPage } from '@stencil/core/testing';
import { LoaderSkeleton } from './loader-skeleton';

describe('loader-skeleton', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [LoaderSkeleton],
			html: '<loader-skeleton></loader-skeleton>'
		});
		expect(root).toEqualHtml(`
      <loader-skeleton>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </loader-skeleton>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [LoaderSkeleton],
			html: `<loader-skeleton first="Stencil" last="'Don't call me a framework' JS"></loader-skeleton>`
		});
		expect(root).toEqualHtml(`
      <loader-skeleton first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </loader-skeleton>
    `);
	});
});
