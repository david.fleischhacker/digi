# digi-loader-skeleton

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                             | Type                                                                 | Default                        |
| ------------- | -------------- | --------------------------------------- | -------------------------------------------------------------------- | ------------------------------ |
| `afCount`     | `af-count`     | Sätter antalet rader av skeleton loader | `number`                                                             | `4`                            |
| `afVariation` | `af-variation` | Sätter varianten                        | `"circle" \| "header" \| "line" \| "rounded" \| "section" \| "text"` | `LoaderSkeletonVariation.LINE` |


## Slots

| Slot       | Description              |
| ---------- | ------------------------ |
| `"mySlot"` | Slot description, if any |


## CSS Custom Properties

| Name                                             | Description                                        |
| ------------------------------------------------ | -------------------------------------------------- |
| `--digi--loader-skeleton--background`            | var(--digi--color--background--neutral-3);         |
| `--digi--loader-skeleton--circle-border-radius`  | 50%;                                               |
| `--digi--loader-skeleton--circle-height`         | 2rem;                                              |
| `--digi--loader-skeleton--circle-width`          | 2rem;                                              |
| `--digi--loader-skeleton--header-height`         | 2rem;                                              |
| `--digi--loader-skeleton--header-margin`         | var(--digi--typography--h3--margin--small);        |
| `--digi--loader-skeleton--line-height`           | var(--digi--typography--body--font-size--desktop); |
| `--digi--loader-skeleton--line-margin-bottom`    | var(--digi--typography--paragraph--margin--small); |
| `--digi--loader-skeleton--margin-bottom`         | var(--digi--typography--paragraph--margin--small); |
| `--digi--loader-skeleton--rounded-border-radius` | var(--digi--border-radius--complementary-2);       |
| `--digi--loader-skeleton--text-height`           | var(--digi--typography--body--font-size--desktop); |
| `--digi--loader-skeleton--text-margin`           | var(--digi--margin--h4-h6-large);                  |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
