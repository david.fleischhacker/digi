import { newE2EPage } from '@stencil/core/testing';

describe('digi-navigation-vertical-menu', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-navigation-vertical-menu></digi-navigation-vertical-menu>');

    const element = await page.find('digi-navigation-vertical-menu');
    expect(element).toHaveClass('hydrated');
  });
});
