import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { NavigationVerticalMenuVariation } from './navigation-vertical-menu-variation.enum';
import { NavigationVerticalMenuActiveIndicatorSize } from './navigation-vertical-menu-active-indicator-size.enum';

export default {
	title: 'navigation/digi-navigation-vertical-menu',
	argTypes: {
		'af-variation': enumSelect(NavigationVerticalMenuVariation),
		'af-active-indicator-size': enumSelect(
			NavigationVerticalMenuActiveIndicatorSize
		)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-navigation-vertical-menu',
	'af-id': null,
	'af-variation': NavigationVerticalMenuVariation.PRIMARY,
	'af-active-indicator-size': NavigationVerticalMenuActiveIndicatorSize.PRIMARY,
	'af-aria-label': '',
	/* html */
	children: `
    <ul>
        <li>
        <digi-navigation-vertical-menu-item af-text="About" af-active-subnav="false" af-href="/about/about"></digi-navigation-vertical-menu-item>
        <ul>
            <li>
            <digi-navigation-vertical-menu-item af-text="About 1" af-href="/about/about-1"></digi-navigation-vertical-menu-item></digi-navigation-vertical-menu-item>
            </li>
            <li>
            <digi-navigation-vertical-menu-item af-text="About 2" af-href="/about/about-2"></digi-navigation-vertical-menu-item></digi-navigation-vertical-menu-item>
            </li>
        </ul>
        </li>
        <li>
        <digi-navigation-vertical-menu-item af-text="Pages" af-active-subnav="false" af-href="/grafisk-profil/pages"></digi-navigation-vertical-menu-item>
        <ul>
            <li>
            <digi-navigation-vertical-menu-item af-text="Pages 1" af-href="/pages/pages-1"></digi-navigation-vertical-menu-item>
            </li>
            <li>
            <digi-navigation-vertical-menu-item af-text="Pages 2" af-active-subnav="false" af-href="/pages/pages-2"></digi-navigation-vertical-menu-item>
            <ul>
                <li>
                <digi-navigation-vertical-menu-item af-text="Pages 2.1" af-href="/pages/pages-2-1"></digi-navigation-vertical-menu-item>
                </li>
                <li>
                <digi-navigation-vertical-menu-item af-text="Pages 2.2" af-href="/pages/pages-2-2"></digi-navigation-vertical-menu-item>
                </li>
                <li>
                <digi-navigation-vertical-menu-item af-text="Pages 2.3" af-href="/grafisk-profil/pages-2-3"></digi-navigation-vertical-menu-item>
                </li>
            </ul>
            </li>
            <li>
            <digi-navigation-vertical-menu-item af-text="Pages 3" af-active-subnav="false" af-href="/grafisk-profil/pages-3"></digi-navigation-vertical-menu-item>
            <ul>
                <li>
                <digi-navigation-vertical-menu-item af-text="Pages 3.1" af-href="/pages/pages-3-1"></digi-navigation-vertical-menu-item>
                </li>
                <li>
                <digi-navigation-vertical-menu-item af-text="Pages 3.2" af-href="/pages/pages-3-2"></digi-navigation-vertical-menu-item>
                </li>
                <li>
                <digi-navigation-vertical-menu-item af-text="Pages 3.3" af-href="/grafisk-profil/pages-3-3"></digi-navigation-vertical-menu-item>
                </li>
            </ul>
            </li>
        </ul>
        </li>
        <li>
        <digi-navigation-vertical-menu-item af-text="Contact" af-href="/contact"></digi-navigation-vertical-menu-item>
        </li>
    </ul>`
};
