import { newE2EPage } from '@stencil/core/testing';

describe('digi-progress-indicator', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-progress-indicator></digi-progress-indicator>');

    const element = await page.find('digi-progress-indicator');
    expect(element).toHaveClass('hydrated');
  });
});
