# digi-progress-steps



<!-- Auto Generated Below -->


## Properties

| Property                 | Attribute          | Description                                                           | Type                                                                                                                                                                                     | Default                                   |
| ------------------------ | ------------------ | --------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------- |
| `afHeading` _(required)_ | `af-heading`       | Rubrikens text                                                        | `string`                                                                                                                                                                                 | `undefined`                               |
| `afHeadingLevel`         | `af-heading-level` | Sätter rubriknivå. 'h2' är förvalt.                                   | `ProgressStepHeadingLevel.H1 \| ProgressStepHeadingLevel.H2 \| ProgressStepHeadingLevel.H3 \| ProgressStepHeadingLevel.H4 \| ProgressStepHeadingLevel.H5 \| ProgressStepHeadingLevel.H6` | `ProgressStepHeadingLevel.H2`             |
| `afId`                   | `af-id`            | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id. | `string`                                                                                                                                                                                 | `randomIdGenerator('digi-progress-step')` |
| `afIsLast`               | `af-is-last`       | Kollar om det är sista steget                                         | `boolean`                                                                                                                                                                                | `false`                                   |
| `afStepStatus`           | `af-step-status`   | Sätter attributet 'afStepStatus'. Förvalt är 'upcoming'.              | `ProgressStepStatus.CURRENT \| ProgressStepStatus.DONE \| ProgressStepStatus.UPCOMING`                                                                                                   | `ProgressStepStatus.UPCOMING`             |
| `afVariation`            | `af-variation`     | Sätter variant. Kan vara 'Primary' eller 'Secondary'                  | `ProgressStepVariation.PRIMARY \| ProgressStepVariation.SECONDARY`                                                                                                                       | `ProgressStepVariation.PRIMARY`           |


## CSS Custom Properties

| Name                                                 | Description                                               |
| ---------------------------------------------------- | --------------------------------------------------------- |
| `--digi--progress-step--heading--font-size`          | var(--digi--typography--heading-3--font-size--desktop);   |
| `--digi--progress-step--heading--font-weight`        | var(--digi--typography--heading-3--font-weight--desktop); |
| `--digi--progress-step--indicator--color--primary`   | var(--digi--color--icons--success);                       |
| `--digi--progress-step--indicator--color--secondary` | var(--digi--color--icons--secondary);                     |


## Dependencies

### Depends on

- [digi-typography](../../_typography/typography)

### Graph
```mermaid
graph TD;
  digi-progress-step --> digi-typography
  style digi-progress-step fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
