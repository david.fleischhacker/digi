import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { ProgressStepsVariation } from './progress-steps-variation.enum';
import { ProgressStepsHeadingLevel } from './progress-steps-heading-level.enum';

export default {
	title: 'progress/digi-progress-steps',
	argTypes: {
		'af-heading-level': enumSelect(ProgressStepsHeadingLevel),
		'af-variation': enumSelect(ProgressStepsVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-progress-steps',
	'af-heading-level': ProgressStepsHeadingLevel.H2,
	'af-variation': ProgressStepsVariation.PRIMARY,
	'af-current-step': 1,
	/* html */
	children: `
    <digi-progress-step af-heading="Heading 1">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</digi-progress-step>
    <digi-progress-step af-heading="Heading 2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</digi-progress-step>
    <digi-progress-step af-heading="Heading 3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</digi-progress-step>`
};
