import { enumSelect, Template } from '../../../../../../shared/utils/src';

import { TagSize } from './tag-size.enum';

export default {
	title: 'tag/digi-tag',
	parameters: {
		actions: {
			handles: ['afOnClick']
		}
	},
	argTypes: {
		'af-size': enumSelect(TagSize)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-tag',
	'af-text': 'Text is required',
	'af-no-icon': false,
	'af-size': TagSize.SMALL,
	children: ''
};
