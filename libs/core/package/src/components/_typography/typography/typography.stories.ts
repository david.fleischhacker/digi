import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { TypographyVariation } from './typography-variation.enum';

export default {
	title: 'typography/digi-typography',
	argTypes: {
		'af-variation': enumSelect(TypographyVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-typography',
	'af-variation': TypographyVariation.SMALL,
	/* html */
	children: `
    <h1>CSS Basic Elements</h1>
    <p>
    The purpose of <span>ett span</span> this HTML is to help determine what default settings are with CSS and to make
    sure that all possible HTML Elements are included in this HTML so as to not miss any possible Elements when designing
    a site.
    </p>
    <h1 id="headings">Headings</h1>
    <h1>Heading 1</h1>
    <h2>Heading 2</h2>
    <h3>Heading 3</h3>
    <h4>Heading 4</h4>
    <h5>Heading 5</h5>
    <h6>Heading 6</h6>
    <h1 id="paragraph">Paragraph</h1>
    <p>
    Lorem ipsum dolor sit amet, <a href="#" title="test link">test link</a> adipiscing elit. Nullam dignissim convallis
    est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at,
    tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu
    volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante.
    Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.
    </p>
    <p>
    Lorem ipsum dolor sit amet, <em>emphasis</em> consectetuer adipiscing elit. Nullam dignissim convallis est. Quisque
    aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt
    nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim
    diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse
    quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.
    </p>
    <ul>
        <li>Unordered list item one</li>
        <li>Unordered list item two</li>
        <li>Unordered list item three</li>
    </ul>
    <ol>
        <li>Ordered list item one</li>
        <li>Ordered list item two</li>
        <li>Ordered list item three</li>
    </ol>`
};
