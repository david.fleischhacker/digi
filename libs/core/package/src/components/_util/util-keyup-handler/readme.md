# digi-util-keyup-handler

Detect keyup events on the most common keys, and also provides a catch-all `afOnKeyUp`-event.

<!-- Auto Generated Below -->


## Events

| Event          | Description                 | Type                         |
| -------------- | --------------------------- | ---------------------------- |
| `afOnDown`     | Vid keyup på pil ner        | `CustomEvent<KeyboardEvent>` |
| `afOnEnd`      | Vid keyup på end            | `CustomEvent<KeyboardEvent>` |
| `afOnEnter`    | Vid keyup på enter          | `CustomEvent<KeyboardEvent>` |
| `afOnEsc`      | Vid keyup på escape         | `CustomEvent<KeyboardEvent>` |
| `afOnHome`     | Vid keyup på home           | `CustomEvent<KeyboardEvent>` |
| `afOnKeyUp`    | Vid keyup på alla tangenter | `CustomEvent<KeyboardEvent>` |
| `afOnLeft`     | Vid keyup på pil vänster    | `CustomEvent<KeyboardEvent>` |
| `afOnRight`    | Vid keyup på pil höger      | `CustomEvent<KeyboardEvent>` |
| `afOnShiftTab` | Vid keyup på skift och tabb | `CustomEvent<KeyboardEvent>` |
| `afOnSpace`    | Vid keyup på space          | `CustomEvent<KeyboardEvent>` |
| `afOnTab`      | Vid keyup på tab            | `CustomEvent<KeyboardEvent>` |
| `afOnUp`       | Vid keyup på pil upp        | `CustomEvent<KeyboardEvent>` |


## Slots

| Slot        | Description     |
| ----------- | --------------- |
| `"default"` | Can be anything |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
