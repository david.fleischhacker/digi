import { newE2EPage } from '@stencil/core/testing';

describe('digi-util-mutation-observer', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-util-mutation-observer></digi-util-mutation-observer>');

    const element = await page.find('digi-util-mutation-observer');
    expect(element).toHaveClass('hydrated');
  });
});
