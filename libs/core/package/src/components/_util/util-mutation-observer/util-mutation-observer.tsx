import {
	Component,
	Element,
	Event,
	EventEmitter,
	h,
	Host,
	Prop,
	Watch
} from '@stencil/core';
import { randomIdGenerator } from '../../../../../../shared/utils/src/index';

/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Mutation Observer
 */
@Component({
	tag: 'digi-util-mutation-observer',
	scoped: true
})
export class UtilMutationObserver {
	@Element() $el: HTMLElement;

	private _observer: MutationObserver = null;
	private _isObserving = false;
	private _afOptions: object;

	private _wrapper;

	/**
	 * Sätter attributet 'id' på det omslutande elementet. Ges inget värde så genereras ett slumpmässigt.
	 * @en Label id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-util-mutation-observer');

	/**
	 * Skicka options till komponentens interna Mutation Observer. T.ex. för att kontrollera förändring i en slot.
	 * @en Accepts an Mutation Observer options object. Controls changes .
	 */
	@Prop() afOptions: MutationObserverInit | string = {
		attributes: false,
		childList: true,
		subtree: false,
		characterData: false,
	};

	@Prop() afHidden: boolean = false

	@Watch('afOptions')
	afOptionsWatcher(newValue: object | string) {
		this._afOptions =
			typeof newValue === 'string' ? JSON.parse(newValue) : newValue;
	}

	/**
	 * När DOM-element läggs till eller tas bort inuti Mutation Observer:n
	 * @en When DOM-elements is added or removed inside the Mutation Observer
	 */
	@Event() afOnChange: EventEmitter;

	componentWillLoad() {
		this.afOptionsWatcher(this.afOptions);
	}
	
	componentDidLoad() {
		this.initObserver();
	}

	disconnectedCallback() {
		if (this._isObserving) {
			this.removeObserver();
		}
	}

	initObserver() {
		this._observer = new MutationObserver((mutationsList) => {
			for (const mutation of mutationsList) {
				if (mutation.type === 'childList' || mutation.type === 'attributes' || mutation.type === 'characterData') {
					this.changeHandler(mutation);
				}
			}
		});
		this._observer.observe(this._wrapper, this._afOptions);
		this._isObserving = true;
	}

	removeObserver() {
		this._observer.disconnect();
		this._isObserving = false;
	}

	changeHandler(e) {
		this.afOnChange.emit(e);
	}

	render() {
		return (
			<Host>
				<div 
					id={this.afId} 
					ref={ref => this._wrapper = ref}
					style={{ display: this.afHidden ? 'none' : 'initial'}}
				>
					<slot></slot>
				</div>
			</Host>
		)
	}
}
