export function randomIdGenerator(prefix = 'digi') {
  return `${prefix}-${Math.random()
    .toString(36)
    .substring(2, 7)}`;
}

export function setupId (prefix: string, userSeed?: string): 
(scope: string) => string  {
  const seed = userSeed || (
    Math.random()
    .toString(36)
    .substring(2, 7)
  )
  return (scope) => {
    return `${prefix}-${seed}-${scope}`
  }
}