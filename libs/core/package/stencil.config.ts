import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';

console.log('Core - stencil.config');

// Path is relative from .config/

export const StencilBaseConfig: Config = {
	namespace: 'digi-core',
	taskQueue: 'async',
  autoprefixCss: true,
  sourceMap: false,
	srcDir: '../src',
	tsconfig: '../tsconfig.json',
	plugins: [
		sass()
	],
  enableCache: false,
  cacheDir: '../../../.digi/core-stencil',
	// Output targets is in .config-folder
	outputTargets: [],
	globalStyle: '../src/global/styles/index.scss'
};
