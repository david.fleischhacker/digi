/* eslint-disable */
export default {
	displayName: 'core-react',
	preset: '../../jest.preset.js',
	transform: {
		'^.+\\.[tj]sx?$': ['babel-jest', { presets: ['@nx/react/babel'] }]
	},
	moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
	coverageDirectory: '../../coverage/libs/core-react'
};
