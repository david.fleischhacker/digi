import { render } from '@testing-library/react';

import CoreReact from './core-react';

describe('CoreReact', () => {
	it('should render successfully', () => {
		const { baseElement } = render(<CoreReact />);
		expect(baseElement).toBeTruthy();
	});
});
