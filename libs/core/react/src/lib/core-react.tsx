import styles from './core-react.module.scss';

/* eslint-disable-next-line */
export interface CoreReactProps {}

export function CoreReact(props: CoreReactProps) {
	return (
		<div className={styles['container']}>
			<h1>Welcome to CoreReact!</h1>
		</div>
	);
}

export default CoreReact;
