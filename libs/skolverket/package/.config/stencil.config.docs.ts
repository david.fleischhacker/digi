import { Config } from '@stencil/core';
import { StencilBaseConfig } from '../stencil.config';

console.log('Skolverket - stencil.config.docs');

export const config: Config = {
	...StencilBaseConfig,
	srcDir: '../src',
	outputTargets: [
		{
			type: 'docs-json',
			file: '../../../../dist/libs/skolverket/docs.json'
		}
	]
};
