import { Component, Element, State, h } from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import { TypographyVariation } from '../../../enums-core';

/**
 * @slot default - kan innehålla vad som helst
 * @slot image - Bild
 * @slot link-heading - Ska innehålla en rubrik med länk inuti
 *
 * @swedishName Länkat kort
 */
@Component({
	tag: 'digi-card-link',
	styleUrls: ['card-link.scss'],
	scoped: true
})
export class CardLink {
	@Element() hostElement: HTMLStencilElement;
	@State() hasImage: boolean;

	componentWillLoad() {
		this.setHasImage();
	}

	componentWillUpdate() {
		this.setHasImage();
	}

	setHasImage() {
		this.hasImage = !!this.hostElement.querySelector('[slot="image"]');
	}

	get cssModifiers() {
		return {
			[`digi-card-link--image-${this.hasImage}`]: true
		};
	}

	render() {
		return (
			<div
				class={{
					'digi-card-link': true,
					...this.cssModifiers
				}}
			>
				{this.hasImage && (
					<div class="digi-card-link__image">
						<slot name="image"></slot>
					</div>
				)}
				<digi-typography
					afVariation={TypographyVariation.LARGE}
					class="digi-card-link__content"
				>
					<div class="digi-card-link__heading">
						<slot name="link-heading"></slot>
					</div>
					<div class="digi-card-link__text">
						<slot></slot>
					</div>
				</digi-typography>
			</div>
		);
	}
}
