import { parseArgs } from '../../../../../../shared/utils/src';

export default {
	title: 'dialog/digi-dialog',
	parameters: {
		actions: {
			handles: ['afPrimaryButtonClick', 'afSecondaryButtonClick', 'afOnBlur']
		}
	}
};

const Template = ({ children, ...args }) => /*html*/ `
	<script>
		document.addEventListener('afOnClick', (e) => {
			if(e.target.classList.contains('open-btn')) {
				e.target.nextElementSibling.setAttribute('af-open', true);
			}

			if(e.target.classList.contains('close-btn')) {
				e.target.closest('digi-dialog').setAttribute('af-open', false);
			}
		})
  </script>
		<digi-button class="open-btn" af-variation="primary">Open dialog</digi-button>
		<digi-dialog ${parseArgs(args)}>
			${children}
		</digi-dialog>
`;

export const Standard = Template.bind({});
Standard.args = {
	id: 'dialog',
	'af-open': false,
	'af-hide-close-button': false,
	/* html */
	children: `<h2 slot="heading">En rubrik</h2>
	<p>
	Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Sed posuere consectetur est at lobortis. Donec ullamcorper nulla non metus auctor fringilla. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
	Etiam porta sem malesuada magna mollis euismod. Curabitur blandit tempus porttitor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget metus.
	</p>
	<p>
	Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Sed posuere consectetur est at lobortis. Donec ullamcorper nulla non metus auctor fringilla. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
	Etiam porta sem malesuada magna mollis euismod. Curabitur blandit tempus porttitor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget metus.
	</p>
	<digi-button class="close-btn" slot="actions" af-variation="secondary">Avbryt</digi-button>
	<digi-button class="close-btn" slot="actions">Stäng</digi-button>`
};

export const WithFocusableHeading = Template.bind({});
WithFocusableHeading.args = {
	'af-open': false,
	'af-hide-close-button': false,
	/* html */
	children: `<h2 slot="heading" tabindex="-1">En rubrik</h2>
	<p>
	Med tabindex -1 på rubriken så får den fokus när dialogen öppnas.
	</p>
	<digi-button class="close-btn" slot="actions" af-variation="secondary">Avbryt</digi-button>
	<digi-button class="close-btn" slot="actions">Stäng</digi-button>`
};

export const WithExplicitFirstFocusedItem = Template.bind({});
WithExplicitFirstFocusedItem.args = {
	'af-open': false,
	'af-hide-close-button': false,
	/* html */
	children: `<h2 slot="heading" tabindex="-1">En rubrik</h2>
	<p>
	Med attributet autofocus på sista knappen så får den fokus när dialogen öppnas.
	</p>
	<digi-button class="close-btn" slot="actions" af-variation="secondary">Avbryt</digi-button>
	<digi-button class="close-btn" slot="actions" af-autofocus>Stäng</digi-button>`
};

// export const WithForm = Template.bind({});
// WithForm.args = {
// 	'af-open': false,
// 	'af-hide-close-button': false,
// 	/* html */
// 	children: `<h2 slot="heading" tabindex="-1">En rubrik</h2>
// 	<p>Med method="dialog" på formuläret så stängs dialogen när datan skickas in. I det här fallet använder vi af-form på knapparna för att associera dem till formuläret (då de ligger utanför i domen)</p>
// 	<form method="dialog" id="dialog-example-form">
// 		<digi-form-input af-label="Ett fält"></digi-form-input>
// 	</form>
// 	<digi-button slot="actions" af-variation="secondary" af-type="reset" af-form="dialog-example-form">Avbryt</digi-button>
// 	<digi-button slot="actions" af-type="submit" af-form="dialog-example-form">Skicka in</digi-button>`
// };
