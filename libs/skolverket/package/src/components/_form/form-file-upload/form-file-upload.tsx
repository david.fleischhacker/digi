import {
	Component,
	h,
	Prop,
	State,
	Element,
	Method,
	EventEmitter,
	Event
} from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';

import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import {
	ButtonType,
	ButtonVariation,
	TypographyVariation
} from '../../../enums-core';
import { FormFileUploadHeadingLevel } from './form-file-upload-heading-level.enum';
import { _t } from '@digi/skolverket/text';

/**
 *	@enums FormInputFileType - form-input-file-type.enum.ts
 *  @enums FormInputFileVariation - form-input-file-variation.enum.ts
 * 	@swedishName Filuppladdare
 */
@Component({
	tag: 'digi-form-file-upload',
	styleUrl: 'form-file-upload.scss',
	scoped: true
})
export class FormFileUpload {
	@Element() hostElement: HTMLStencilElement;

	@State() _input: HTMLInputElement;

	@State() files: File[] = [];

	@State() error: boolean = false;

	@State() errorMessage: string;

	@State() fileHover: boolean = false;

	/**
	 * Texten till labelelementet
	 * @en The label text
	 */
	@Prop() afLabel = _t.form.file_upload;

	/**
	 * Valfri beskrivande text
	 * @en A description text
	 */
	@Prop() afLabelDescription: string;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Input id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-form-file-upload');

	/** Sätter attributet 'accept'. Använd för att limitera accepterade filtyper
	 * @en Set input field's accept attribute. Use this to limit accepted file types.
	 */
	@Prop() afFileTypes!: string;

	/**
	 * Sätter en maximal filstorlek i MB, 10MB är standard.
	 * @en Set file max size in MB
	 */
	@Prop() afFileMaxSize: number = 10;

	/**
	 * Sätter maximalt antal filer man kan ladda upp
	 * @en Set max number of files you can upload
	 */
	@Prop() afMaxFiles: number;

	/**
	 * Sätter attributet 'name'.
	 * @en Input name attribute
	 */
	@Prop() afName: string;

	/**
	 * Sätter ladda upp knappens text.
	 * @en Set upload buttons text
	 */
	@Prop() afUploadBtnText = _t.form.file_choose;

	/**
	 * Sätter attributet 'required'.
	 * @en Input required attribute
	 */
	@Prop() afRequired: boolean;

	/**
	 * Sätter text för afRequired.
	 * @en Set text for afRequired.
	 */
	@Prop() afRequiredText: string;

	/**
	 * Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria.
	 * @en Set this to true if the form contains more required fields than optional fields.
	 */
	@Prop() afAnnounceIfOptional = false;

	/**
	 * Sätt rubrik för uppladdade filer.
	 * @en Set heading for uploaded files.
	 */
	@Prop() afHeadingFiles = _t.form.file_uploaded_files;

	/**
	 * Sätt text som visas på ytan där man kan dra och släppa filer.
	 * @en Set text for the file drop zone.
	 */
	@Prop() afFileDropZoneText = _t.form.file_dropzone;

	/**
	 * Sätt text som visas under tiden filer håller på att laddas upp.
	 * @en Set text that is shown while files are being uploaded.
	 */
	@Prop() afUploadInProgressText = 'Laddar upp...';

	/**
	 * Sätter text för afAnnounceIfOptional.
	 * @en Set text for afAnnounceIfOptional.
	 */
	@Prop() afAnnounceIfOptionalText: string;

	/**
	 * Sätt rubrikens vikt. 'h2' är förvalt.
	 * @en Set heading level. Default is 'h2'
	 */
	@Prop() afHeadingLevel: FormFileUploadHeadingLevel =
		FormFileUploadHeadingLevel.H2;

	/**
	 * Sätt text för att försöka igen. Om inget värde anges används: "Försök igen"
	 * @en Set text for trying agian. Default is "Försök igen"
	 */
	@Prop() afTryAgainText = 'Försök igen';

	/**
	 * Sätt text för att ta bort fil. Om inget värde anges används: "Ta bort fil"
	 * @en Set text for removing a file. Default is "Ta bort fil"
	 */
	@Prop() afRemoveFileText = 'Ta bort fil';

	/**
	 * Sätt text som används i en aria-label där man kan ta bort en fil. Om inget värde anges används: "Ta bort"
	 * @en Set text used in an aria-label attribute where the user can remove a file. Default is "Ta bort"
	 */
	@Prop() afRemoveAriaLabelText = 'Ta bort';

	/**
	 * Sätt text som visas om uppladdningen avbryts. Om inget värde anges används: "Uppladdningen avbruten".
	 * @en Set text that is shown if the upload process is canceled. Default is "Uppladdningen avbruten".
	 */
	@Prop() afFileErrorCanceledText = _t.form.file_error_canceled;

	/**
	 * Sätt text som visas om man redan laddat upp max antal filer. Om inget värde anges används: "Max antal filer uppladdade".
	 * @en Set text that is shown if the maximum number of files have already been uploaded. Default is "Max antal filer uppladdade".
	 */
	@Prop() afFileErrorTooManyText = _t.form.file_error_too_many;

	/**
	 * Sätt text som visas där användaren kan avbryta uppladdningen av en fil. Om inget värde anges används: "Avbryt uppladdning".
	 * @en Set text that is displayed where the user can cancel uploading a file. Default is "Avbryt uppladdning".
	 */
	@Prop() afFileCancelUploadText = _t.form.file_cancel_upload;

	/**
	 * Sätt text för felmeddelande som visas när filtypen är ogiltig. Om inget värde anges används: "Filtypen tillåts inte att ladda upp".
	 * @en Set text for error message being displayed when the file type isn't allowed. Default is "Filtypen tillåts inte att ladda upp".
	 */
	@Prop() afFileErrorFiletypeNotAllowedText =
		_t.form.file_error_filetype_not_allowed;

	/**
	 * Sätt text för felmeddelande som visas när de valda filerna redan är uppladdade. Om inget värde anges används: "Alla valda filer är redan uppladdade".
	 * @en Set text for error message being displayed when the selected files have already been uploaded. Default is "Alla valda filer är redan uppladdade".
	 */
	@Prop() afFileErrorFileAlreadyUploadedText =
		_t.form.file_error_already_uploaded;

	/**
	 * Sätt text för när uppladdningen blivit avbruten. Om inget värde anges används: "Avbruten".
	 * @en Set text for when the upload process has been canceled. Default is "Avbruten".
	 */
	@Prop() afCanceledText = _t.canceled;

	/**
	 * Sätter en textmall som används vid fel när användaren uppmanas att försöka igen. T.ex. "Misslyckad uppladdning, försök ladda upp filen igen {{fileName}}". Där {{fileName}} ersätts av filens namn.
	 * @en Sets a template text that is used when an error occurs and the user is asked to try again i.e.: `Upload failed, please try again {{fileName}}`. Where {{fileName}} will be replaced by the name of the file.
	 */
	@Prop() afErrorTryAgainTemplateText: string;

	/**
	 * Sätter en textmall som används på en knapp för att ta bort filer som visas vid fel under uppladdning. T.ex. "Misslyckad uppladdning, ta bort {{fileName}} från fillistan". Där {{fileName}} ersätts av filens namn.
	 * @en Sets a template text on a button that is used to remove files when an error occurs while uploading a file i.e.: `Upload failed, remove {{fileName}} from the list of files.`. Where {{fileName}} will be replaced by the name of the file.
	 */
	@Prop() afFileErrorRemoveTemplateText: string;

	/**
	 * Sätter en textmall som visar felinformation vid uppladdning. T.ex. "{{fileName}} gick inte att ladda upp. {{error}}". Där {{fileName}} ersätts av filens namn och {{error}} med det inträffade felet.
	 * @en Sets a template text displaying error information i.e.: `{{fileName}} couldn't be uploaded. {{error}}`. Where {{fileName}} will be replaced by the name of the file and {{error}} by the current error information.
	 */
	@Prop() afFileErrorTemplateText: string;

	/**
	 * Sätter en textmall för att visa där man kan avbryta uppladdningen av en fil. T.ex. "Avbryt uppladdning av filen {{fileName}}". Där {{fileName}} ersätts av filens namn.
	 * @en Sets a template text that is shown where the user can cancel uploading a file i.e.: `Cancel uploading {{fileName}}`. Where {{fileName}} will be replaced by the name of the file.
	 */
	@Prop() afFileCancelUploadLongTemplateText: string;

	/**
	 * Sätter en textmall för ett felmeddelande som visas när den uppladdade filens storlek är för stor. T.ex. "Filens storlek är för stor {{fileName}}". Där {{fileName}} ersätts av filens namn.
	 * @en Sets a template text that will be used in an error message being displayed when the size of the uploaded file is too large i.e.: `The size of the file is too large {{fileName}}`. Where {{fileName}} will be replaced by the name of the file.
	 */
	@Prop() afFileErrorTooLargeTemplateText: string;

	/**
	 * Sätter en textmall för ett felmeddelande som visas när det redan finns en fil med samma namn som den fil man försöker importera. T.ex. "Det finns redan en fil med filnamnet ({{fileName}})". Där {{fileName}} ersätts av filens namn.
	 * @en Sets a template text that will be used in an error message being displayed when there already exists a file with the same name as the one that is being imported i.e.: `There already exists a file with the name {{fileName}}`. Where {{fileName}} will be replaced by the name of the file.
	 */
	@Prop() afFileErrorDuplicateTemplateText: string;

	/**
	 * Sänder ut fil vid uppladdning
	 * @en Emits file on upload
	 */
	@Event() afOnUploadFile: EventEmitter;

	/**
	 * Sänder ut vilken fil som tagits bort
	 * @en Emits which file that was deleted.
	 */
	@Event() afOnRemoveFile: EventEmitter;

	/**
	 * Sänder ut vilken fil som har avbrutis uppladdning
	 * @en Emits which file that was canceled
	 */
	@Event() afOnCancelFile: EventEmitter;

	/**
	 * Sänder ut vilken fil som försöker laddas upp igen
	 * @en Emits which file is trying to retry its upload
	 */
	@Event() afOnRetryFile: EventEmitter;

	/**
	 * Få ut alla uppladdade filer
	 * @en Get all uploaded files
	 */
	@Method()
	async afMGetAllFiles() {
		return this.files;
	}

	/**
	 * Importera en array med filer till komponenten utan att trigga uppladdningsevent. Ett fil objekt måste innehålla id, status och filen själv.
	 * @en Import an array with files without triggering upload event. A file object needs to contain an id, status and the file itself.
	 */
	@Method()
	async afMImportFiles(files: File[]) {
		for (const importedFile of files) {
			let duplicate = false;
			for (const item of this.files) {
				if (item.name == importedFile.name) {
					this.error = true;
					this.errorMessage = this.afFileErrorDuplicateTemplateText?.replace
						? this.afFileErrorDuplicateTemplateText.replace(
								'{{fileName}}',
								importedFile.name
						  )
						: _t.form.file_error_duplicate(importedFile.name);
					duplicate = true;
				}
			}

			if (!duplicate) {
				this.files = [...this.files, importedFile];
			}
		}
	}

	@Method()
	async afMGetFormControlElement() {
		return this._input;
	}

	get fileMaxSize() {
		// 1MB is 1048576 in Bytes
		return this.afFileMaxSize * 1048576;
	}

	emitFile(incomingFile: File) {
		const index = this.getIndexOfFile(incomingFile['id']);
		this.files[index]['status'] = 'OK';
		this.afOnUploadFile.emit(this.files[index]);
	}

	onRetryFileHandler(e: Event, id: string) {
		e.preventDefault();

		const fileFromList = this.removeFileFromList(id);
		fileFromList['status'] = 'pending';
		delete fileFromList['error'];

		this.files = [...this.files, fileFromList];

		this.afOnRetryFile.emit(fileFromList);
	}

	onCancelFileHandler(e: Event, id: string) {
		e.preventDefault();

		const fileFromList = this.removeFileFromList(id);
		fileFromList['status'] = 'error';
		fileFromList['error'] = this.afFileErrorCanceledText;

		this.files = [...this.files, fileFromList];

		this.afOnCancelFile.emit(fileFromList);
	}

	removeFileFromList(id: string) {
		const index = this.getIndexOfFile(id);

		if (index === -1) {
			return;
		}

		const removedFile = this.files[index];
		this.files = [...this.files.slice(0, index), ...this.files.slice(index + 1)];
		return removedFile;
	}

	getIndexOfFile(id: string) {
		const index = this.files.findIndex((file) => file['id'] == id);
		return index;
	}

	onButtonUploadFileHandler(e) {
		e.preventDefault();

		if (!e.target.files) return;
		[...e.target.files].forEach((file) => this.addUploadedFiles(file));
	}

	onRemoveFileHandler(e: Event, id: any) {
		e.preventDefault();

		const fileToRemove = this.removeFileFromList(id);

		this.afOnRemoveFile.emit(fileToRemove);

		this._input.value = '';

		if (this.files.length < 1) this.error = false;
	}

	async addUploadedFiles(file: File) {
		this.error = false;

		if (this.files.length >= this.afMaxFiles) {
			this.error = true;
			this.errorMessage = this.afFileErrorTooManyText;
			return;
		} else if (file.size > this.fileMaxSize) {
			this.error = true;
			this.errorMessage = this.afFileErrorTooLargeTemplateText?.replace
				? this.afFileErrorTooLargeTemplateText.replace('{{fileName}}', file.name)
				: _t.form.file_error_too_large;
			return;
		} else {
			for (const item of this.files) {
				if (item.name == file.name) {
					this.error = true;
					this.errorMessage = this.afFileErrorFileAlreadyUploadedText;
					return;
				}
			}
		}

		file['id'] = randomIdGenerator('file');
		file['status'] = 'pending';
		file['base64'] = await this.toBase64(file);

		this.files = [...this.files, file];
		this.emitFile(file);
	}

	toBase64 = (file: Blob) =>
		new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = () => {
				let encoded = reader.result.toString().replace(/^data:(.*,)?/, '');
				if (encoded.length % 4 > 0) {
					encoded += '='.repeat(4 - (encoded.length % 4));
				}
				resolve(encoded);
			};
			reader.onerror = (error) => reject(error);
		});

	handleDrop(e: DragEvent) {
		e.stopPropagation();
		e.preventDefault();
		this.fileHover = false;

		if (e.dataTransfer.files) {
			const file = e.dataTransfer.files[0];

			if (file.type.includes(this.afFileTypes) || this.afFileTypes == '*') {
				this.addUploadedFiles(file);
			} else {
				this.error = true;
				this.errorMessage = this.afFileErrorFiletypeNotAllowedText;
			}
		}
	}

	handleAllowDrop(e: DragEvent) {
		e.stopPropagation();
		e.preventDefault();
	}

	handleOnDragEnter(e: DragEvent) {
		this.handleAllowDrop(e);
		this.fileHover = !this.fileHover;
	}

	handleOnDragLeave(e: DragEvent) {
		this.handleAllowDrop(e);
		this.fileHover = !this.fileHover;
	}

	handleInputClick(e: Event) {
		e.cancelBubble = true;
		this._input.click();
	}

	render() {
		return (
			<div
				class={{
					'digi-form-file-upload': true
				}}
			>
				<digi-form-label
					afFor={this.afId}
					afLabel={this.afLabel}
					afDescription={this.afLabelDescription}
					afRequired={this.afRequired}
					afAnnounceIfOptional={this.afAnnounceIfOptional}
					afRequiredText={this.afRequiredText}
					afAnnounceIfOptionalText={this.afAnnounceIfOptionalText}
				></digi-form-label>
				<div
					class={{
						'digi-form-file-upload__upload-area': true,
						'digi-form-file-upload__upload-area--hover': this.fileHover
					}}
					onDrop={(e) => this.handleDrop(e)}
					onDragOver={(e) => this.handleAllowDrop(e)}
					onDragEnter={(e) => this.handleOnDragEnter(e)}
					onDragLeave={(e) => this.handleOnDragLeave(e)}
					onClick={(e) => this.handleInputClick(e)}
				>
					{this.fileHover && (
						<div class="digi-form-file-upload__upload-area-overlay"></div>
					)}
					<digi-icon
						afName="upload"
						class="digi-form-file-upload__icon"
						aria-hidden="true"
					/>
					<div class="digi-form-file-upload__text-area">
						<digi-button
							class="digi-form-file-upload__upload-button"
							afVariation={ButtonVariation.FUNCTION}
							afType={ButtonType.BUTTON}
							onClick={(e) => this.handleInputClick(e)}
						>
							{this.afUploadBtnText}
						</digi-button>
						<input
							ref={(el) => {
								this._input = el as HTMLInputElement;
							}}
							onChange={(e) => this.onButtonUploadFileHandler(e)}
							class="digi-form-file-upload__input"
							multiple
							type="file"
							id={this.afId}
							accept={this.afFileTypes}
							name={this.afName ? this.afName : null}
							required={this.afRequired ? this.afRequired : null}
							onDrop={(e) => this.handleDrop(e)}
							onDragOver={(e) => this.handleAllowDrop(e)}
							aria-errormessage="digi-form-file-upload__error"
							aria-describedBy="digi-form-file-upload__error"
						/>
						<digi-typography
							class="digi-form-file-upload__upload-text"
							afVariation={TypographyVariation.LARGE}
						>
							<p>{this.afFileDropZoneText}</p>
						</digi-typography>
					</div>
				</div>
				{this.error && (
					<digi-form-validation-message
						id="digi-form-file-upload__error"
						class="digi-form-file-upload__error"
						role="alert"
						aria-label={this.errorMessage}
						af-variation="error"
					>
						{this.errorMessage}
					</digi-form-validation-message>
				)}
				{this.files.length > 0 && (
					<div class="digi-form-file-upload__files">
						<this.afHeadingLevel class="digi-form-file-upload__files-heading">
							{this.afHeadingFiles}
						</this.afHeadingLevel>
						<ul aria-live="assertive">
							{this.files.map((file) => {
								return (
									<li class="digi-form-file-upload__file-container">
										{file['status'] == 'pending' && (
											<div class="digi-form-file-upload__file">
												<div class="digi-form-file-upload__file-header">
													<digi-icon-spinner class="digi-form-file-upload__spinner hidden-mobile"></digi-icon-spinner>
													<span>
														{this.afUploadInProgressText}{' '}
														<span class={'hidden-mobile'}>|</span>
													</span>
													<p class="digi-form-file-upload__file-name hidden-mobile">
														{file.name}
													</p>
													<button
														type="button"
														onClick={(e) => this.onCancelFileHandler(e, file['id'])}
														class="digi-form-file-upload__button hidden-mobile"
														aria-label={
															this.afFileCancelUploadLongTemplateText?.replace
																? this.afFileCancelUploadLongTemplateText.replace(
																		'fileName',
																		file.name
																  )
																: _t.form.file_cancel_upload_long(file.name)
														}
													>
														{this.afFileCancelUploadText}
													</button>
													<button
														type="button"
														onClick={(e) => this.onCancelFileHandler(e, file['id'])}
														class="digi-form-file-upload__button digi-form-file-upload__button--mobile"
														aria-label={
															this.afFileCancelUploadLongTemplateText?.replace
																? this.afFileCancelUploadLongTemplateText.replace(
																		'fileName',
																		file.name
																  )
																: _t.form.file_cancel_upload_long(file.name)
														}
													>
														<digi-icon-x-button-outline
															style={{
																'--digi--icon--height': '20px',
																'--digi--icon--color':
																	'var(--digi--global--color--function--info--base)'
															}}
															aria-hidden="true"
														></digi-icon-x-button-outline>
													</button>
												</div>
											</div>
										)}
										{file['status'] == 'OK' && (
											<div class="digi-form-file-upload__file">
												<div class="digi-form-file-upload__file-header">
													<div class="digi-form-file-upload__file-header-name-container">
														<digi-icon
															afName="notification-success"
															class="digi-form-file-upload__file-header-icon digi-form-file-upload__file-header-icon--success hidden-mobile"
														/>
														<digi-typography afVariation={TypographyVariation.LARGE}>
															<p class="digi-form-file-upload__file-name">{file.name}</p>
														</digi-typography>
													</div>
													<button
														type="button"
														onClick={(e) => this.onRemoveFileHandler(e, file['id'])}
														class="digi-form-file-upload__button hidden-mobile"
														aria-label={`${this.afRemoveAriaLabelText} ${file.name}`}
													>
														{this.afRemoveFileText}
													</button>
													<button
														type="button"
														onClick={(e) => this.onRemoveFileHandler(e, file['id'])}
														class="digi-form-file-upload__button digi-form-file-upload__button--mobile"
														aria-label={`${this.afRemoveAriaLabelText} ${file.name}`}
													>
														<digi-icon
															afName="trash"
															style={{
																'--digi--icon--height': '20px'
															}}
														></digi-icon>
													</button>
												</div>
											</div>
										)}
										{file['status'] == 'error' && (
											<div class="digi-form-file-upload__file">
												<div class="digi-form-file-upload__file-header">
													<digi-icon-danger-outline
														style={{
															'--digi--icon--height': '20px',
															'--digi--icon--color': 'red'
														}}
														aria-hidden="true"
														class={'hidden-mobile'}
													></digi-icon-danger-outline>
													<span
														class="digi-form-file-upload__file--error"
														aria-label={
															this.afFileErrorTemplateText.replace
																? this.afFileErrorTemplateText
																		.replace('{{error}}', file['error'])
																		.replace('{{fileName}}', file['name'])
																: _t.form.file_error(file['error'], file['name'])
														}
													>
														{this.afCanceledText} <span>|</span>
													</span>
													<p
														class="digi-form-file-upload__file-name
													digi-form-file-upload__file--error hidden-mobile
													"
													>
														{file.name}
													</p>
													{file['error'] === this.afFileErrorCanceledText && (
														<div style={{ display: 'flex' }}>
															<button
																type="button"
																onClick={(e) => this.onRetryFileHandler(e, file['id'])}
																class="digi-form-file-upload__button digi-form-file-upload__button--file hidden-mobile"
																aria-label={
																	this.afErrorTryAgainTemplateText?.replace
																		? this.afErrorTryAgainTemplateText.replace(
																				'{{fileName}}',
																				file.name
																		  )
																		: _t.form.file_error_try_again(file.name)
																}
															>
																{this.afTryAgainText}
																<span>|</span>
															</button>
															<button
																type="button"
																onClick={(e) => this.onRetryFileHandler(e, file['id'])}
																class="digi-form-file-upload__button digi-form-file-upload__button--mobile digi-form-file-upload__button--file"
																aria-label={
																	this.afErrorTryAgainTemplateText?.replace
																		? this.afErrorTryAgainTemplateText.replace(
																				'{{fileName}}',
																				file.name
																		  )
																		: _t.form.file_error_try_again(file.name)
																}
																style={{
																	'margin-right': '10px'
																}}
															>
																<digi-icon-update
																	style={{
																		'--digi--icon--height': '20px',
																		'--digi--icon--color':
																			'var(--digi--global--color--function--info--base)'
																	}}
																></digi-icon-update>
															</button>
														</div>
													)}
													<button
														type="button"
														onClick={(e) => this.onRemoveFileHandler(e, file['id'])}
														class="digi-form-file-upload__button digi-form-file-upload__button--file hidden-mobile"
														aria-label={
															this.afFileErrorRemoveTemplateText
																? this.afFileErrorRemoveTemplateText.replace(
																		'{{fileName}}',
																		file.name
																  )
																: _t.form.file_error_remove(file.name)
														}
													>
														{this.afRemoveFileText}
													</button>
													<button
														type="button"
														onClick={(e) => this.onRemoveFileHandler(e, file['id'])}
														class="digi-form-file-upload__button digi-form-file-upload__button--mobile digi-form-file-upload__button--file"
														aria-label={
															this.afFileErrorRemoveTemplateText
																? this.afFileErrorRemoveTemplateText.replace(
																		'{{fileName}}',
																		file.name
																  )
																: _t.form.file_error_remove(file.name)
														}
													>
														<digi-icon-x-button-outline
															style={{
																'--digi--icon--height': '20px',
																'--digi--icon--color':
																	'var(--digi--global--color--function--info--base)'
															}}
														></digi-icon-x-button-outline>
													</button>
												</div>
												<div class="digi-form-file-upload__file-footer">
													<p
														class={'hidden-mobile'}
														role="status"
														aria-label={
															this.afFileErrorTemplateText.replace
																? this.afFileErrorTemplateText
																		.replace('{{error}}', file['error'])
																		.replace('{{fileName}}', file['name'])
																: _t.form.file_error(file['error'], file['name'])
														}
													>
														{this.afFileErrorTemplateText.replace
															? this.afFileErrorTemplateText
																	.replace('{{error}}', file['error'])
																	.replace('{{fileName}}', '')
															: _t.form.file_error(file['error'], undefined)}
													</p>
												</div>
											</div>
										)}
									</li>
								);
							})}
						</ul>
					</div>
				)}
			</div>
		);
	}
}
