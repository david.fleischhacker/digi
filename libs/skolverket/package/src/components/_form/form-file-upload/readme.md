# digi-form-file-upload

<!-- Auto Generated Below -->


## Properties

| Property                             | Attribute                                  | Description                                                                                                                                                                                                                            | Type                                                                                                                                                                                                 | Default                                      |
| ------------------------------------ | ------------------------------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- |
| `afAnnounceIfOptional`               | `af-announce-if-optional`                  | Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria.                                                                                                                                                      | `boolean`                                                                                                                                                                                            | `false`                                      |
| `afAnnounceIfOptionalText`           | `af-announce-if-optional-text`             | Sätter text för afAnnounceIfOptional.                                                                                                                                                                                                  | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afCanceledText`                     | `af-canceled-text`                         | Sätt text för när uppladdningen blivit avbruten. Om inget värde anges används: "Avbruten".                                                                                                                                             | `string`                                                                                                                                                                                             | `_t.canceled`                                |
| `afErrorTryAgainTemplateText`        | `af-error-try-again-template-text`         | Sätter en textmall som används vid fel när användaren uppmanas att försöka igen. T.ex. "Misslyckad uppladdning, försök ladda upp filen igen {{fileName}}". Där {{fileName}} ersätts av filens namn.                                    | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afFileCancelUploadLongTemplateText` | `af-file-cancel-upload-long-template-text` | Sätter en textmall för att visa där man kan avbryta uppladdningen av en fil. T.ex. "Avbryt uppladdning av filen {{fileName}}". Där {{fileName}} ersätts av filens namn.                                                                | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afFileCancelUploadText`             | `af-file-cancel-upload-text`               | Sätt text som visas där användaren kan avbryta uppladdningen av en fil. Om inget värde anges används: "Avbryt uppladdning".                                                                                                            | `string`                                                                                                                                                                                             | `_t.form.file_cancel_upload`                 |
| `afFileDropZoneText`                 | `af-file-drop-zone-text`                   | Sätt text som visas på ytan där man kan dra och släppa filer.                                                                                                                                                                          | `string`                                                                                                                                                                                             | `_t.form.file_dropzone`                      |
| `afFileErrorCanceledText`            | `af-file-error-canceled-text`              | Sätt text som visas om uppladdningen avbryts. Om inget värde anges används: "Uppladdningen avbruten".                                                                                                                                  | `string`                                                                                                                                                                                             | `_t.form.file_error_canceled`                |
| `afFileErrorDuplicateTemplateText`   | `af-file-error-duplicate-template-text`    | Sätter en textmall för ett felmeddelande som visas när det redan finns en fil med samma namn som den fil man försöker importera. T.ex. "Det finns redan en fil med filnamnet ({{fileName}})". Där {{fileName}} ersätts av filens namn. | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afFileErrorFileAlreadyUploadedText` | `af-file-error-file-already-uploaded-text` | Sätt text för felmeddelande som visas när de valda filerna redan är uppladdade. Om inget värde anges används: "Alla valda filer är redan uppladdade".                                                                                  | `string`                                                                                                                                                                                             | `_t.form.file_error_already_uploaded`        |
| `afFileErrorFiletypeNotAllowedText`  | `af-file-error-filetype-not-allowed-text`  | Sätt text för felmeddelande som visas när filtypen är ogiltig. Om inget värde anges används: "Filtypen tillåts inte att ladda upp".                                                                                                    | `string`                                                                                                                                                                                             | `_t.form.file_error_filetype_not_allowed`    |
| `afFileErrorRemoveTemplateText`      | `af-file-error-remove-template-text`       | Sätter en textmall som används på en knapp för att ta bort filer som visas vid fel under uppladdning. T.ex. "Misslyckad uppladdning, ta bort {{fileName}} från fillistan". Där {{fileName}} ersätts av filens namn.                    | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afFileErrorTemplateText`            | `af-file-error-template-text`              | Sätter en textmall som visar felinformation vid uppladdning. T.ex. "{{fileName}} gick inte att ladda upp. {{error}}". Där {{fileName}} ersätts av filens namn och {{error}} med det inträffade felet.                                  | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afFileErrorTooLargeTemplateText`    | `af-file-error-too-large-template-text`    | Sätter en textmall för ett felmeddelande som visas när den uppladdade filens storlek är för stor. T.ex. "Filens storlek är för stor {{fileName}}". Där {{fileName}} ersätts av filens namn.                                            | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afFileErrorTooManyText`             | `af-file-error-too-many-text`              | Sätt text som visas om man redan laddat upp max antal filer. Om inget värde anges används: "Max antal filer uppladdade".                                                                                                               | `string`                                                                                                                                                                                             | `_t.form.file_error_too_many`                |
| `afFileMaxSize`                      | `af-file-max-size`                         | Sätter en maximal filstorlek i MB, 10MB är standard.                                                                                                                                                                                   | `number`                                                                                                                                                                                             | `10`                                         |
| `afFileTypes` _(required)_           | `af-file-types`                            | Sätter attributet 'accept'. Använd för att limitera accepterade filtyper                                                                                                                                                               | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afHeadingFiles`                     | `af-heading-files`                         | Sätt rubrik för uppladdade filer.                                                                                                                                                                                                      | `string`                                                                                                                                                                                             | `_t.form.file_uploaded_files`                |
| `afHeadingLevel`                     | `af-heading-level`                         | Sätt rubrikens vikt. 'h2' är förvalt.                                                                                                                                                                                                  | `FormFileUploadHeadingLevel.H1 \| FormFileUploadHeadingLevel.H2 \| FormFileUploadHeadingLevel.H3 \| FormFileUploadHeadingLevel.H4 \| FormFileUploadHeadingLevel.H5 \| FormFileUploadHeadingLevel.H6` | `FormFileUploadHeadingLevel.H2`              |
| `afId`                               | `af-id`                                    | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                                                                                                                                                                  | `string`                                                                                                                                                                                             | `randomIdGenerator('digi-form-file-upload')` |
| `afLabel`                            | `af-label`                                 | Texten till labelelementet                                                                                                                                                                                                             | `string`                                                                                                                                                                                             | `_t.form.file_upload`                        |
| `afLabelDescription`                 | `af-label-description`                     | Valfri beskrivande text                                                                                                                                                                                                                | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afMaxFiles`                         | `af-max-files`                             | Sätter maximalt antal filer man kan ladda upp                                                                                                                                                                                          | `number`                                                                                                                                                                                             | `undefined`                                  |
| `afName`                             | `af-name`                                  | Sätter attributet 'name'.                                                                                                                                                                                                              | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afRemoveAriaLabelText`              | `af-remove-aria-label-text`                | Sätt text som används i en aria-label där man kan ta bort en fil. Om inget värde anges används: "Ta bort"                                                                                                                              | `string`                                                                                                                                                                                             | `'Ta bort'`                                  |
| `afRemoveFileText`                   | `af-remove-file-text`                      | Sätt text för att ta bort fil. Om inget värde anges används: "Ta bort fil"                                                                                                                                                             | `string`                                                                                                                                                                                             | `'Ta bort fil'`                              |
| `afRequired`                         | `af-required`                              | Sätter attributet 'required'.                                                                                                                                                                                                          | `boolean`                                                                                                                                                                                            | `undefined`                                  |
| `afRequiredText`                     | `af-required-text`                         | Sätter text för afRequired.                                                                                                                                                                                                            | `string`                                                                                                                                                                                             | `undefined`                                  |
| `afTryAgainText`                     | `af-try-again-text`                        | Sätt text för att försöka igen. Om inget värde anges används: "Försök igen"                                                                                                                                                            | `string`                                                                                                                                                                                             | `'Försök igen'`                              |
| `afUploadBtnText`                    | `af-upload-btn-text`                       | Sätter ladda upp knappens text.                                                                                                                                                                                                        | `string`                                                                                                                                                                                             | `_t.form.file_choose`                        |
| `afUploadInProgressText`             | `af-upload-in-progress-text`               | Sätt text som visas under tiden filer håller på att laddas upp.                                                                                                                                                                        | `string`                                                                                                                                                                                             | `'Laddar upp...'`                            |


## Events

| Event            | Description                                       | Type               |
| ---------------- | ------------------------------------------------- | ------------------ |
| `afOnCancelFile` | Sänder ut vilken fil som har avbrutis uppladdning | `CustomEvent<any>` |
| `afOnRemoveFile` | Sänder ut vilken fil som tagits bort              | `CustomEvent<any>` |
| `afOnRetryFile`  | Sänder ut vilken fil som försöker laddas upp igen | `CustomEvent<any>` |
| `afOnUploadFile` | Sänder ut fil vid uppladdning                     | `CustomEvent<any>` |


## Methods

### `afMGetAllFiles() => Promise<File[]>`

Få ut alla uppladdade filer

#### Returns

Type: `Promise<File[]>`



### `afMGetFormControlElement() => Promise<HTMLInputElement>`



#### Returns

Type: `Promise<HTMLInputElement>`



### `afMImportFiles(files: File[]) => Promise<void>`

Importera en array med filer till komponenten utan att trigga uppladdningsevent. Ett fil objekt måste innehålla id, status och filen själv.

#### Parameters

| Name    | Type     | Description |
| ------- | -------- | ----------- |
| `files` | `File[]` |             |

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [digi-form-label](../../../__core/_form/form-label)
- [digi-icon](../../../__core/_icon/icon)
- [digi-button](../../../__core/_button/button)
- [digi-typography](../../../__core/_typography/typography)
- [digi-form-validation-message](../../../__core/_form/form-validation-message)
- [digi-icon-spinner](../../../__core/_icon/icon-spinner)
- [digi-icon-danger-outline](../../../__core/_icon/icon-danger-outline)

### Graph
```mermaid
graph TD;
  digi-form-file-upload --> digi-form-label
  digi-form-file-upload --> digi-icon
  digi-form-file-upload --> digi-button
  digi-form-file-upload --> digi-typography
  digi-form-file-upload --> digi-form-validation-message
  digi-form-file-upload --> digi-icon-spinner
  digi-form-file-upload --> digi-icon-danger-outline
  digi-form-validation-message --> digi-icon
  style digi-form-file-upload fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
