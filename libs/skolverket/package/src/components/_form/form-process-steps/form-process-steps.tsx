import { _t } from '@digi/skolverket/text';
import { Component, h, Element, Prop, State, Watch } from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import { ButtonType, ButtonVariation } from '../../../enums-core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

// Based on the step-by-step indicator here: https://www.w3.org/WAI/tutorials/forms/multi-page/

/**
 * @slot default - Ska innehålla flera <li><form-process-step></form-process-step></li>
 *
 * @swedishName Processteg
 */
@Component({
	tag: 'digi-form-process-steps',
	styleUrls: ['form-process-steps.scss'],
	scoped: true
})
export class FormProcessSteps {
	private _contentElement: HTMLDivElement;
	private _button: HTMLButtonElement;

	@Element() hostElement: HTMLStencilElement;

	/**
	 * När stegen inte får plats så ska de visas i en accordion.
	 */
	@State() isFallback = false;
	@State() isExpanded = false;
	@State() fallbackIsSet = false;
	@State() listWidth: number;
	@State() containerWidth: number;
	@State() steps: NodeListOf<HTMLDigiFormProcessStepElement>;

	@Watch('listWidth')
	@Watch('containerWidth')
	handleWidthChange() {
		this.isFallback = this.listWidth > this.containerWidth;
		if (!this.fallbackIsSet) {
			this.fallbackIsSet = true;
		}
	}

	@Prop() afCurrentStep!: number;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Set id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-form-process-steps');

	/**
	 * Sätter text på knapp som visas efter lista av expanderade steg för att dölja dessa i mobilläget.
	 * @en Sets text on button used to hide steps, appearing after the list of expanded steps in the mobile view.
	 */
	@Prop() afHideText?: string;
	/**
	 * Sätter text på knapp för att dölja alla steg.
	 * @en Sets text on button used to hide all steps.
	 */
	@Prop() afHideAllStepsText?: string;
	/**
	 * Sätter text på knapp för att visa steg.
	 * @en Sets text on button used to show steps.
	 */
	@Prop() afShowStepText?: string;
	/**
	 * Sätter text på knapp för att visa alla steg.
	 * @en Sets text on button used to show all steps.
	 */
	@Prop() afShowAllStepsText?: string;
	/**
	 * Sätter ett textformat för att visa det aktuella steget som t.ex.: "Steg {{currentStepNo}} av {{totalNoOfSteps}}" där {{currentStepNo}} kommer ersättas av det aktuella stegnumret och {{totalNoOfSteps}} av det totala antalet steg.
	 * @en Sets a text format to display the current step, e.g.: "Step {{currentStepNo}} of {{totalNoOfSteps}}" where {{currentStepNo}} is replaced by the current step number and {{totalNoOfSteps}} by the total number of steps.
	 */
	@Prop() afCurrentStepTemplateText?: string;

	getCurrentStepText(): string {
		if (!this) {
			return '';
		}

		if (!this.afCurrentStepTemplateText?.replace) {
			return `${_t.step} ${this.afCurrentStep} ${_t.of} ${this.steps.length}`;
		}

		return this.afCurrentStepTemplateText
			.replace('{{currentStepNo}}', this.afCurrentStep?.toString())
			.replace('{{totalNoOfSteps}}', this.steps?.length?.toString());
	}

	getToggleLabelText(): string {
		if (!this) {
			return '';
		}

		if (this.isExpanded) {
			return this.afHideAllStepsText ?? _t.hide_action('alla steg');
		}

		return this.afShowStepText ?? _t.show_action('steg');
	}

	getToggleInsideText(): string {
		if (!this) {
			return '';
		}

		if (this.isExpanded) {
			return this.afHideText ?? _t.hide;
		}

		return this.afShowAllStepsText ?? _t.show_action('alla steg');
	}

	componentWillLoad() {
		this.setTypeOnChildren();
	}

	componentDidLoad() {
		this.measureItemsList();
	}

	componentWillUpdate() {
		this.setTypeOnChildren();
	}

	handleFocusWithin() {
		this.isExpanded = true;
	}

	measureItemsList() {
		const itemsList = this.hostElement.querySelector('ol');
		this.listWidth = itemsList.scrollWidth;
	}

	@Watch('afCurrentStep')
	setTypeOnChildren() {
		const steps = this.hostElement.querySelectorAll('digi-form-process-step');
		this.steps = steps;
		steps.forEach((step, i) => {
			if (i + 1 === this.afCurrentStep) {
				step.afType = 'current';
			} else if (i + 1 < this.afCurrentStep) {
				step.afType = 'completed';
			} else {
				step.afType = 'upcoming';
			}
		});
	}

	@Watch('isFallback')
	setContextOnChildren() {
		const steps = this.hostElement.querySelectorAll('digi-form-process-step');
		steps.forEach(
			(step) => (step.afContext = this.isFallback ? 'fallback' : 'regular')
		);
	}

	resizeHandler() {
		this.containerWidth = this._contentElement.getBoundingClientRect().width;
	}

	clickToggleHandler(e: MouseEvent, resetFocus = false) {
		e.preventDefault();
		this.isExpanded = !this.isExpanded;
		if (resetFocus) {
			this._button.focus();
		}
	}

	get cssModifiers() {
		return {
			[`digi-form-process-steps--expanded-${this.isExpanded}`]: true,
			[`digi-form-process-steps--fallback-${this.isFallback}`]: true,
			[`digi-form-process-steps--fallback-is-set`]: this.fallbackIsSet
		};
	}

	render() {
		return (
			<digi-util-resize-observer onAfOnChange={() => this.resizeHandler()}>
				<div class={{ 'digi-form-process-steps': true, ...this.cssModifiers }}>
					{this.isFallback && (
						<button
							class="digi-form-process-steps__toggle"
							type="button"
							aria-pressed={this.isExpanded ? 'true' : 'false'}
							aria-expanded={this.isExpanded ? 'true' : 'false'}
							aria-controls={`${this.afId}-content`}
							onClick={(e) => this.clickToggleHandler(e, true)}
							ref={(el) => (this._button = el)}
						>
							<div
								class="digi-form-process-steps__toggle-heading"
								data-current-step={this.afCurrentStep}
							>
								<div class="digi-form-process-steps__toggle-text">
									<span>{this.getCurrentStepText()}</span>
									<p>{this.steps[this.afCurrentStep - 1]?.textContent}</p>
								</div>
							</div>
							<span class="digi-form-process-steps__toggle-label">
								{this.getToggleLabelText()}
								<digi-icon
									afName={this.isExpanded ? 'chevron-up' : 'chevron-down'}
									aria-hidden
									slot="icon-secondary"
								/>
							</span>
						</button>
					)}
					<div
						class="digi-form-process-steps__content"
						ref={(el) => (this._contentElement = el)}
					>
						<ol
							class="digi-form-process-steps__items"
							id={`${this.afId}-items`}
							onFocusin={() => this.handleFocusWithin()}
						>
							<slot />
						</ol>
						{this.isFallback && (
							<digi-button
								class="digi-form-process-steps__toggle-inside"
								afVariation={ButtonVariation.FUNCTION}
								afType={ButtonType.BUTTON}
								onAfOnClick={(e) => this.clickToggleHandler(e.detail, true)}
								afFullWidth
								afAriaPressed={this.isExpanded}
								afAriaExpanded={this.isExpanded}
								afAriaControls={`${this.afId}-content`}
							>
								{this.getToggleInsideText()}
								<digi-icon
									afName={this.isExpanded ? 'chevron-up' : 'chevron-down'}
									aria-hidden
									slot="icon-secondary"
								/>
							</digi-button>
						)}
					</div>
				</div>
			</digi-util-resize-observer>
		);
	}
}
