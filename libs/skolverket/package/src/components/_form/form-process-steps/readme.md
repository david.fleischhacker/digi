# digi-form-process-steps

<!-- Auto Generated Below -->


## Properties

| Property                     | Attribute                       | Description                                                                                                                                                                                                                          | Type     | Default                                        |
| ---------------------------- | ------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | -------- | ---------------------------------------------- |
| `afCurrentStep` _(required)_ | `af-current-step`               |                                                                                                                                                                                                                                      | `number` | `undefined`                                    |
| `afCurrentStepTemplateText`  | `af-current-step-template-text` | Sätter ett textformat för att visa det aktuella steget som t.ex.: "Steg {{currentStepNo}} av {{totalNoOfSteps}}" där {{currentStepNo}} kommer ersättas av det aktuella stegnumret och {{totalNoOfSteps}} av det totala antalet steg. | `string` | `undefined`                                    |
| `afHideAllStepsText`         | `af-hide-all-steps-text`        | Sätter text på knapp för att dölja alla steg.                                                                                                                                                                                        | `string` | `undefined`                                    |
| `afHideText`                 | `af-hide-text`                  | Sätter text på knapp som visas efter lista av expanderade steg för att dölja dessa i mobilläget.                                                                                                                                     | `string` | `undefined`                                    |
| `afId`                       | `af-id`                         | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                                                                                                                                                                | `string` | `randomIdGenerator('digi-form-process-steps')` |
| `afShowAllStepsText`         | `af-show-all-steps-text`        | Sätter text på knapp för att visa alla steg.                                                                                                                                                                                         | `string` | `undefined`                                    |
| `afShowStepText`             | `af-show-step-text`             | Sätter text på knapp för att visa steg.                                                                                                                                                                                              | `string` | `undefined`                                    |


## Slots

| Slot        | Description                                                          |
| ----------- | -------------------------------------------------------------------- |
| `"default"` | Ska innehålla flera <li><form-process-step></form-process-step></li> |


## Dependencies

### Depends on

- [digi-util-resize-observer](../../../__core/_util/util-resize-observer)
- [digi-icon](../../../__core/_icon/icon)
- [digi-button](../../../__core/_button/button)

### Graph
```mermaid
graph TD;
  digi-form-process-steps --> digi-util-resize-observer
  digi-form-process-steps --> digi-icon
  digi-form-process-steps --> digi-button
  style digi-form-process-steps fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
