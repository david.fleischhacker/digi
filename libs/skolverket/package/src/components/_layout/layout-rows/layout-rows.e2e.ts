import { newE2EPage } from '@stencil/core/testing';

describe('digi-layout-rows', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-layout-rows></digi-layout-rows>');

		const element = await page.find('digi-layout-rows');
		expect(element).toHaveClass('hydrated');
	});
});
