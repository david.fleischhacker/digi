import { Component, h, Prop } from '@stencil/core';
import { LayoutStackedBlocksVariation } from './layout-stacked-blocks-variation.enum';

/**
 * @slot default - kan innehålla vad som helst
 *
 * @swedishName Staplande block
 */
@Component({
	tag: 'digi-layout-stacked-blocks',
	styleUrls: ['layout-stacked-blocks.scss'],
	scoped: true
})
export class LayoutStackedBlocks {
	@Prop() afVariation: `${LayoutStackedBlocksVariation}` =
		LayoutStackedBlocksVariation.REGULAR;

	render() {
		return (
			<div
				class={{
					'digi-layout-stacked-blocks': true,
					[`digi-layout-stacked-blocks--variation-${this.afVariation}`]:
						!!this.afVariation
				}}
			>
				<slot></slot>
			</div>
		);
	}
}
