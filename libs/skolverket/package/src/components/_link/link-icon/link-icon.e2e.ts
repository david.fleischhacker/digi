import { newE2EPage } from '@stencil/core/testing';

describe('digi-link-icon', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-link-icon></digi-link-icon>');

		const element = await page.find('digi-link-icon');
		expect(element).toHaveClass('hydrated');
	});
});
