import { newE2EPage } from '@stencil/core/testing';

describe('digi-list-link', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-list-link></digi-list-link>');

		const element = await page.find('digi-list-link');
		expect(element).toHaveClass('hydrated');
	});
});
