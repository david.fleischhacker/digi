import { Template, enumSelect } from '../../../../../../shared/utils/src';

export default {
	title: 'logo/digi-logo-service'
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-logo-service',
	'af-name': 'Stadsbidrag',
	'af-description': 'En e-tjänst från Skolverket',
	'af-title-id': null
};
