import { Template, enumSelect } from '../../../../../../shared/utils/src';

export default {
	title: 'logo/digi-logo-sister'
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-logo-sister',
	'af-name': 'Utbildningsguiden',
	'af-title-id': null
};
