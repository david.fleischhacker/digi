import { Template, enumSelect } from '../../../../../../shared/utils/src';

export default {
	title: 'logo/digi-logo'
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-logo',
	'af-title': '',
	'af-desc': '',
	'af-svg-aria-hidden': false,
	'af-title-id': null,
	children: ''
};
