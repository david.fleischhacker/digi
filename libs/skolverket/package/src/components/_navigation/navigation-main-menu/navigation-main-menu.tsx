import {
	Component,
	Element,
	Event,
	EventEmitter,
	Prop,
	State,
	h
} from '@stencil/core';
import { HTMLStencilElement, Listen } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { BREAKPOINT_LARGE } from '../../../design-tokens/web/tokens.es6';

/**
 * @slot default - Se översikten för exakt innehåll
 *
 *@swedishName Huvudmeny
 */
@Component({
	tag: 'digi-navigation-main-menu',
	styleUrls: ['navigation-main-menu.scss'],
	scoped: true
})
export class NavigationMainMenu {
	@Element() hostElement: HTMLStencilElement;

	@State() menuButtons = [];
	@State() isActive = false;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Set id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-navigation-main-menu');

	@Event() afOnRoute: EventEmitter;

	componentWillLoad() {
		this.menuButtons = Array.from(
			this.hostElement.querySelectorAll('nav > ul > li > button')
		);

		if (this.menuButtons.length) {
			this.menuButtons.forEach(this.hydrateButton.bind(this));
		}
	}

	hydrateButton(button, i) {
		button.setAttribute('aria-expanded', 'false');
		button.setAttribute('aria-controls', `${this.afId}-submenu-${i}`);
		button.nextElementSibling?.setAttribute('id', `${this.afId}-submenu-${i}`);
		button.nextElementSibling?.addEventListener('afOnResize', (e) => {
			this.setPanelHeight(button.closest('li'), e.detail);
		});
		button.addEventListener('click', (e) => this.toggleMenu(e));
	}

	toggleMenu(e) {
		this.menuButtons.forEach((button) => {
			if (button !== e.target) {
				this.removeActive(button);
			}
		});

		e.target.setAttribute(
			'aria-expanded',
			e.target.getAttribute('aria-expanded') === 'true' ? 'false' : 'true'
		);
		e.target.closest('li').setAttribute('data-active', 'true');
		this.setMainLinkFocus(e.target);
		this.isActive = true;
	}

	setMainLinkFocus(target) {
		if (window.matchMedia(`(min-width: ${BREAKPOINT_LARGE})`).matches) return;
		target.nextElementSibling
			?.querySelector('a[slot="main-link"], [slot="main-link"] a')
			?.focus();
	}

	setTopLevelButtonFocus(button) {
		if (window.matchMedia(`(min-width: ${BREAKPOINT_LARGE})`).matches) return;
		button.nextElementSibling
			?.querySelector('a[slot="main-link"], [slot="main-link"] a')
			?.focus();
	}

	setPanelHeight(li, resizeObserverEntry) {
		li.style.setProperty(
			'--panel-height',
			`${resizeObserverEntry.contentRect.height}px`
		);
	}

	removeActive(button) {
		button.setAttribute('aria-expanded', 'false');
		button.closest('li').removeAttribute('data-active');
		this.isActive = false;
	}

	@Listen('afOnClose')
	closeSubmenuHandler(e) {
		this.closeSubmenu(e);
	}

	@Listen('keyup')
	keyUpHandler(e: KeyboardEvent) {
		if (
			e.key !== 'Escape' ||
			!window.matchMedia(`(min-width: ${BREAKPOINT_LARGE})`).matches
		)
			return;
		this.closeSubmenu(e);
	}

	closeSubmenu(e) {
		const button = this.hostElement.querySelector(
			'[data-active="true"] button'
		) as HTMLButtonElement;
		if (!button) return;
		this.removeActive(button);

		if (e.detail.target.tagName !== 'A') {
			setTimeout(() => {
				button.focus();
			}, 10);
		} else {
			this.afOnRoute.emit();
		}
	}

	get cssModifiers() {
		return {
			[`digi-navigation-main-menu--active-${this.isActive}`]: true
		};
	}

	render() {
		return (
			<digi-layout-container>
				<div
					class={{
						'digi-navigation-main-menu': true,
						...this.cssModifiers
					}}
				>
					<slot></slot>
				</div>
			</digi-layout-container>
		);
	}
}
