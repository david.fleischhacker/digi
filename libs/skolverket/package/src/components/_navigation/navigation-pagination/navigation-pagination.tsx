import {
	Component,
	Event,
	EventEmitter,
	Prop,
	State,
	Watch,
	h,
	Method
} from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { ButtonSize, ButtonVariation } from '../../../enums-core';
import { _t } from '@digi/skolverket/text';

/**
 * @swedishName Paginering
 */

@Component({
	tag: 'digi-navigation-pagination',
	styleUrls: ['navigation-pagination.scss'],
	scoped: true
})
export class NavigationPagination {
	@State() pages = [];
	@State() currentPage: number;
	@State() totalPages: number;

	/**
	 * Sätter initiala aktiva sidan
	 * @en Set initial start page
	 */
	@Prop() afInitActivePage: number = 1;
	@Watch('afInitActivePage')
	afInitActivePageChanged() {
		this.setCurrentPage(this.afInitActivePage, false);
	}

	/**
	 * Sätter totala mängden sidor
	 * @en Set total pages that exists
	 */
	@Prop() afTotalPages: number;
	@Watch('afTotalPages')
	afTotalPagesChanged() {
		this.setTotalPages(this.afTotalPages);
	}

	/**
	 * Sätter startvärdet för nuvarande resultat
	 * @en Set start value of current results
	 */
	@Prop() afCurrentResultStart: number;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Input id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-navigation-pagination');

	/**
	 * Sätter slutvärdet för nuvarande resultat
	 * @en Set end value of current results
	 */
	@Prop() afCurrentResultEnd: number;

	/**
	 * Sätter totala mängden resultat
	 * @en Set total results
	 */
	@Prop() afTotalResults: number = 0;

	/**
	 * Sätter ett resultatnamn. Det används för att kommunicera vad som listas. T.ex. om värdet är 'användare' så kan det stå 'Visar 23-138 användare'.
	 * @en Set a result name. This is used to communicate what is being listed. For example, if the value is 'users', the component might say 'Showing 23-138 users'.
	 */
	@Prop() afResultName: string = _t.hits.toLowerCase();

	/**
	 * Vid byte av sida
	 * @en When page changes
	 */
	@Event() afOnPageChange: EventEmitter<number>;

	/**
	 * Kan användas för att manuellt sätta om den aktiva sidan.
	 * @en Can be used to set the active page.
	 */
	@Method()
	async afMSetCurrentPage(pageNumber: number) {
		this.setCurrentPage(pageNumber, false);
	}

	prevPage() {
		this.currentPage--;
		this.afOnPageChange.emit(this.currentPage);
	}

	nextPage() {
		this.currentPage++;
		this.afOnPageChange.emit(this.currentPage);
	}

	setTotalPages(pagesToSet = this.totalPages) {
		if (pagesToSet === this.totalPages) {
			return;
		}

		this.totalPages = pagesToSet;
	}

	setCurrentPage(pageToSet = this.currentPage, emitEvent = true) {
		if (pageToSet === this.currentPage || isNaN(pageToSet)) {
			return;
		}

		this.currentPage =
			pageToSet <= 1
				? 1
				: pageToSet <= this.afTotalPages
				? pageToSet
				: this.afTotalPages;

		if (emitEvent) {
			this.afOnPageChange.emit(this.currentPage);
		}
	}

	componentWillLoad() {
		this.setCurrentPage(this.afInitActivePage, false);
		this.updateTotalPages();
	}

	@Watch('afTotalPages')
	updateTotalPages() {
		if (this.afTotalPages) {
			this.pages = [...Array(this.afTotalPages)];
		}
	}

	isCurrentPage(currentPage: number, page: number) {
		if (currentPage === page || null) {
			return 'page';
		}
	}

	labelledby() {
		if (this.afTotalResults > 0) {
			return `
        ${this.afId}-aria-label
        ${this.afId}-result-show
        ${this.afId}-result-current
        ${this.afId}-result-of
        ${this.afId}-result-total
        ${this.afId}-result-name
      `;
		} else {
			return `${this.afId}-aria-label`;
		}
	}

	render() {
		return (
			<div class="digi-navigation-pagination">
				<span
					id={`${this.afId}-aria-label`}
					aria-hidden="true"
					class="digi-navigation-pagination__aria-label"
				>
					Paginering
				</span>
				{this.afTotalResults > 0 && (
					<div
						class={{
							'digi-navigation-pagination__result': true,
							'digi-navigation-pagination__result--pages': this.pages.length > 1
						}}
						aria-hidden={this.pages.length > 1 ? 'true' : 'false'}
						id={`${this.afId}-result`}
					>
						<digi-typography>
							<span id={`${this.afId}-result-show`}>{_t.showing} </span>
							<strong id={`${this.afId}-result-current`}>
								{this.afCurrentResultStart}-{this.afCurrentResultEnd}
							</strong>
							<span id={`${this.afId}-result-of`}> {_t.of} </span>
							<strong id={`${this.afId}-result-total`}>{this.afTotalResults}</strong>
							{this.afResultName && (
								<span id={`${this.afId}-result-name`}>{` ${this.afResultName}`}</span>
							)}
						</digi-typography>
					</div>
				)}
				{this.pages.length > 1 && (
					<nav
						aria-labelledby={this.labelledby()}
						class={{
							'digi-navigation-pagination__pagination': true
						}}
					>
						<div class="digi-navigation-pagination__select">
							<digi-form-select
								afLabel={`Sida`}
								afValue={`${this.currentPage}`}
								onAfOnChange={(e) => {
									this.setCurrentPage(parseInt(e.detail.target.value), true);
								}}
							>
								{this.pages.map((_, index) => (
									<option
										value={`${index + 1}`}
										selected={index + 1 === this.currentPage}
									>
										{index + 1} av {this.afTotalPages}
									</option>
								))}
							</digi-form-select>
						</div>
						<div class="digi-navigation-pagination__buttons">
							<digi-button
								onClick={() => this.prevPage()}
								afVariation={ButtonVariation.SECONDARY}
								afSize={ButtonSize.LARGE}
								afFullWidth
								class={{
									'digi-navigation-pagination__button digi-navigation-pagination__button--previous':
										true,
									'digi-navigation-pagination__button--keep-left':
										this.currentPage === this.pages.length,
									'digi-navigation-pagination__button--hidden': this.currentPage === 1
								}}
							>
								<digi-icon slot="icon" afName={`chevron-left`}></digi-icon>
								<span>{_t.previous}</span>
							</digi-button>
							<digi-button
								onClick={() => this.nextPage()}
								afVariation={ButtonVariation.SECONDARY}
								afSize={ButtonSize.LARGE}
								afFullWidth
								class={{
									'digi-navigation-pagination__button digi-navigation-pagination__button--next':
										true,
									'digi-navigation-pagination__button--keep-right':
										this.currentPage === 1,
									'digi-navigation-pagination__button--hidden':
										this.currentPage === this.afTotalPages
								}}
								af-variation="secondary"
							>
								<digi-icon slot="icon-secondary" afName={`chevron-right`}></digi-icon>
								<span>{_t.next}</span>
							</digi-button>
						</div>
					</nav>
				)}
			</div>
		);
	}
}
