# digi-notification-alert

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                              | Type                              | Default                           |
| ------------- | -------------- | -------------------------------------------------------- | --------------------------------- | --------------------------------- |
| `afCloseable` | `af-closeable` | Gör det möjligt att stänga meddelandet med en stängknapp | `boolean`                         | `false`                           |
| `afVariation` | `af-variation` | Sätter variant.                                          | `"danger" \| "info" \| "warning"` | `NotificationAlertVariation.INFO` |


## Events

| Event       | Description                   | Type                      |
| ----------- | ----------------------------- | ------------------------- |
| `afOnClose` | Stängknappens 'onclick'-event | `CustomEvent<MouseEvent>` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"actions"` | Knappar                     |
| `"default"` | Kan innehålla vad som helst |
| `"heading"` | Rubrik                      |
| `"link"`    | Länk                        |


## CSS Custom Properties

| Name                                                    | Description                                           |
| ------------------------------------------------------- | ----------------------------------------------------- |
| `--digi--notification-alert--background-color--danger`  | var(--digi--color--background--notification-danger);  |
| `--digi--notification-alert--background-color--info`    | var(--digi--color--background--notification-info);    |
| `--digi--notification-alert--background-color--warning` | var(--digi--color--background--notification-warning); |
| `--digi--notification-alert--border-color--danger`      | var(--digi--color--border--danger);                   |
| `--digi--notification-alert--border-color--info`        | var(--digi--color--border--informative);              |
| `--digi--notification-alert--border-color--warning`     | var(--digi--color--border--warning);                  |


## Dependencies

### Used by

 - [digi-notification-cookie](../notification-cookie)

### Depends on

- [digi-icon](../../../__core/_icon/icon)
- [digi-button](../../../__core/_button/button)
- [digi-typography](../../../__core/_typography/typography)

### Graph
```mermaid
graph TD;
  digi-notification-alert --> digi-icon
  digi-notification-alert --> digi-button
  digi-notification-alert --> digi-typography
  digi-notification-cookie --> digi-notification-alert
  style digi-notification-alert fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
