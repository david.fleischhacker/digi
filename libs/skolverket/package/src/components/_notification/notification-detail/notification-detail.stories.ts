import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { NotificationDetailVariation } from './notification-detail-variation.enum';

export default {
	title: 'notification/digi-notification-detail',
	argTypes: {
		'af-variation': enumSelect(NotificationDetailVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-notification-detail',
	'af-variation': NotificationDetailVariation.INFO,
	/* html */
	children: `
		<h2 slot="heading">Driftstörning</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat tortor id ultricies convallis. Aenean ultrices tortor eget varius auctor. In ac est eu turpis porttitor rutrum.</p>
		`
};
