import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { PageBlockCardsVariation } from './page-block-cards-variation.enum';

export default {
	title: 'page/digi-page-block-cards',
	argTypes: {
		'af-variation': enumSelect(PageBlockCardsVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-page-block-cards',
	'af-variation': 'start',
	/* html */
	children: `
    <h2 slot="heading">Undervisning</h2>
    <digi-card-link>
      <img slot="image" src="https://www.skolverket.se/images/18.3770ea921807432e6c726e7/1656400441366/s%C3%A4kerhet-kris-bild-webb.png" width="392" height="220" style="width: 100%; height: auto; display: block;" />
      <h2 slot="link-heading"><a href="#">Beredskap vid ett förändrat omvärldsläge</a></h2>
      <p>Förskola och skola är samhällsviktig verksamhet som behöver fungera även vid olika typer av kriser. Här finns stöd för hur du kan förbereda din verksamhet vid olika händelser av kris.</p>
    </digi-card-link>
    <digi-card-link>
      <img slot="image" src="https://www.skolverket.se/images/18.3e89579d17f69780fbcceb/1648714549765/Mottagandet%20fr%C3%A5n%20Ukraina%20-puffbild.png" width="392" height="220" style="width: 100%; height: auto; display: block;" />
      <h2 slot="link-heading"><a href="#" slot="link-heading">Nyanlända barn och elever från Ukraina</a></h2>
      <p>Här hittar du som tar emot barn och elever från Ukraina information vad som gäller, det stöd vi erbjuder för mottagande och kartläggning och om skolsystemet och de olika skolformerna på engelska.</p>
    </digi-card-link>
    <digi-card-link>
      <h2 slot="link-heading"><a href="#">Ett kort utan bild</a></h2>
      <p>Etiam porta sem malesuada magna mollis euismod.</p>
      <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
    </digi-card-link>
    <digi-card-link>
      <img slot="image" src="https://www.skolverket.se/images/18.d9bdf0a17bea20b43b101f/1633508909105/andringar-kursplaner-IG_355x200p.jpg" width="392" height="220" style="width: 100%; height: auto; display: block;" />
      <h2 slot="link-heading"><a href="#">Och så en länkad rubrik</a></h2>
    </digi-card-link>
    <digi-card-link>
      <div slot="image"></div>
      <h2 slot="link-heading"><a href="#">Och så en länkad rubrik</a></h2>
      <p>Lägg in nåt tomt element i slot="image" så får du en automatisk placeholder</p>
    </digi-card-link>
  `
};
