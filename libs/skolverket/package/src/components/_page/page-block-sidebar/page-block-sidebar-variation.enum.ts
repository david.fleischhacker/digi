export enum PageBlockSidebarVariation {
	START = 'start',
	SUB = 'sub',
	SECTION = 'section',
	PROCESS = 'process',
	ARTICLE = 'article'
}
