export enum PageFooterVariation {
	PRIMARY = 'primary',
	SECONDARY = 'secondary'
}
