# digi-page-header

<!-- Auto Generated Below -->


## Properties

| Property          | Attribute            | Description                                                                      | Type     | Default                                 |
| ----------------- | -------------------- | -------------------------------------------------------------------------------- | -------- | --------------------------------------- |
| `afCloseMenuText` | `af-close-menu-text` | Text som visas när menyn är öppen i mobilläget. Om inget väljs så visas "Stäng". | `string` | `undefined`                             |
| `afId`            | `af-id`              | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.            | `string` | `randomIdGenerator('digi-page-header')` |
| `afOpenMenuText`  | `af-open-menu-text`  | Text som visas när menyn är stängd i mobilläget. Om inget väljs så visas "Meny". | `string` | `undefined`                             |


## Slots

| Slot        | Description                                           |
| ----------- | ----------------------------------------------------- |
| `"default"` | kan innehålla vad som helst                           |
| `"logo"`    | ska innehålla en logotyp                              |
| `"nav"`     | ska innehålla en navigation                           |
| `"top"`     | länk till mina sidor, tillbaka till skolvetket.se etc |


## Dependencies

### Depends on

- [digi-layout-container](../../../__core/_layout/layout-container)
- [digi-icon](../../../__core/_icon/icon)
- [digi-navigation-main-menu](../../_navigation/navigation-main-menu)

### Graph
```mermaid
graph TD;
  digi-page-header --> digi-layout-container
  digi-page-header --> digi-icon
  digi-page-header --> digi-navigation-main-menu
  digi-navigation-main-menu --> digi-layout-container
  style digi-page-header fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
