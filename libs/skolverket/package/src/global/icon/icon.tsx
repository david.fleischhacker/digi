import { h } from '@stencil/core';
import { icon as coreIcon } from '@digi/core/global/icon/icon';

export const skvIcon = {
	'accessibility-deaf': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path
						d="M17,20a1.9,1.9,0,0,1-.76-.15,3.475,3.475,0,0,1-1.71-2.38,5.892,5.892,0,0,0-2.39-3,7.6,7.6,0,0,1-2.32-2.53A6.264,6.264,0,0,1,9,9,5,5,0,0,1,19,9h2a6.92,6.92,0,0,0-7-7A6.92,6.92,0,0,0,7,9a8.278,8.278,0,0,0,1.07,3.9,9.533,9.533,0,0,0,2.85,3.15,4.028,4.028,0,0,1,1.71,2.05,5.443,5.443,0,0,0,2.73,3.55A4,4,0,0,0,21,18H19a2.006,2.006,0,0,1-2,2ZM7.64,2.64,6.22,1.22a11.009,11.009,0,0,0,0,15.56l1.41-1.41A9.005,9.005,0,0,1,7.64,2.64ZM11.5,9A2.5,2.5,0,1,0,14,6.5,2.5,2.5,0,0,0,11.5,9Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	bars: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path
						d="M3,18H21V16H3Zm0-5H21V11H3ZM3,6V8H21V6Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	'notification-success': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8.011,8.011,0,0,1,12,20ZM16.59,7.58,10,14.17,7.41,11.59,6,13l4,4,8-8Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	check: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path
						d="M9,16.2,4.8,12,3.4,13.4,9,19,21,7,19.6,5.6Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	'chevron-down': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					transform="rotate(90)"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M288.917,415.517,287.5,414.1l3.96-3.96-3.96-3.96,1.414-1.414,5.373,5.374Z"
						transform="translate(-278.17 -398.103)"
						fill="currentColor"
					/>
				</svg>
			);
		}
	},
	'chevron-left': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					transform="rotate(180)"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M288.917,415.517,287.5,414.1l3.96-3.96-3.96-3.96,1.414-1.414,5.373,5.374Z"
						transform="translate(-278.17 -398.103)"
						fill="currentColor"
					/>
				</svg>
			);
		}
	},
	'chevron-right': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M288.917,415.517,287.5,414.1l3.96-3.96-3.96-3.96,1.414-1.414,5.373,5.374Z"
						transform="translate(-278.17 -398.103)"
						fill="currentColor"
					/>
				</svg>
			);
		}
	},
	'chevron-up': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					transform="rotate(-90)"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M288.917,415.517,287.5,414.1l3.96-3.96-3.96-3.96,1.414-1.414,5.373,5.374Z"
						transform="translate(-278.17 -398.103)"
						fill="currentColor"
					/>
				</svg>
			);
		}
	},
	dislike: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

					transform="rotate(180)"
				>
					{children}
					<path d="M0,0H24V24H0ZM0,0H24V24H0Z" fill="none" />
					<g fill="currentColor">
						<path d="M9,21h9a1.987,1.987,0,0,0,1.84-1.22l3.02-7.05A1.976,1.976,0,0,0,23,12V10a2.006,2.006,0,0,0-2-2H14.69l.95-4.57.03-.32a1.505,1.505,0,0,0-.44-1.06L14.17,1,7.58,7.59A1.987,1.987,0,0,0,7,9V19A2.006,2.006,0,0,0,9,21ZM9,9l4.34-4.34L12,10h9v2l-3,7H9ZM1,9H5V21H1Z" />
					</g>
				</svg>
			);
		}
	},
	'exclamation-triangle-warning': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<g fill="currentColor">
						<path
							d="M12,5.99,19.53,19H4.47L12,5.99M12,2,1,21H23Zm1,14H11v2h2Zm0-6H11v4h2Z"
							fill-rule="nonzero"
						/>
					</g>
				</svg>
			);
		}
	},
	globe: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path
						d="M11.99,2A10,10,0,1,0,22,12,10,10,0,0,0,11.99,2Zm6.93,6H15.97a15.649,15.649,0,0,0-1.38-3.56A8.03,8.03,0,0,1,18.92,8ZM12,4.04A14.087,14.087,0,0,1,13.91,8H10.09A14.087,14.087,0,0,1,12,4.04ZM4.26,14a7.822,7.822,0,0,1,0-4H7.64a16.515,16.515,0,0,0-.14,2,16.515,16.515,0,0,0,.14,2Zm.82,2H8.03a15.649,15.649,0,0,0,1.38,3.56A7.987,7.987,0,0,1,5.08,16ZM8.03,8H5.08A7.987,7.987,0,0,1,9.41,4.44,15.649,15.649,0,0,0,8.03,8ZM12,19.96A14.087,14.087,0,0,1,10.09,16h3.82A14.087,14.087,0,0,1,12,19.96ZM14.34,14H9.66a14.713,14.713,0,0,1-.16-2,14.585,14.585,0,0,1,.16-2h4.68a14.585,14.585,0,0,1,.16,2A14.713,14.713,0,0,1,14.34,14Zm.25,5.56A15.649,15.649,0,0,0,15.97,16h2.95A8.03,8.03,0,0,1,14.59,19.56ZM16.36,14a16.515,16.515,0,0,0,.14-2,16.515,16.515,0,0,0-.14-2h3.38a7.822,7.822,0,0,1,0,4Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	lattlast: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path
						d="M-12518.443-17740.133v11.785h7.209"
						transform="translate(12525 17747)"
						fill="none"
						stroke="currentColor"
						stroke-width="2"
					/>
					<path
						d="M-12518.443-17740.133v11.785h7.209"
						transform="translate(12528.604 17743)"
						fill="none"
						stroke="currentColor"
						stroke-width="2"
					/>
				</svg>
			);
		}
	},
	like: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0ZM0,0H24V24H0Z" fill="none" />
					<g fill="currentColor">
						<path d="M9,21h9a1.987,1.987,0,0,0,1.84-1.22l3.02-7.05A1.976,1.976,0,0,0,23,12V10a2.006,2.006,0,0,0-2-2H14.69l.95-4.57.03-.32a1.505,1.505,0,0,0-.44-1.06L14.17,1,7.58,7.59A1.987,1.987,0,0,0,7,9V19A2.006,2.006,0,0,0,9,21ZM9,9l4.34-4.34L12,10h9v2l-3,7H9ZM1,9H5V21H1Z" />
					</g>
				</svg>
			);
		}
	},
	minus: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path d="M19,13H5V11H19Z" fill="currentColor" fill-rule="nonzero" />
				</svg>
			);
		}
	},
	plus: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path
						d="M19,13H13v6H11V13H5V11h6V5h2v6h6Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	search: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path
						d="M15.5,14h-.79l-.28-.27a6.51,6.51,0,1,0-.7.7l.27.28v.79l5,4.99L20.49,19Zm-6,0A4.5,4.5,0,1,1,14,9.5,4.494,4.494,0,0,1,9.5,14Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	sort: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<path
						d="M10,18h4V16H10ZM3,6V8H21V6Zm3,7H18V11H6Z"
						fill="currentColor"
						fill-rule="evenodd"
					/>
				</svg>
			);
		}
	},
	x: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<g fill="currentColor">
						<path d="M19,6.41,17.59,5,12,10.59,6.41,5,5,6.41,10.59,12,5,17.59,6.41,19,12,13.41,17.59,19,19,17.59,13.41,12Z" />
					</g>
				</svg>
			);
		}
	},
	'input-select-marker': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					xmlns="http://www.w3.org/2000/svg"
					width="24"
					height="24"
					viewBox="0 0 24 24"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="m7 10 5 5 5-5Z" fill="currentColor" />
				</svg>
			);
		}
	},
	'notification-danger': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<g fill="currentColor">
						<path
							d="M12,5.99,19.53,19H4.47L12,5.99M12,2,1,21H23Zm1,14H11v2h2Zm0-6H11v4h2Z"
							fill-rule="nonzero"
						/>
					</g>
				</svg>
			);
		}
	},
	'notification-warning': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<g fill="currentColor">
						<path
							d="M11,15h2v2H11Zm0-8h2v6H11Zm.99-5A10,10,0,1,0,22,12,10,10,0,0,0,11.99,2ZM12,20a8,8,0,1,1,8-8A8,8,0,0,1,12,20Z"
							fill-rule="nonzero"
						/>
					</g>
				</svg>
			);
		}
	},
	'notification-info': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<g fill="currentColor">
						<path
							d="M11,15h2v2H11Zm0-8h2v6H11Zm.99-5A10,10,0,1,0,22,12,10,10,0,0,0,11.99,2ZM12,20a8,8,0,1,1,8-8A8,8,0,0,1,12,20Z"
							fill-rule="nonzero"
						/>
					</g>
				</svg>
			);
		}
	},
	close: {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="24"
					height="24"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path d="M0,0H24V24H0Z" fill="none" />
					<g fill="currentColor">
						<path d="M19,6.41,17.59,5,12,10.59,6.41,5,5,6.41,10.59,12,5,17.59,6.41,19,12,13.41,17.59,19,19,17.59,13.41,12Z" />
					</g>
				</svg>
			);
		}
	},
	'notification-error': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="48"
					height="48"
					viewBox="0 0 48 48"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<path
						d="M16.3083126,13.5575621 C17.212521,12.6758465 18.6746453,12.6758465 19.4922806,13.5575621 L24.0133226,18.0230248 L28.6209379,13.5575621 C29.5251463,12.6758465 30.9872705,12.6758465 31.8049058,13.5575621 C32.7860681,14.4487585 32.7860681,15.889842 31.8049058,16.6957111 L27.3608176,21.151693 L31.8049058,25.6930023 C32.7860681,26.5841986 32.7860681,28.0252822 31.8049058,28.8311512 C30.9872705,29.7981941 29.5251463,29.7981941 28.6209379,28.8311512 L24.0133226,24.4510158 L19.4922806,28.8311512 C18.6746453,29.7981941 17.212521,29.7981941 16.3083126,28.8311512 C15.4137234,28.0252822 15.4137234,26.5841986 16.3083126,25.6930023 L20.8389739,21.151693 L16.3083126,16.6957111 C15.4137234,15.889842 15.4137234,14.4487585 16.3083126,13.5575621 L16.3083126,13.5575621 Z M9.87119353,3.24 C11.1166068,1.228125 13.3583507,0 15.6767354,0 L32.240732,0 C34.6453376,0 36.8870815,1.228125 38.1324948,3.24 L46.9940893,17.49375 C48.3353036,19.65 48.3353036,22.35 46.9940893,24.50625 L38.1324948,38.75625 C36.8870815,40.771875 34.6453376,42 32.240732,42 L15.6767354,42 C13.3583507,42 11.1166068,40.771875 9.87119353,38.75625 L1.00576702,24.50625 C-0.335255674,22.35 -0.335255674,19.65 1.00576702,17.49375 L9.87119353,3.24 Z M4.95376353,20.0519187 C4.50454509,20.7819413 4.50454509,21.6920993 4.95376353,22.4221219 L13.8554068,36.8329571 C14.2690341,37.5060948 15.0193347,37.9232506 15.7407776,37.9232506 L32.3724409,37.9232506 C33.1804569,37.9232506 33.9307575,37.5060948 34.3443848,36.8329571 L43.2421804,22.4221219 C43.6942846,21.6920993 43.6942846,20.7819413 43.2421804,20.0519187 L34.3443848,5.64297968 C33.9307575,4.96510158 33.1804569,4.55079007 32.3724409,4.55079007 L15.7407776,4.55079007 C15.0193347,4.55079007 14.2690341,4.96510158 13.8554068,5.64297968 L4.95376353,20.0519187 Z M38.2882725,3.27656885 L34.3443848,5.64297968 L38.2882725,3.27656885 Z M1.00987575,24.7828442 L4.95376353,22.4221219 L1.00987575,24.7828442 Z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
				</svg>
			);
		}
	},
	'validation-error': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="18"
					height="18"
					viewBox="0 0 18 18"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<g fill="none" fill-rule="evenodd">
						<circle cx="9" cy="9" r="6.75" fill="white" />
						<path
							d="m18 9c0 4.9717-4.0298 9-9 9-4.9702 0-9-4.0283-9-9 0-4.9688 4.0298-9 9-9 4.9702 0 9 4.0312 9 9zm-9 1.8145c-0.92196 0-1.6694 0.7474-1.6694 1.6694 0 0.92196 0.7474 1.6694 1.6694 1.6694s1.6694-0.7474 1.6694-1.6694c0-0.92196-0.7474-1.6694-1.6694-1.6694zm-1.3157-1.065c0.012593 0.23095 0.20355 0.41175 0.43483 0.41175h1.7618c0.23128 0 0.42224-0.1808 0.43483-0.41175l0.2692-4.9355c0.013609-0.24946-0.18501-0.45922-0.43483-0.45922h-2.3002c-0.24982 0-0.4484 0.20976-0.43479 0.45922l0.2692 4.9355z"
							fill="var(--digi--color--icons--danger)"
						/>
					</g>
				</svg>
			);
		}
	},
	'validation-warning': {
		IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
			return (
				<svg
					width="18"
					height="18"
					viewBox="0 0 18 18"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}

				>
					{children}
					<g fill="none" fill-rule="evenodd">
						<circle
							cx="9"
							cy="9"
							r="6.75"
							fill="var(--digi--color--icons--informative)"
						/>
						<path
							d="m18 9c0 4.9717-4.0298 9-9 9-4.9702 0-9-4.0283-9-9 0-4.9688 4.0298-9 9-9 4.9702 0 9 4.0312 9 9zm-9 1.8145c-0.92196 0-1.6694 0.7474-1.6694 1.6694 0 0.92196 0.7474 1.6694 1.6694 1.6694s1.6694-0.7474 1.6694-1.6694c0-0.92196-0.7474-1.6694-1.6694-1.6694zm-1.3157-1.065c0.012593 0.23095 0.20355 0.41175 0.43483 0.41175h1.7618c0.23128 0 0.42224-0.1808 0.43483-0.41175l0.2692-4.9355c0.013609-0.24946-0.18501-0.45922-0.43483-0.45922h-2.3002c-0.24982 0-0.4484 0.20976-0.43479 0.45922l0.2692 4.9355z"
							fill="var(--digi--color--icons--warning)"
						/>
					</g>
				</svg>
			);
		}
	},
  upload: {
    IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
      return (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          viewBox="0 0 24 24"
          aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}
        >
          {children}
          <path
            d="M2.25 24A2.307 2.307 0 0 1 0 21.75v-5.362h2.25v5.362h19.5v-5.362H24v5.362A2.307 2.307 0 0 1 21.75 24Zm8.625-5.738V4.35l-4.5 4.5-1.612-1.612L12 0l7.238 7.237-1.613 1.613-4.5-4.5v13.912Z"
            fill="currentColor"
          />
        </svg>
      );
    }
  },
  work: {
    IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
      return (
        <svg
        xmlns="http://www.w3.org/2000/svg"
        width="24"
        height="24"
        viewBox="0 0 24 24"
        aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}
        >
          {children}
          <path d="M160-120q-33 0-56.5-23.5T80-200v-440q0-33 23.5-56.5T160-720h160v-80q0-33 23.5-56.5T400-880h160q33 0 56.5 23.5T640-800v80h160q33 0 56.5 23.5T880-640v440q0 33-23.5 56.5T800-120H160Zm240-600h160v-80H400v80Z"
          fill="currentColor"
          />
          </svg>
      );
    }
  },
  grading: {
    IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
      return (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          viewBox="0 0 24 24"
          aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}
        >
          {children}
          <path d="M657-121 544-234l56-56 57 57 127-127 56 56-183 183Zm-537 1v-80h360v80H120Zm0-160v-80h360v80H120Zm0-160v-80h720v80H120Zm0-160v-80h720v80H120Zm0-160v-80h720v80H120Z"
            fill="currentColor"
          />
        </svg>
      );
    }
  },
  info: {
    IconComponent({ afSvgAriaHidden, afSvgAriaLabelledby }: { afSvgAriaHidden: boolean, afSvgAriaLabelledby: string }, children: any) {
      return (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          viewBox="0 0 24 24"
          aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
					aria-labelledby={afSvgAriaLabelledby}
        >
          {children}
          <path d="M440-280h80v-240h-80v240Zm40-320q17 0 28.5-11.5T520-640q0-17-11.5-28.5T480-680q-17 0-28.5 11.5T440-640q0 17 11.5 28.5T480-600Zm0 520q-83 0-156-31.5T197-197q-54-54-85.5-127T80-480q0-83 31.5-156T197-763q54-54 127-85.5T480-880q83 0 156 31.5T763-763q54 54 85.5 127T880-480q0 83-31.5 156T763-197q-54 54-127 85.5T480-80Zm0-80q134 0 227-93t93-227q0-134-93-227t-227-93q-134 0-227 93t-93 227q0 134 93 227t227 93Zm0-320Z"
            fill="currentColor"
          />
        </svg>
      );
    }
  }
} as const;

export const icon = {
  ...coreIcon,
  ...skvIcon
}

export type IconName = keyof typeof icon;
export const iconNames = Object.keys(icon) as IconName[];
