import { HTMLStencilElement } from '@stencil/core/internal';

export const getCSSModifiersFromProps = (
	el: HTMLStencilElement,
	propBaseName: string | string[]
) => {
	if (typeof propBaseName !== 'string') {
		return propBaseName.reduce((acc, curr) => {
			return { ...acc, ...getCSSModifiersFromProps(el, curr) };
		}, {});
	} else {
		const propMatch = Array.from(el.attributes).find((i) => {
			return i.nodeName.startsWith(`af-${propBaseName}`);
		});

		return {
			[`${el.nodeName.toLowerCase()}--${propBaseName}-${propMatch?.value}`]:
				!!propMatch
		};
	}
};
