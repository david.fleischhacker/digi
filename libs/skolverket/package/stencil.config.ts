import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';

console.log('Skolverket - stencil.config');

// Path is relative from .config/

export const StencilBaseConfig: Config = {
	namespace: 'digi-skolverket',
	taskQueue: 'async',
	autoprefixCss: false,
  sourceMap: false,
	srcDir: '../src',
	tsconfig: '../tsconfig.json',
	plugins: [
		sass()
	],
  enableCache: false,
  cacheDir: '../../../.digi/skv-stencil',
	outputTargets: [],
	globalStyle: '../src/global/styles/index.scss'
};
