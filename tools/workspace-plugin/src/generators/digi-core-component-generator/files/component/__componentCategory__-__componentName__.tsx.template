import { Component, h, Host, Prop, State, Element, Method, EventEmitter, Event } from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';

/**
 * @slot mySlot - Slot description, if any
 *
 * @enums myEnum - <%= componentCategory %>-<%= componentName %>-myEnum.enum.ts
 *
 * @swedishName <namn>
 */
@Component({
  tag: 'digi-<%= componentCategory %>-<%= componentName %>',
  styleUrl: '<%= componentCategory %>-<%= componentName %>.<%= style %>',
  scoped: true
})
export class <%= className %> {
  @Element() hostElement: HTMLStencilElement;

  // Replace with prop and enums!
  private variation: any;

  @State() myState: any;

  /**
   * Prop-beskrivning på svenska
   * @en Prop description in english
   */
  @Prop() afMyProp: any;

  /**
   * Event-beskrivning på svenska
   * @en Event description in english
   */
  @Event() afMyEvent: EventEmitter;

  /**
   * Metod-beskrivning på svenska
   * @en Method description in english
   */
  @Method()
  async afMMyMethod(): Promise<any> {
    return this.myState;
  }

  componentWillLoad() {}
  componentDidLoad() {}
  componentWillUpdate() {}

  get cssModifiers() {
    return {
      <% for( let variation of variations ) { %>'digi-<%= componentCategory %>-<%= componentName %>--<%= variation %>': this.variation == '<%= variation %>',<% } %>
    };
  }

  render() {
    return (
      <Host>
        <div class={{
          "digi-<%= componentCategory %>-<%= componentName %>": true,
            ...this.cssModifiers
          }}>
          ... my content here!
          <div class="digi-<%= componentCategory %>-<%= componentName %>__my-block">
          </div>
        </div>
      </Host>
    );
  }
}
